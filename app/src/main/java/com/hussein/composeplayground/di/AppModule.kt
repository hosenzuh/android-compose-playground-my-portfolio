package com.hussein.composeplayground.di

import com.hussein.composeplayground.data.sources.remote.BaseRemoteService
import com.hussein.composeplayground.data.sources.remote.MediumRemoteService
import com.hussein.composeplayground.data.sources.remote.RemoteDataSource
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import dagger.Module
import dagger.Provides
import dagger.assisted.AssistedFactory
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Qualifier
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    private const val BASE_URL = "https://backend-compose-playground-my-portfolio.onrender.com"
    private const val MEDIUM_BASE_URL = "https://www.toptal.com"

    @Provides
    @BaseUrl
    @Singleton
    fun provideBaseUrl() = BASE_URL

    @Provides
    @MediumBaseUrl
    @Singleton
    fun provideMediumBaseUrl() = MEDIUM_BASE_URL

    @Provides
    @Singleton
    fun provideMoshi(): Moshi = Moshi.Builder().add(KotlinJsonAdapterFactory()).build()

    @Provides
    @BaseRemoteDataSource
    fun provideBaseRemoteDataSource(
        @BaseUrl baseUrl: String,
        remoteDataSourceFactory: RemoteDataSourceFactory<BaseRemoteService>
    ) =
        remoteDataSourceFactory.create(baseUrl, BaseRemoteService::class.java)

    @Provides
    @MediumRemoteDataSource
    fun provideMediumRemoteDataSource(
        @MediumBaseUrl baseUrl: String,
        remoteDataSourceFactory: RemoteDataSourceFactory<MediumRemoteService>
    ) =
        remoteDataSourceFactory.create(baseUrl, MediumRemoteService::class.java)

    @Provides
    @Singleton
    fun provideRemoteService(@BaseRemoteDataSource remoteDataSource: RemoteDataSource<BaseRemoteService>) =
        remoteDataSource.remoteService

    @Provides
    @Singleton
    fun provideMediumRemoteService(@MediumRemoteDataSource remoteDataSource: RemoteDataSource<MediumRemoteService>) =
        remoteDataSource.remoteService

}

@Qualifier
annotation class BaseUrl

@Qualifier
annotation class MediumBaseUrl

@Qualifier
annotation class BaseRemoteDataSource

@Qualifier
annotation class MediumRemoteDataSource

@AssistedFactory
interface RemoteDataSourceFactory<T> {
    fun create(baseUrl: String, serviceClass: Class<T>): RemoteDataSource<T>
}
