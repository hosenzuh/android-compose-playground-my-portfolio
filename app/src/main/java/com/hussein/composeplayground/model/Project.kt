package com.hussein.composeplayground.model

import android.os.Build
import android.os.Bundle
import android.os.Parcelable
import androidx.compose.runtime.Immutable
import androidx.navigation.NavType
import com.hussein.composeplayground.util.parcelableType
import kotlinx.parcelize.Parcelize
import kotlinx.serialization.Serializable
import kotlinx.serialization.json.Json

@Immutable
@Serializable
@Parcelize
data class Project(
    val id: Int,
    val name: String,
    val description: String,
    val video: String?,
    val link: String?,
    val workExpertiseId: Int?,
    val images: List<ProjectImage>,
    val skills: List<String>,
    val logo: String?,
) : Parcelable