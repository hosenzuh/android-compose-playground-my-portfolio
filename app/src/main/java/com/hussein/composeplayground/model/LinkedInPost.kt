package com.hussein.composeplayground.model

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class LinkedInPost(
    val title: String?,
    val content: String,
    val images: List<String>,
    val url: String,
)
