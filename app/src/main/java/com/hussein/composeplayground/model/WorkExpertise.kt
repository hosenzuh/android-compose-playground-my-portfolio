package com.hussein.composeplayground.model

import androidx.compose.runtime.Immutable
import com.squareup.moshi.JsonClass

@Immutable
@JsonClass(generateAdapter = true)
data class WorkExpertise(
    val id: Int,
    val companyName: String,
    val companyImage: String?,
    val startDate: String,
    val endDate: String?,
    val description: String,
    val link: String?,
    val projects: List<Project>,
)
