package com.hussein.composeplayground.model

import androidx.compose.runtime.Immutable
import androidx.compose.runtime.Stable
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
@Stable
@Immutable
data class MediumArticle(
    val url: String,
    val title: String,
    @Json(name = "content_html") val contentHtml: String,
    @Json(name = "date_published") val publishingDate: String
)