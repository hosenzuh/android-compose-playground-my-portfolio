package com.hussein.composeplayground.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize
import kotlinx.serialization.Serializable

@Serializable
@Parcelize
data class ProjectImage(val id: Int, val path: String, val projectId: Int) : Parcelable
