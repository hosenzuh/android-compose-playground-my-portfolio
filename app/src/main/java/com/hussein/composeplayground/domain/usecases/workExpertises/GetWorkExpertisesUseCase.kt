package com.hussein.composeplayground.domain.usecases.workExpertises


import com.hussein.composeplayground.data.repositories.WorkExpertisesRepository
import com.hussein.composeplayground.domain.base.ReceiveDataUseCase
import com.hussein.composeplayground.model.WorkExpertise
import com.hussein.composeplayground.util.ConnectionManager
import javax.inject.Inject

class GetWorkExpertisesUseCase @Inject constructor(
    connectionManager: ConnectionManager,
    private val repository: WorkExpertisesRepository,
) : ReceiveDataUseCase<List<WorkExpertise>>(connectionManager) {

    operator fun invoke() = execute(repository::getWorkExpertises)

}