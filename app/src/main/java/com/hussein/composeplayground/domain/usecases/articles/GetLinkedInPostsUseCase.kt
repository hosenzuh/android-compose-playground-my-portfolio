package com.hussein.composeplayground.domain.usecases.articles


import com.hussein.composeplayground.data.repositories.ArticlesRepository
import com.hussein.composeplayground.domain.base.ReceiveDataUseCase
import com.hussein.composeplayground.model.LinkedInPost
import com.hussein.composeplayground.util.ConnectionManager
import kotlinx.coroutines.delay
import javax.inject.Inject


class GetLinkedInPostsUseCase @Inject constructor(
    connectionManager: ConnectionManager,
    private val repository: ArticlesRepository,
) : ReceiveDataUseCase<List<LinkedInPost>>(connectionManager) {

    operator fun invoke() = execute {
        repository.getLinkedInPosts()
    }
}