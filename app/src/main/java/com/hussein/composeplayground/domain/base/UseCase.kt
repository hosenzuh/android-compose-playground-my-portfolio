package com.hussein.composeplayground.domain.base

import com.hussein.composeplayground.util.ConnectionManager
import com.hussein.composeplayground.util.logcat
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.flow

abstract class UseCase

abstract class ReceiveDataUseCase<T>(
    private val connectionManager: ConnectionManager,
) : UseCase() {

    protected fun execute(request: suspend () -> State<T>) = flow {
        if (!connectionManager.isConnected) {
            emit(State.Failure.InternetUnavailable())

            return@flow
        }

        emit(State.loading())
        emit(request())
    }.catch { throwable ->
        logcat(throwable)

        emit(State.failure())
    }
}

abstract class SendDataUseCase(
    private val connectionManager: ConnectionManager,
) : UseCase() {

    protected fun execute(request: suspend () -> State<Unit>) = flow {
        if (!connectionManager.isConnected) {
            emit(State.Failure.InternetUnavailable())

            return@flow
        }

        emit(State.loading())
        emit(request())
    }.catch { throwable ->
        logcat(throwable)

        emit(State.failure())
    }
}