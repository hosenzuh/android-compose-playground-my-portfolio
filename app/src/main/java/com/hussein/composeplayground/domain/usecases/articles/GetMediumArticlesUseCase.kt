package com.hussein.composeplayground.domain.usecases.articles

import com.hussein.composeplayground.data.repositories.ArticlesRepository
import com.hussein.composeplayground.domain.base.ReceiveDataUseCase
import com.hussein.composeplayground.domain.base.UseCase
import com.hussein.composeplayground.model.MediumArticle
import com.hussein.composeplayground.util.ConnectionManager
import javax.inject.Inject

class GetMediumArticlesUseCase @Inject constructor(
    connectionManager: ConnectionManager,
    private val articlesRepository: ArticlesRepository
) :
    ReceiveDataUseCase<List<MediumArticle>>(connectionManager) {

    operator fun invoke() = execute(articlesRepository::getMediumArticles)

}