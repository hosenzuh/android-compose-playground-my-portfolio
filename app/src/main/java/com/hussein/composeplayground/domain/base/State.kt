package com.hussein.composeplayground.domain.base

import kotlin.contracts.ExperimentalContracts
import kotlin.contracts.contract

@OptIn(ExperimentalContracts::class)
sealed class State<T>() {

    abstract val message: String?

    companion object {

        fun <T> initial(message: String? = null) = Initial<T>(message)

        fun <T> loading(message: String? = null) = Loading.Generic<T>(message)

        fun <T> success(data: T? = null, message: String? = null) =
            if (data == null)
                Success.Generic<T>(message)
            else
                Success.Data<T>(data, message)

        fun <T> failure(message: String? = null) = Failure.Generic<T>(message)

    }

    data class Initial<T>(override val message: String?) : State<T>()

    sealed class Loading<T> : State<T>() {

        data class Generic<T>(override val message: String?) : Loading<T>()

        data class Refreshing<T>(override val message: String?) : Loading<T>()

    }

    sealed class Success<T> : State<T>() {

        data class Generic<T>(override val message: String? = null) : Success<T>()

        data class Data<T>(val data: T, override val message: String? = null) : Success<T>()

    }

    sealed class Failure<T> : State<T>() {

        data class Generic<T>(override val message: String? = null) : Failure<T>()

        data class InternetUnavailable<T>(override val message: String? = null) : Failure<T>()

        data class ItemNotFound<T>(override val message: String? = null) : Failure<T>()
    }

    val dataOrNull get() = if (this is Success.Data) data else null

    fun isSuccessWithData(): Boolean {
        contract {
            returns(true) implies (this@State is Success.Data<T>)
        }
        return this is Success.Data<T>
    }

    fun isInitial(): Boolean {
        contract {
            returns(true) implies (this@State is Initial)
        }
        return this is Initial
    }

    fun isFailure(): Boolean {
        contract {
            returns(true) implies (this@State is Failure)
        }
        return this is Failure
    }

    fun <E> transform(mapper: (T) -> E): State<E> {
        return if (this is Success.Data) {
            State.Success.Data(mapper(this.data))
        } else {
            this as State<E>
        }
    }

}