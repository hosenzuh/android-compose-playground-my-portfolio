package com.hussein.composeplayground.domain.usecases.projects


import com.hussein.composeplayground.data.repositories.ProjectsRepository
import com.hussein.composeplayground.domain.base.ReceiveDataUseCase
import com.hussein.composeplayground.model.Project
import com.hussein.composeplayground.util.ConnectionManager
import javax.inject.Inject

class GetProjectsUseCase @Inject constructor(
    connectionManager: ConnectionManager,
    private val repository: ProjectsRepository,
) : ReceiveDataUseCase<List<Project>>(connectionManager) {

    operator fun invoke() = execute(repository::getProjects)

}