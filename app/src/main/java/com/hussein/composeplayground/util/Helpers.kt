package com.hussein.composeplayground.util

import android.util.Log

const val LOG_TAG = "Compose Playground"

fun logcat(throwable: Throwable, message: String = "") {
    Log.e(LOG_TAG, message, throwable)
}