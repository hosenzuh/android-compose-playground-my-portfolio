package com.hussein.composeplayground.util

const val GITLAB_URL = "https://gitlab.com/hosenzuh"
const val LINKEDIN_URL = "https://www.linkedin.com/in/hussein-al-zuhile-7026011a5"
const val INDEED_CV_URL = "https://profile.indeed.com/p/husseina-5q7rv4s"
const val MEDIUM_URL = "https://medium.com/@husseinalzuhile"

const val MY_PHONE_NUMBER = "+963991039529"