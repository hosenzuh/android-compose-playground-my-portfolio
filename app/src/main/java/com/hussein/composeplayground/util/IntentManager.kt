package com.hussein.composeplayground.util

import android.Manifest
import android.app.SearchManager
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.provider.AlarmClock
import android.provider.CalendarContract
import android.provider.Settings
import androidx.annotation.IntRange
import androidx.core.content.ContextCompat
import androidx.core.net.toUri
import androidx.core.os.bundleOf
import com.google.android.gms.actions.NoteIntents
import com.google.common.net.MediaType
import java.net.URLEncoder
import java.util.Calendar


object IntentManager {

    lateinit var declearedPermissionsInManifest: Array<String>

    fun Context.startIntentDirection(
        intentDirection: IntentDirection,
        withChooser: Boolean = false,
        chooserTitle: String? = null
    ): StartingIntentResult {
        val intent = Intent(intentDirection.action, intentDirection.data).apply {
            intentDirection.extras?.let { putExtras(it) }
            intentDirection.mimeType?.let {
                type = "${it.type()}/${it.subtype()}"

            }
        }

        if (this@IntentManager::declearedPermissionsInManifest.isInitialized.not()) {
            declearedPermissionsInManifest = getDeclaredPermissionsInManifest()
        }

        return when {

            intentDirection.requiredPermissions
                .any { requiredPermission -> requiredPermission !in declearedPermissionsInManifest } ->
                StartingIntentResult.PERMISSIONS_NOT_DECLARED_IN_MANIFEST

            !arePermissionsGranted(*intentDirection.requiredPermissions) ->
                StartingIntentResult.PERMISSIONS_NOT_GRANTED

            else -> {
                try {
                    if (withChooser)
                        startActivity(Intent.createChooser(intent, chooserTitle ?: ""))
                    else
                        startActivity(intent)
                    StartingIntentResult.STARTED
                } catch (e: ActivityNotFoundException) {
                    StartingIntentResult.NO_HANDLER_FOR_INTENT_EXIST
                }
            }
        }
    }

    // Helpers

    fun Context.viewUrl(url: String) = this.startIntentDirection(IntentDirection.ViewUrl(url))
}


sealed class IntentDirection(
    val action: String,
    val data: Uri?,
    val extras: Bundle? = null,
    val mimeType: MediaType? = null
) {

    open val requiredPermissions: Array<String> = emptyArray()

    open class Generic(action: String, data: Uri?, extras: Bundle?, mimeType: MediaType?) :
        IntentDirection(action, data, extras, mimeType)

    class ViewUrl(url: String) : IntentDirection(Intent.ACTION_VIEW, url.toUri())

    class OpenDialer(telephone: String) :
        IntentDirection(Intent.ACTION_VIEW, "tel:$telephone".toUri())

    class OpenMapLocation(
        latitude: Double,
        longitude: Double,
        label: String?,
        @IntRange(1, 23) zoom: Int = 12
    ) : IntentDirection(
        Intent.ACTION_VIEW,
        data = "geo:$latitude,$longitude?q=lat,lng${label?.let { "($it)" }}&z=${zoom}".toUri()
    )

    class SearchOnWeb(query: String) : IntentDirection(
        Intent.ACTION_WEB_SEARCH, null, bundleOf(
            SearchManager.QUERY to query
        )
    )

    class QueryMapLocation(
        nearestLatitude: Double,
        nearestLongitude: Double,
        query: String,
        @IntRange(1, 23) zoom: Int = 12
    ) : IntentDirection(
        Intent.ACTION_VIEW,
        "geo:$nearestLatitude,$nearestLongitude?q=${
            URLEncoder.encode(
                query,
                "UTF-8"
            )
        }&z=$zoom".toUri()
    )

    class OpenSettings : IntentDirection(Settings.ACTION_SETTINGS, null)

    class ComposeEmail(
        email: String,
        subject: String? = null,
        attachments: Array<Uri> = emptyArray(),
        CCs: Array<String> = emptyArray(),
        BCCs: Array<String> = emptyArray(),
    ) :
        IntentDirection(
            when (attachments.size) {
                0 -> Intent.ACTION_SENDTO
                1 -> Intent.ACTION_SEND
                else -> Intent.ACTION_SEND_MULTIPLE
            }, "mailto:".toUri(),
            bundleOf(
                Intent.EXTRA_EMAIL to email,
                Intent.EXTRA_SUBJECT to subject,
                Intent.EXTRA_STREAM to attachments.takeIf { it.isNotEmpty() },
                Intent.EXTRA_CC to CCs,
                Intent.EXTRA_BCC to BCCs,
            ),
            mimeType = MediaType.ANY_TYPE
        )

    class OpenMediaFile(mediaUri: Uri, context: Context) :
        IntentDirection(
            Intent.ACTION_VIEW,
            mediaUri,
            mimeType = context.contentResolver.getType(mediaUri)?.let { MediaType.parse(it) }
        )

    class CreateNote(title: String, content: String) : IntentDirection(
        NoteIntents.ACTION_CREATE_NOTE,
        null,
        bundleOf(
            NoteIntents.EXTRA_NAME to title,
            NoteIntents.EXTRA_TEXT to content
        )
    )

    class SetAlarm(
        hour: Int,
        minutes: Int,
        message: String? = null,
        days: IntArray? = null,
        vibrate: Boolean = false
    ) : IntentDirection(
        AlarmClock.ACTION_SET_ALARM,
        null,
        bundleOf(
            AlarmClock.EXTRA_HOUR to hour,
            AlarmClock.EXTRA_MINUTES to minutes,
            AlarmClock.EXTRA_MESSAGE to message,
            AlarmClock.EXTRA_DAYS to days,
            AlarmClock.EXTRA_VIBRATE to vibrate,
        )
    ) {
        override val requiredPermissions: Array<String> = arrayOf(Manifest.permission.SET_ALARM)
    }

    class SetCalendarEvent(
        title: String,
        description: String? = null,
        location: String? = null,
        beginTime: Calendar,
        endTime: Calendar,
    ) : IntentDirection(
        Intent.ACTION_INSERT, CalendarContract.Events.CONTENT_URI,
        bundleOf(
            CalendarContract.Events.TITLE to title,
            CalendarContract.Events.DESCRIPTION to description,
            CalendarContract.Events.EVENT_LOCATION to location,
            CalendarContract.EXTRA_EVENT_BEGIN_TIME to beginTime,
            CalendarContract.EXTRA_EVENT_END_TIME to endTime,
        )
    )
}

enum class StartingIntentResult {
    STARTED,
    PERMISSIONS_NOT_DECLARED_IN_MANIFEST,
    PERMISSIONS_NOT_GRANTED,
    NO_HANDLER_FOR_INTENT_EXIST
}

fun Context.arePermissionsGranted(vararg permissions: String) = permissions.all {
    ContextCompat.checkSelfPermission(this, it) == PackageManager.PERMISSION_GRANTED
}

fun Context.getDeclaredPermissionsInManifest(): Array<String> {

    return try {
        packageManager.getPackageInfo(
            packageName,
            PackageManager.GET_PERMISSIONS
        ).requestedPermissions
    } catch (e: PackageManager.NameNotFoundException) {
        e.printStackTrace()
        emptyArray()
    }

}
