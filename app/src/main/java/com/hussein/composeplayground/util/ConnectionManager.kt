package com.hussein.composeplayground.util

import android.content.Context
import android.net.ConnectivityManager
import android.net.ConnectivityManager.NetworkCallback
import android.net.Network
import android.net.NetworkCapabilities
import android.net.NetworkRequest
import androidx.core.content.getSystemService
import androidx.lifecycle.DefaultLifecycleObserver
import androidx.lifecycle.LifecycleOwner
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import javax.inject.Inject
import javax.inject.Singleton


@Singleton
class ConnectionManager @Inject constructor(@ApplicationContext val context: Context) :
    DefaultLifecycleObserver {

    private val connectivityManager = context.getSystemService<ConnectivityManager>()

    private val mutableConnectionStatus = MutableStateFlow(getInitialConnectionStatus())

    private val connectionStatus = mutableConnectionStatus.asStateFlow()

    val isConnected = connectionStatus.value

    private val networkRequest =
        NetworkRequest.Builder().addCapability(NetworkCapabilities.NET_CAPABILITY_INTERNET).build()

    private val networkCallback = object : NetworkCallback() {
        override fun onAvailable(network: Network) {
            mutableConnectionStatus.value = true
        }

        override fun onLost(network: Network) {
            mutableConnectionStatus.value = false
        }
    }

    override fun onResume(owner: LifecycleOwner) {
        super.onResume(owner)
        connectivityManager?.registerNetworkCallback(networkRequest, networkCallback)
    }

    override fun onPause(owner: LifecycleOwner) {
        super.onPause(owner)

        connectivityManager?.unregisterNetworkCallback(networkCallback)
    }

    private fun getInitialConnectionStatus(): Boolean {
        connectivityManager?.let {
            val networkCapabilities = it.getNetworkCapabilities(it.activeNetwork)
            if (networkCapabilities != null) {
                return networkCapabilities.hasCapability(NetworkCapabilities.NET_CAPABILITY_INTERNET)
            }
        }

        return false
    }
}