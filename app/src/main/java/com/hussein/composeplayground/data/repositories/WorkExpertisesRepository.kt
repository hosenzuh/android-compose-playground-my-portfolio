package com.hussein.composeplayground.data.repositories

import com.hussein.composeplayground.data.base.BaseRepository
import com.hussein.composeplayground.data.sources.remote.BaseRemoteService
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class WorkExpertisesRepository @Inject constructor(private val baseRemoteService: BaseRemoteService) :
    BaseRepository() {

    suspend fun getWorkExpertises() = sendRemoteRequest(baseRemoteService::getWorkExpertises)

}