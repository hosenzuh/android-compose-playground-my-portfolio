package com.hussein.composeplayground.data.sources.remote

import com.hussein.composeplayground.model.LinkedInPost
import com.hussein.composeplayground.model.Project
import com.hussein.composeplayground.model.WorkExpertise
import retrofit2.Response
import retrofit2.http.GET

interface BaseRemoteService {


    @GET("/workExpertises")
    suspend fun getWorkExpertises(): Response<List<WorkExpertise>>

    @GET("/projects")
    suspend fun getProjects(): Response<List<Project>>

    @GET("/linkedInPosts")
    suspend fun getLinkedInPosts(): Response<List<LinkedInPost>>

}