package com.hussein.composeplayground.data.base

import com.hussein.composeplayground.domain.base.State
import retrofit2.Response

abstract class BaseRepository {

    suspend fun <T> sendRemoteRequest(request: suspend () -> Response<T>): State<T> {

        val response = request()

        return if (response.isSuccessful) {
            State.success(response.body())
        } else {
            State.failure(response.message())
        }
    }

}