package com.hussein.composeplayground.data.repositories

import com.hussein.composeplayground.data.base.BaseRepository
import com.hussein.composeplayground.data.sources.remote.BaseRemoteService
import com.hussein.composeplayground.domain.base.State
import com.hussein.composeplayground.model.Project
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ProjectsRepository @Inject constructor(private val baseRemoteService: BaseRemoteService) :
    BaseRepository() {

    suspend fun getProjects() = sendRemoteRequest(baseRemoteService::getProjects)

}