package com.hussein.composeplayground.data.base

import androidx.paging.LoadState
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingSource
import androidx.paging.PagingState
import androidx.paging.compose.LazyPagingItems
import com.hussein.composeplayground.data.base.PagingFetchingState.*
import com.hussein.composeplayground.domain.base.State
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import okio.IOException

const val DEFAULT_INITIAL_PAGE_NUMBER = 1
const val DEFAULT_PAGE_SIZE = 10

class BasePagingSource<ResponseType : Any, PageItemModel : Any, MetaData>(
    val request: (pageNumber: Int) -> State<ResponseType>,
    val pageItemMapper: (ResponseType) -> List<PageItemModel>,
    val metaDateMapper: ((ResponseType) -> MetaData)?,
    val pageSize: Int
) : PagingSource<Int, PageItemModel>() {

    private val mutableMetaDataFlow = MutableStateFlow<MetaData?>(null)
    val metaDataFlow: StateFlow<MetaData?> = mutableMetaDataFlow.asStateFlow()
    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, PageItemModel> {
        val pageNumber = params.key ?: DEFAULT_INITIAL_PAGE_NUMBER
        val responseState: State<ResponseType> = request(pageNumber)

        val prevPageNumber: Int?
        val nextPageNumber: Int?
        return if (responseState is State.Success.Data) {
            prevPageNumber = if (pageNumber > DEFAULT_INITIAL_PAGE_NUMBER) pageNumber - 1 else null
            nextPageNumber = if (params.loadSize == pageSize) pageNumber + 1 else null
            val pageItems = pageItemMapper(responseState.data)
            mutableMetaDataFlow.value = metaDateMapper?.invoke(responseState.data)
            LoadResult.Page(pageItems, prevPageNumber, nextPageNumber)
        } else {
            LoadResult.Error(IOException())
        }
    }

    override fun getRefreshKey(state: PagingState<Int, PageItemModel>): Int? {
        return state.anchorPosition?.let {
            state.closestPageToPosition(it)?.let { closestPage ->
                closestPage.prevKey?.plus(1) ?: closestPage.nextKey?.minus(1)
            }
        }
    }
}

fun <ResponseType : Any, PageItemModel : Any> createSimplePagingSource(
    request: (pageNumber: Int) -> State<ResponseType>,
    pageItemMapper: (ResponseType) -> List<PageItemModel>,
    pageSize: Int = DEFAULT_PAGE_SIZE,
): BasePagingSource<ResponseType, PageItemModel, Nothing> {
    return BasePagingSource(request, pageItemMapper, null, pageSize)
}

fun <ResponseType : Any, PageItemModel : Any, MetaData : Any> createPagingSourceWithMetaDataFlow(
    request: (pageNumber: Int) -> State<ResponseType>,
    pageItemMapper: (ResponseType) -> List<PageItemModel>,
    metaDateMapper: ((ResponseType) -> MetaData),
    pageSize: Int = DEFAULT_PAGE_SIZE,
): BasePagingSource<ResponseType, PageItemModel, MetaData> {
    return BasePagingSource(
        request, pageItemMapper, metaDateMapper, pageSize
    )
}

fun <ResponseType : Any, PageItemModel : Any, MetaData> createPagerWithMetaDataFlow(
    pagingSource: BasePagingSource<ResponseType, PageItemModel, MetaData>,
    pagingConfig: PagingConfig? = null
): PagerWithMetaDataFlow<PageItemModel, MetaData> {

    return PagerWithMetaDataFlow(
        Pager(
            pagingConfig ?: PagingConfig(
                pagingSource.pageSize,
                enablePlaceholders = false
            )
        ) { pagingSource },
        pagingSource.metaDataFlow
    )

}

data class PagerWithMetaDataFlow<PageItemModel : Any, MetaData>(
    val pager: Pager<Int, PageItemModel>,
    val metaDataFlow: StateFlow<MetaData?>
)

sealed class PagingFetchingState {
    data object Initial : PagingFetchingState()
    data object Loading : PagingFetchingState()
    data object NoData : PagingFetchingState()
    data class Error(val throwable: Throwable?) : PagingFetchingState()
    data object NotLoading : PagingFetchingState()
}

data class CombinedPagingFetchingState(
    val initialLoad: PagingFetchingState,
    val refreshingLoad: PagingFetchingState,
    val prepend: PagingFetchingState,
    val append: PagingFetchingState,
)

fun <T : Any> LazyPagingItems<T>.getPagingState(): CombinedPagingFetchingState {


    val initialLoadState = if (itemCount == 0) {
        when (loadState.refresh) {
            is LoadState.NotLoading -> {
                if (loadState.refresh.endOfPaginationReached) {
                    NoData
                } else {
                    Initial
                }
            }

            LoadState.Loading -> {
                Loading
            }

            is LoadState.Error -> {
                Error((loadState.refresh as? LoadState.Error)?.error)
            }
        }
    } else {
        // There is a data so the initial load is ended
        NotLoading
    }
    val refreshingState = if (itemCount == 0) {
        NotLoading
    } else {
        when (loadState.refresh) {
            is LoadState.NotLoading -> NotLoading
            LoadState.Loading -> Loading
            is LoadState.Error -> {
                Error((loadState.refresh as? LoadState.Error)?.error)
            }
        }
    }
    val prependState = when (loadState.prepend) {
        is LoadState.Error -> Error((loadState.prepend as? LoadState.Error)?.error)
        LoadState.Loading -> Loading
        is LoadState.NotLoading -> {
            if (loadState.prepend.endOfPaginationReached) {
                NoData
            } else {
                NotLoading
            }
        }
    }
    val appendState = when (loadState.append) {
        is LoadState.Error -> Error((loadState.append as? LoadState.Error)?.error)
        LoadState.Loading -> Loading
        is LoadState.NotLoading -> {
            if (loadState.append.endOfPaginationReached) {
                NoData
            } else {
                NotLoading
            }
        }
    }

    return CombinedPagingFetchingState(initialLoadState, refreshingState, prependState, appendState)
}