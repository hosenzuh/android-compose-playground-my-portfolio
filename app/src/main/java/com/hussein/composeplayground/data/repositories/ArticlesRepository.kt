package com.hussein.composeplayground.data.repositories

import com.hussein.composeplayground.data.base.BaseRepository
import com.hussein.composeplayground.data.sources.remote.BaseRemoteService
import com.hussein.composeplayground.data.sources.remote.MediumRemoteService
import com.hussein.composeplayground.model.MediumArticle
import com.squareup.moshi.JsonAdapter
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ArticlesRepository @Inject constructor(
    private val baseRemoteService: BaseRemoteService,
    private val mediumRemoteService: MediumRemoteService,
) : BaseRepository() {

    suspend fun getMediumArticles() =
        sendRemoteRequest { mediumRemoteService.getMediumArticles() }.transform {
            it.items
        }

    suspend fun getLinkedInPosts() = sendRemoteRequest { baseRemoteService.getLinkedInPosts() }

}