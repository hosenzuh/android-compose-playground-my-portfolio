package com.hussein.composeplayground.data.sources.remote

import com.squareup.moshi.Moshi
import dagger.assisted.Assisted
import dagger.assisted.AssistedInject
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

class RemoteDataSource<T> @AssistedInject constructor(
    private val moshi: Moshi,
    @Assisted private val baseUrl: String,
    @Assisted private val serviceClass: Class<T>
) {

    val remoteService: T

    init {

        val client = OkHttpClient.Builder().apply {
            val loggingInterceptor = HttpLoggingInterceptor().apply {
                level = HttpLoggingInterceptor.Level.BODY
            }
            addInterceptor(loggingInterceptor)
        }.build()

        val retrofit = Retrofit.Builder().apply {
            baseUrl(baseUrl)
            client(client)
            addConverterFactory(MoshiConverterFactory.create(moshi))
        }.build()

        remoteService = retrofit.create(serviceClass)
    }
}