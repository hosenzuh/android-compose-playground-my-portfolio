package com.hussein.composeplayground.data.sources.remote

import com.hussein.composeplayground.data.sources.remote.response.MediumArticlesResponse
import com.hussein.composeplayground.model.Project
import com.hussein.composeplayground.model.WorkExpertise
import org.json.JSONObject
import retrofit2.Response
import retrofit2.http.GET

interface MediumRemoteService {

    @GET("/developers/feed2json/convert?url=https://medium.com/feed/@husseinalzuhile")
    suspend fun getMediumArticles(): Response<MediumArticlesResponse>

}