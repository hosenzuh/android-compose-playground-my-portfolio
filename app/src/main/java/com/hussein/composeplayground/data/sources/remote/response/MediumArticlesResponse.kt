package com.hussein.composeplayground.data.sources.remote.response

import com.hussein.composeplayground.model.MediumArticle
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class MediumArticlesResponse(
    val items: List<MediumArticle>
)
