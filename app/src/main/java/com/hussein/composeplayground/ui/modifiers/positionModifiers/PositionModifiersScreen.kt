package com.hussein.composeplayground.ui.modifiers.positionModifiers

import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.background
import androidx.compose.foundation.combinedClickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.absoluteOffset
import androidx.compose.foundation.layout.defaultMinSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.requiredSizeIn
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.Button
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalLayoutDirection
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.IntOffset
import androidx.compose.ui.unit.LayoutDirection
import androidx.compose.ui.unit.dp
import com.hussein.composeplayground.ui.base.components.ModifierExample
import com.hussein.composeplayground.ui.base.components.NoteCard
import com.hussein.composeplayground.ui.theme.ComposePlaygroundTheme
import com.hussein.composeplayground.ui.theme.Default
import com.hussein.composeplayground.ui.theme.HalfDefault

@OptIn(ExperimentalFoundationApi::class)
@Composable
fun PositionModifiersScreen(
    modifier: Modifier = Modifier,
    onShowCodeSnippetClick: (imageResId: Int) -> Unit
) {
    Column(modifier.verticalScroll(rememberScrollState())) {
        ModifierExample(
            title = "Offset and Absolute Offset",
            body = "Modifier.offset: This modifier will offset from the start of the layout direction (from left in LTR and right in RTL).\n" +
                    "Modifier.absoluteOffset This offset is not affected by whatever the Layout Direction is (RTL or LTR)",
            codeSnippetImage = null,
            onShowCodeSnippetClick = onShowCodeSnippetClick
        ) {
            val defaultDirection = LocalLayoutDirection.current
            var layoutDirection by remember {
                mutableStateOf(defaultDirection)
            }
            Button(onClick = {
                layoutDirection = if (layoutDirection == LayoutDirection.Ltr) {
                    LayoutDirection.Rtl
                } else {
                    LayoutDirection.Ltr
                }
            }) {
                Text(
                    text = "Current layout Direction: ${layoutDirection.name}, Reverse layout direction"
                )
            }
            CompositionLocalProvider(value = LocalLayoutDirection provides layoutDirection) {
                Surface(
                    modifier = Modifier
                        .fillMaxWidth()
                        .absoluteOffset(x = Default)
                ) {
                    Text(text = "Box with absolute offset", Modifier.fillMaxWidth())
                }
                Spacer(modifier = Modifier.height(HalfDefault))
                Surface(
                    modifier = Modifier
                        .fillMaxWidth()
                        .offset(x = Default)
                ) {
                    Text(
                        text = "Box with normal (relative) offset", Modifier.fillMaxWidth(),
                    )
                }
            }
            NoteCard {
                Text(text = "Note that the offset does not reduce the size of the composable, but it only displaces the composable (that's why the text is clipped in RTL direction)")
            }
        }
        ModifierExample(
            title = "Offset and absolute offset lambdas",
            body = "This offset modifier provides a lambda with the Density of the screen as a Receiver to this lambda and a return type with type IntOffset, \n" +
                    "This modifier is designed to be used for offsets that change, possibly due to user interactions. It avoids recomposition when the offset is changing.",
            codeSnippetImage = null,
            onShowCodeSnippetClick = onShowCodeSnippetClick
        ) {
            var offset by remember { mutableStateOf(0) }
            Text(
                "Click for adding offset, and Double click to reduce it.",
                Modifier
                    .combinedClickable(onDoubleClick = { offset -= 10 }) { offset += 10 }
                    .offset { IntOffset(offset, offset) }
            )
        }
        ModifierExample(title = "Default min size", body = "", codeSnippetImage = null, onShowCodeSnippetClick = onShowCodeSnippetClick) {
            @Composable
            fun DefaultMinBox(modifier: Modifier = Modifier) {
                Box(
                    modifier
                        .defaultMinSize(minWidth = 100.dp, minHeight = 100.dp)
                        .background(Color.Blue)
                )
            }
            // This will be a 100.dp x 100.dp blue box. Because we are not providing any min constraints
            // to the DefaultMinBox, defaultMinSize will apply its min constraints.
            DefaultMinBox()
            // This will be a 50.dp x 50.dp blue box. Because we are providing min constraints
            // to the DefaultMinBox, defaultMinSize will not apply its min constraints.
            DefaultMinBox(Modifier.requiredSizeIn(minWidth = 200.dp, minHeight = 50.dp))
            // Note that if DefaultMinBox used requiredSizeIn or sizeIn rather than
            // defaultMinSize, the min constraints would have been applied with either // of the above usages.
        }
    }
}

@Preview
@Composable
fun OffsetModifiersScreenPreview() {
    ComposePlaygroundTheme {
        PositionModifiersScreen {

        }
    }
}