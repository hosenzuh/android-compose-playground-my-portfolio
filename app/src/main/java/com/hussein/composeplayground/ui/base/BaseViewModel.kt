package com.hussein.composeplayground.ui.base

import androidx.lifecycle.ViewModel
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.receiveAsFlow

abstract class BaseViewModel : ViewModel() {

    protected val _messages by lazy { Channel<String>() }

    val messages by lazy { _messages.receiveAsFlow() }

}