package com.hussein.composeplayground.ui.modifiers.layoutModifiers

import androidx.compose.animation.core.AnimationVector2D
import androidx.compose.animation.core.DeferredTargetAnimation
import androidx.compose.animation.core.ExperimentalAnimatableApi
import androidx.compose.animation.core.VectorConverter
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.gestures.rememberTransformableState
import androidx.compose.foundation.gestures.transformable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableFloatStateOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.graphicsLayer
import androidx.compose.ui.layout.Layout
import androidx.compose.ui.layout.approachLayout
import androidx.compose.ui.layout.layout
import androidx.compose.ui.layout.layoutId
import androidx.compose.ui.layout.onGloballyPositioned
import androidx.compose.ui.layout.positionInParent
import androidx.compose.ui.layout.positionInRoot
import androidx.compose.ui.layout.positionInWindow
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Constraints
import androidx.compose.ui.unit.IntSize
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.offset
import com.hussein.composeplayground.R
import com.hussein.composeplayground.ui.base.components.ModifierExample
import com.hussein.composeplayground.ui.theme.ComposePlaygroundTheme
import com.hussein.composeplayground.ui.theme.Default
import com.hussein.composeplayground.ui.theme.QuarterDefault
import kotlinx.coroutines.CoroutineScope
import kotlin.math.roundToInt

@OptIn(ExperimentalComposeUiApi::class, ExperimentalAnimatableApi::class)
@Composable
fun LayoutModifiersScreen(
    modifier: Modifier = Modifier,
    onShowCodeSnippetClick: (imageResId: Int) -> Unit
) {
    Column(modifier.verticalScroll(rememberScrollState())) {
        ModifierExample(
            title = "Layout Id",
            body = "This modifiers allows you to tag the element with layoutId to identify within its parent.\n" +
                    "This modifier will help when creating custom layouts, as you will see in the example,\n" +
                    "this is just a simple example to focus on the use-case of the modifier.",
            codeSnippetImage = R.drawable.modifier_layout_id,
            onShowCodeSnippetClick = onShowCodeSnippetClick
        ) {
            Layout({
                // Here the Containers are only needed to apply the modifiers. You could use the
                // modifier on header and footer directly if they are composables accepting modifiers.
                Box(Modifier.layoutId("header")) {
                    Row {
                        Text(text = "Header")
                    }
                }
                Box(Modifier.layoutId("footer")) {
                    Row {
                        Text(text = "Footer")
                    }
                }
            }) { measurables, constraints ->
                val placeables = measurables.map { measurable ->
                    when (measurable.layoutId) {
                        // You should use appropriate constraints. Here we measure fake constraints.
                        "header" -> measurable.measure(constraints)
                        "footer" -> measurable.measure(constraints)
                        else -> error("Unexpected tag")
                    }
                }
                // Size should be derived from children measured sizes on placeables,
                // but this is simplified for the purposes of the example.
                layout(placeables.sumOf { it.width }, placeables.sumOf { it.height }) {
                    placeables.first().placeRelative(0, 0)
                    placeables[1].placeRelative(0, placeables.first().measuredHeight)
                }
            }
        }
        ModifierExample(
            title = "Layout (To create a custom layouting to this composable)",
            body = "This is a convenience API of creating a custom LayoutModifier modifier, " +
                    "without having to create a class or an object that implements the LayoutModifier interface. " +
                    "The intrinsic measurements follow the default logic provided by the LayoutModifier.",
            codeSnippetImage = R.drawable.modifier_layout,
            onShowCodeSnippetClick = onShowCodeSnippetClick
        ) {
            val density = LocalDensity.current
            Box(
                Modifier
                    .background(Color.Green)
                    .aspectRatio(4f)
                    .layout { measurable, constraints ->
                        val padding = with(density) {
                            Default
                                .toPx()
                                .roundToInt()
                        }
                        val placeable =
                            measurable.measure(constraints.offset(horizontal = -padding))
                        layout(placeable.width + padding, placeable.height) {
                            placeable.placeRelative(padding, 0)
                        }
                    }
            ) {
                Box(
                    modifier = Modifier
                        .fillMaxSize()
                        .background(Color.Yellow)
                )
            }
        }

        ModifierExample(
            title = "onGloballyPositioned",
            body = "This modifier is a callback will invoke the LayoutCoordinates of the element when the global position of the element changed" +
                    ", keep in your mind that the modifier should be added after the position-changing modifier is",
            codeSnippetImage = 0,
            onShowCodeSnippetClick = onShowCodeSnippetClick
        ) {
            Column {

                var translationX by rememberSaveable {
                    mutableFloatStateOf(0f)
                }
                val transformationState =
                    rememberTransformableState(onTransformation = { zoomChange, panChange, rotationChange ->
                        translationX += panChange.x
                    })
                var translationText by remember {
                    mutableStateOf("currentTranslation = ")
                }
                Text(text = "Swipe the box horizontally to see the result changes from the callback")
                Spacer(modifier = Modifier.height(QuarterDefault))

                Box(
                    Modifier
                        .fillMaxWidth(0.3f)
                        .aspectRatio(1f)
                        .graphicsLayer {
                            this.translationX += translationX
                        }
                        .transformable(transformationState)
                        .onGloballyPositioned {
                            translationText = "current positionInRoot = ${it.positionInRoot()}\n" +
                                    "current positionInParent = ${it.positionInParent()}\n" +
                                    "current positionInWindow = ${it.positionInWindow()}"
                        }
                        .background(Color.DarkGray)
                )
                
                Spacer(modifier = Modifier.height(QuarterDefault))
                Text(text = translationText)
            }
        }
        ModifierExample(
            title = "Approached Layout",
            body = "Allows you to gradually reach the destination size, which makes you able to add custom behavior to it.",
            codeSnippetImage = null,
            onShowCodeSnippetClick = onShowCodeSnippetClick
        ) {
            // Creates a custom modifier that animates the constraints and measures child with the
            // animated constraints. This modifier is built on top of `Modifier.approachLayout` to approach
            // th destination size determined by the lookahead pass. A resize animation will be kicked off
            // whenever the lookahead size changes, to animate children from current size to destination
            // size. Fixed constraints created based on the animation value will be used to measure
            // child, so the child layout gradually changes its animated constraints until the approach
            // completes.
            fun Modifier.animateConstraints(
                sizeAnimation: DeferredTargetAnimation<IntSize, AnimationVector2D>,
                coroutineScope: CoroutineScope
            ) = this.approachLayout(
                isMeasurementApproachInProgress = { lookaheadSize ->
                    // Update the target of the size animation.
                    sizeAnimation.updateTarget(lookaheadSize, coroutineScope)
                    // Return true if the size animation has pending target change or hasn't finished
                    // running.
                    !sizeAnimation.isIdle
                }
            ) { measurable, _ ->
                // In the measurement approach, the goal is to gradually reach the destination size
                // (i.e. lookahead size). To achieve that, we use an animation to track the current
                // size, and animate to the destination size whenever it changes. Once the animation
                // finishes, the approach is complete.

                // First, update the target of the animation, and read the current animated size.
                val (width, height) = sizeAnimation.updateTarget(lookaheadSize, coroutineScope)
                // Then create fixed size constraints using the animated size
                val animatedConstraints = Constraints.fixed(width, height)
                // Measure child with animated constraints.
                val placeable = measurable.measure(animatedConstraints)
                layout(placeable.width, placeable.height) {
                    placeable.place(0, 0)
                }
            }

            var fullWidth by remember { mutableStateOf(false) }

            // Creates a size animation with a target unknown at the time of instantiation.
            val sizeAnimation = remember { DeferredTargetAnimation(IntSize.VectorConverter) }
            val coroutineScope = rememberCoroutineScope()
            Row(
                (if (fullWidth) Modifier.fillMaxWidth() else Modifier.width(100.dp))
                    .height(200.dp)
                    // Use the custom modifier created above to animate the constraints passed
                    // to the child, and therefore resize children in an animation.
                    .animateConstraints(sizeAnimation, coroutineScope)
                    .clickable { fullWidth = !fullWidth }) {
                Box(
                    Modifier
                        .weight(1f)
                        .fillMaxHeight()
                        .background(Color(0xffff6f69)),
                )
                Box(
                    Modifier
                        .weight(2f)
                        .fillMaxHeight()
                        .background(Color(0xffffcc5c))
                )
            }
        }
    }
}

@Preview
@Composable
fun LayoutModifiersScreenPreview() {
    ComposePlaygroundTheme {
        LayoutModifiersScreen {}
    }
}