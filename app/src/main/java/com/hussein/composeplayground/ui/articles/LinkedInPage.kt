package com.hussein.composeplayground.ui.articles

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.heightIn
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Link
import androidx.compose.material3.ElevatedButton
import androidx.compose.material3.ElevatedCard
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import coil.compose.AsyncImage
import com.hussein.composeplayground.model.LinkedInPost
import com.hussein.composeplayground.ui.theme.ComposePlaygroundTheme
import com.hussein.composeplayground.ui.theme.HalfDefault
import com.hussein.composeplayground.util.IntentDirection
import com.hussein.composeplayground.util.IntentManager.startIntentDirection
import com.toolsforfools.shimmery.shimmerIndividual.shimmer

@Composable
fun LinkedInPosts(
    linkedInPostsState: ArticlesViewModel.LinkedInPostsState,
    onRetryClicked: (typesToBeRetried: ArticlesTypes) -> Unit
) {
    Box(Modifier.fillMaxSize(), contentAlignment = Alignment.Center) {
        when (linkedInPostsState) {
            ArticlesViewModel.LinkedInPostsState.Initial -> {}
            ArticlesViewModel.LinkedInPostsState.Loading -> {
                Column(
                    Modifier.verticalScroll(rememberScrollState()),
                    verticalArrangement = Arrangement.spacedBy(
                        HalfDefault
                    )
                ) {
                    repeat(5) {
                        ElevatedCard(
                            Modifier
                                .fillMaxWidth()
                                .height((200..300).random().dp)
                                .clip(MaterialTheme.shapes.medium)
                                .shimmer(enabled = true)
                        ) {}
                    }
                }
            }

            is ArticlesViewModel.LinkedInPostsState.Loaded -> {
                LazyColumn(
                    Modifier.fillMaxSize(),
                    content = {
                        items(linkedInPostsState.posts) {
                            LinkedInPostItem(post = it)
                        }
                    },
                    verticalArrangement = Arrangement.spacedBy(HalfDefault)
                )
            }

            is ArticlesViewModel.LinkedInPostsState.Failure -> {
                Column {
                    linkedInPostsState.message?.let {
                        Text(text = it)
                    }
                    ElevatedButton(onClick = { onRetryClicked(ArticlesTypes.LINKED_IN_POSTS) }) {
                        Text(text = "Retry")
                    }
                }
            }
        }
    }
}

@Composable
fun LinkedInPostItem(modifier: Modifier = Modifier, post: LinkedInPost) {
    ElevatedCard {
        Column(modifier.padding(HalfDefault)) {
            Column {
                post.images.forEach { url ->
                    AsyncImage(
                        model = url,
                        contentDescription = "Banner",
                        Modifier
                            .fillMaxWidth()
                            .heightIn(max = 300.dp)
                    )
                }
            }
            Spacer(modifier = Modifier.height(HalfDefault))
            Row(verticalAlignment = Alignment.CenterVertically) {
                val context = LocalContext.current
                post.title?.let {
                    Text(
                        text = it,
                        style = MaterialTheme.typography.titleLarge,
                        fontWeight = FontWeight.Bold,
                        modifier = Modifier.weight(1f)
                    )
                }
                Spacer(modifier = Modifier.width(HalfDefault))
                IconButton(onClick = {
                    context.startIntentDirection(IntentDirection.ViewUrl(post.url))
                }) {
                    Icon(
                        imageVector = Icons.Default.Link,
                        contentDescription = "Open full article in medium"
                    )
                }
            }
            Spacer(modifier = Modifier.height(HalfDefault))
            Text(text = post.content, Modifier.fillMaxWidth())
        }
    }
}

@Preview
@Composable
fun LinkedInPostPreview() {
    ComposePlaygroundTheme {
        LinkedInPostItem(
            post = LinkedInPost(
                title = "Hussein Posted on Linkedin",
                content = "posidonium",
                images = listOf(),
                url = "https://duckduckgo.com/?q=partiendo"
            )
        )
    }
}