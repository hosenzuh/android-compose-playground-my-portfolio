package com.hussein.composeplayground.ui.articles

import android.text.Html
import android.text.TextUtils
import android.text.util.Linkify
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.CalendarMonth
import androidx.compose.material.icons.filled.Link
import androidx.compose.material3.ElevatedButton
import androidx.compose.material3.ElevatedCard
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.toArgb
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.viewinterop.AndroidView
import coil.compose.AsyncImage
import coil.compose.AsyncImagePainter
import com.google.android.material.textview.MaterialTextView
import com.hussein.composeplayground.model.MediumArticle
import com.hussein.composeplayground.ui.theme.ComposePlaygroundTheme
import com.hussein.composeplayground.ui.theme.HalfDefault
import com.hussein.composeplayground.util.IntentDirection
import com.hussein.composeplayground.util.IntentManager.startIntentDirection
import com.toolsforfools.shimmery.shimmerConfiguration.ShimmerConfiguration
import com.toolsforfools.shimmery.shimmerIndividual.shimmer

import org.jsoup.Jsoup

@Composable
fun MediumArticles(
    mediumArticlesState: ArticlesViewModel.MediumArticlesState,
    onRetryClicked: (typesToBeRetried: ArticlesTypes) -> Unit
) {
    Box(Modifier.fillMaxSize(), contentAlignment = Alignment.Center) {
        when (mediumArticlesState) {
            ArticlesViewModel.MediumArticlesState.Initial -> {}
            ArticlesViewModel.MediumArticlesState.Loading -> {
                Column(
                    Modifier.verticalScroll(rememberScrollState()),
                    verticalArrangement = Arrangement.spacedBy(
                        HalfDefault
                    )
                ) {
                    repeat(5) {
                        ElevatedCard(
                            Modifier
                                .fillMaxWidth()
                                .height((100..200).random().dp)
                                .clip(MaterialTheme.shapes.medium)

                        ) {}
                    }
                }
            }

            is ArticlesViewModel.MediumArticlesState.Loaded -> {
                LazyColumn(
                    Modifier.fillMaxSize(),
                    content = {
                        items(mediumArticlesState.mediumArticles) {
                            MediumArticleItem(article = it)
                        }
                    },
                    verticalArrangement = Arrangement.spacedBy(HalfDefault)
                )
            }

            is ArticlesViewModel.MediumArticlesState.Failure -> {
                Column {
                    mediumArticlesState.message?.let {
                        Text(text = it)
                    }
                    ElevatedButton(onClick = { onRetryClicked(ArticlesTypes.MEDIUM) }) {
                        Text(text = "Retry")
                    }
                }
            }
        }
    }
}

@Composable
fun MediumArticleItem(modifier: Modifier = Modifier, article: MediumArticle) {
    val parsedHtml = Jsoup.parse(article.contentHtml)
    val bannerImageUrl = parsedHtml.select("img").first()?.absUrl("src")

    var isLoadingImage by rememberSaveable {
        mutableStateOf(false)
    }
    ElevatedCard {
        Column(modifier.padding(HalfDefault)) {
            AsyncImage(
                model = bannerImageUrl,
                contentDescription = "Banner",
                Modifier
                    .fillMaxWidth()
                    .height(150.dp)
                    .shimmer(isLoadingImage, ShimmerConfiguration()),
                onState = {
                    isLoadingImage = it is AsyncImagePainter.State.Loading
                },
            )
            Spacer(modifier = Modifier.height(HalfDefault))
            Row(verticalAlignment = Alignment.CenterVertically) {
                val context = LocalContext.current
                Text(
                    text = article.title,
                    style = MaterialTheme.typography.titleLarge,
                    fontWeight = FontWeight.Bold,
                    modifier = Modifier.weight(1f)
                )
                Spacer(modifier = Modifier.width(HalfDefault))
                IconButton(onClick = {
                    context.startIntentDirection(IntentDirection.ViewUrl(article.url))
                }) {
                    Icon(
                        imageVector = Icons.Default.Link,
                        contentDescription = "Open full article in medium"
                    )
                }
            }
            val textColor = MaterialTheme.colorScheme.onSurface.toArgb()
            Spacer(modifier = Modifier.height(HalfDefault))
            Row(verticalAlignment = Alignment.CenterVertically) {
                Icon(imageVector = Icons.Default.CalendarMonth, contentDescription = null)
                Text(
                    text = article.publishingDate.substringBefore("T"),
                    style = MaterialTheme.typography.labelMedium
                )
            }
            Spacer(modifier = Modifier.height(HalfDefault))
            AndroidView(
                modifier = Modifier.fillMaxWidth(),
                factory = {
                    MaterialTextView(it)
                        .apply {
                            autoLinkMask = Linkify.WEB_URLS
                            linksClickable = true
                            ellipsize = TextUtils.TruncateAt.END
                            maxLines = 10
                            setTextColor(textColor)
                        }
                },
                update = {
                    it.text = Html.fromHtml(
                        parsedHtml.select("p").append("<br><br>").html(),
                        Html.FROM_HTML_MODE_LEGACY
                    )
                }
            )
        }
    }
}

@Preview
@Composable
fun MediumArticlePreview() {
    ComposePlaygroundTheme {
        MediumArticleItem(
            article = MediumArticle(
                url = "https://search.yahoo.com/search?p=accumsan",
                title = "omittantur",
                contentHtml = "<figure><img alt=\"\" src=\"https://cdn-images-1.medium.com/max/1024/0*uqcgP4xHxnkf0qjE\" /><figcaption>Photo by <a href=\"https://unsplash.com/@bradyn?utm_source=medium&amp;utm_medium=referral\">Bradyn Trollip</a> on <a href=\"https://unsplash.com?utm_source=medium&amp;utm_medium=referral\">Unsplash</a></figcaption></figure><p>⁉\uFE0F \uD835\uDC03\uD835\uDC22\uD835\uDC1D \uD835\uDC32\uD835\uDC28\uD835\uDC2E \uD835\uDC1E\uD835\uDC2F\uD835\uDC1E\uD835\uDC2B \uD835\uDC1A\uD835\uDC2C\uD835\uDC24 \uD835\uDC32\uD835\uDC28\uD835\uDC2E\uD835\uDC2B\uD835\uDC2C\uD835\uDC1E\uD835\uDC25\uD835\uDC1F \uD835\uDC30\uD835\uDC21\uD835\uDC1A\uD835\uDC2D \uD835\uDC22\uD835\uDC2C \uD835\uDC2D\uD835\uDC21\uD835\uDC1E \uD835\uDC1D\uD835\uDC22\uD835\uDC1F\uD835\uDC1F\uD835\uDC1E\uD835\uDC2B\uD835\uDC1E\uD835\uDC27\uD835\uDC1C\uD835\uDC1E between \uD835\uDC08\uD835\uDC2D\uD835\uDC1E\uD835\uDC2B\uD835\uDC1A\uD835\uDC1B\uD835\uDC25\uD835\uDC1E \uD835\uDC1A\uD835\uDC27\uD835\uDC1D \uD835\uDC12\uD835\uDC1E\uD835\uDC2A\uD835\uDC2E\uD835\uDC1E\uD835\uDC27\uD835\uDC1C\uD835\uDC1E?</p><p>\uD83D\uDC4C Well, actually in the interface definition they are identical, but they have a very \uD835\uDC22\uD835\uDC26\uD835\uDC29\uD835\uDC28\uD835\uDC2B\uD835\uDC2D\uD835\uDC1A\uD835\uDC27\uD835\uDC2D \uD835\uDC1D\uD835\uDC22\uD835\uDC1F\uD835\uDC1F\uD835\uDC1E\uD835\uDC2B\uD835\uDC1E\uD835\uDC27\uD835\uDC1C\uD835\uDC1E \uD835\uDC22\uD835\uDC27 \uD835\uDC29\uD835\uDC1E\uD835\uDC2B\uD835\uDC1F\uD835\uDC28\uD835\uDC2B\uD835\uDC26\uD835\uDC1A\uD835\uDC27\uD835\uDC1C\uD835\uDC1E.</p><p>\uD83D\uDD30 \uD835\uDC08\uD835\uDC2D\uD835\uDC1E\uD835\uDC2B\uD835\uDC1A\uD835\uDC1B\uD835\uDC25\uD835\uDC1E (\uD835\uDC02\uD835\uDC28\uD835\uDC25\uD835\uDC25\uD835\uDC1E\uD835\uDC1C\uD835\uDC2D\uD835\uDC22\uD835\uDC28\uD835\uDC27\uD835\uDC2C):</p><ol><li>Creates a new collection on each operation you assign to it.</li><li>Performs the operations directly when they called on the collection.<br>let’s imagine you have students list and you want to filter them and map them and find an item of them:</li></ol><pre>students.filter { it.age &gt; 20 }.map { it.name }.find { it == &quot;Hussein&quot; }</pre><p>\uD83D\uDCA1 The filter operation will \uD835\uDC1C\uD835\uDC2B\uD835\uDC1E\uD835\uDC1A\uD835\uDC2D\uD835\uDC1E \uD835\uDC1A \uD835\uDC27\uD835\uDC1E\uD835\uDC30 \uD835\uDC25\uD835\uDC22\uD835\uDC2C\uD835\uDC2D and filter all the items then deliver it to the map operation and it will \uD835\uDC1C\uD835\uDC2B\uD835\uDC1E\uD835\uDC1A\uD835\uDC2D\uD835\uDC1E \uD835\uDC1A \uD835\uDC27\uD835\uDC1E\uD835\uDC30 \uD835\uDC25\uD835\uDC22\uD835\uDC2C\uD835\uDC2D and deliver it to find, which will search to find the first element with this name.</p><p>\uD83D\uDD30 \uD835\uDC12\uD835\uDC1E\uD835\uDC2A\uD835\uDC2E\uD835\uDC1E\uD835\uDC27\uD835\uDC1C\uD835\uDC1E:</p><ol><li>It operates the whole chain of operations on an item then move to the next item, this helps avoid creating collection for each step.</li><li>It start operating the collection only when a terminal operator called, so you can decide when you want the chain of operations to be executed.</li><li>Another benefit of this way to operate (item by item) the sequence can be unlimited.</li></ol><p>✅ Back to our example: let’s assume that the second element in students list is the one that will match the condition in the <em>find</em> operation, \uD835\uDC2E\uD835\uDC2C\uD835\uDC22\uD835\uDC27\uD835\uDC20 \uD835\uDC2C\uD835\uDC1E\uD835\uDC2A\uD835\uDC2E\uD835\uDC1E\uD835\uDC27\uD835\uDC1C\uD835\uDC1E, \uD835\uDC30\uD835\uDC1E \uD835\uDC28\uD835\uDC27\uD835\uDC25\uD835\uDC32 \uD835\uDC27\uD835\uDC1E\uD835\uDC1E\uD835\uDC1D \uD835\uDC2D\uD835\uDC28 \uD835\uDC28\uD835\uDC29\uD835\uDC1E\uD835\uDC2B\uD835\uDC1A\uD835\uDC2D\uD835\uDC1E \uD835\uDC2D\uD835\uDC30\uD835\uDC28 \uD835\uDC22\uD835\uDC2D\uD835\uDC1E\uD835\uDC26\uD835\uDC2C \uD835\uDC2D\uD835\uDC21\uD835\uDC1E\uD835\uDC27 \uD835\uDC2C\uD835\uDC2D\uD835\uDC28\uD835\uDC29, because the find cares only about the first element matches the condition, <strong>but if we used the normal collection API, we will operate the whole list for </strong><em>mapping</em><strong>, then again the whole list for </strong><em>filtering</em><strong>, then </strong><em>find</em><strong><em> </em>the second element and stop</strong>, a huge difference right?</p><p>❇\uFE0F This image shows another example of less operations when using sequences in the right place:</p><figure><img alt=\"\" src=\"https://cdn-images-1.medium.com/max/1024/1*GMdP8jFJgDf_D_KqQrQD6Q.jpeg\" /></figure><p>⚠\uFE0F In the end, I am not saying that \uD835\uDC12\uD835\uDC1E\uD835\uDC2A\uD835\uDC2E\uD835\uDC1E\uD835\uDC27\uD835\uDC1C\uD835\uDC1E is better than \uD835\uDC08\uD835\uDC2D\uD835\uDC1E\uD835\uDC2B\uD835\uDC1A\uD835\uDC1B\uD835\uDC25\uD835\uDC1E, but each one has it’s own use-cases, normally we use collections without any problems because they are small and operate them immediately is better to get the result faster when we want to use it.</p><img src=\"https://medium.com/_/stat?event=post.clientViewed&referrerSource=full_rss&postId=f7c2404bc0ed\" width=\"1\" height=\"1\" alt=\"\">",
                publishingDate = "referrentur"
            )
        )
    }
}