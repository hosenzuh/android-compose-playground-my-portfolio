package com.hussein.composeplayground.ui.modifiers.otherModifiers

import android.content.ClipData
import android.net.Uri
import android.util.Half
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.basicMarquee
import androidx.compose.foundation.border
import androidx.compose.foundation.content.MediaType
import androidx.compose.foundation.content.consume
import androidx.compose.foundation.content.contentReceiver
import androidx.compose.foundation.content.hasMediaType
import androidx.compose.foundation.draganddrop.dragAndDropSource
import androidx.compose.foundation.draganddrop.dragAndDropTarget
import androidx.compose.foundation.gestures.detectTapGestures
import androidx.compose.foundation.gestures.rememberTransformableState
import androidx.compose.foundation.gestures.transformable
import androidx.compose.foundation.horizontalScroll
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.magnifier
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.BasicTextField
import androidx.compose.foundation.text.input.rememberTextFieldState
import androidx.compose.foundation.text.input.setTextAndPlaceCursorAtEnd
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.KeyboardDoubleArrowDown
import androidx.compose.material.icons.filled.Pinch
import androidx.compose.material.icons.filled.SwipeLeft
import androidx.compose.material.icons.filled.ZoomInMap
import androidx.compose.material3.Button
import androidx.compose.material3.Checkbox
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.SideEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableFloatStateOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draganddrop.DragAndDropEvent
import androidx.compose.ui.draganddrop.DragAndDropTarget
import androidx.compose.ui.draganddrop.DragAndDropTransferData
import androidx.compose.ui.draganddrop.toAndroidDragEvent
import androidx.compose.ui.draw.blur
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ImageBitmap
import androidx.compose.ui.layout.approachLayout
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import coil.compose.AsyncImage
import com.hussein.composeplayground.R
import com.hussein.composeplayground.ui.base.components.ModifierExample
import com.hussein.composeplayground.ui.theme.ComposePlaygroundTheme
import com.hussein.composeplayground.ui.theme.DefaultFont
import com.hussein.composeplayground.ui.theme.HalfDefault
import okhttp3.internal.notify


@OptIn(ExperimentalFoundationApi::class)
@Composable
fun OtherModifiersScreen(
    modifier: Modifier = Modifier,
    onShowCodeSnippetClick: (imageResId: Int) -> Unit
) {
    var boxSize by remember {
        mutableStateOf(200.dp)
    }

    Column(
        modifier
            .fillMaxSize()
            .verticalScroll(rememberScrollState())
    ) {

        ModifierExample(
            title = "Basic Marquee",
            body = "This modifier slides the content of the composable if it doesn't fit in the screen, you can specify the number of iterations and the animation delay and some other parameters.",
            codeSnippetImage = null,
            onShowCodeSnippetClick = onShowCodeSnippetClick
        ) {

            Text(
                text = "This is a very very very very very very very long text that should not fit in a single line on your screen, if it fits, well congrats for such a big screen 😂",
                maxLines = 1,
                modifier = Modifier.basicMarquee()
            )

        }

        ModifierExample(
            title = "Blur",
            body = "Allows you to add blurring effect with the ability to change the blurred area shape",
            codeSnippetImage = null,
            onShowCodeSnippetClick = onShowCodeSnippetClick
        ) {
            var isBlurred by remember {
                mutableStateOf(false)
            }
            Button(onClick = { isBlurred = !isBlurred }) {
                Text(text = "Change blurring state")
            }
            Spacer(modifier = Modifier.height(HalfDefault))
            Image(
                painter = painterResource(id = R.drawable.hussein_al_zuhile),
                contentDescription = "Blurred image",
                modifier = Modifier
                    .size(200.dp)
                    .run {
                        if (isBlurred)
                            this.blur(10.dp)
                        else
                            this
                    }
            )

        }
        ModifierExample(
            title = "Drag and Drop (Source to Target)",
            body = "You can add these two modifiers to the source and destination of dragging and dropping process, You can add only one of them too.\n" +
                    "You can pass URIs, texts, Intents and Html texts. You can add flags to the DragAndDropTransferData, " +
                    "one of the flags allows you to drag and drop this data outside of your application window (View.DRAG_FLAG_GLOBAL).",
            codeSnippetImage = null,
            onShowCodeSnippetClick = onShowCodeSnippetClick
        ) {

            Row {
                // Source box
                Box(
                    Modifier
                        .weight(1f)
                        .aspectRatio(1f)
                        .border(HalfDefault, Color.Red)
                        .dragAndDropSource {
                            detectTapGestures(onLongPress = {
                                startTransfer(
                                    DragAndDropTransferData(
                                        ClipData.newPlainText(
                                            "Draggable and droppable text Label",
                                            "Dragged and Dropped text"
                                        ),
                                    )
                                )
                            })
                        }
                        .background(Color.Cyan.copy(alpha = 0.5f))
                        .padding(HalfDefault),
                    contentAlignment = Alignment.Center
                ) {
                    Column(horizontalAlignment = Alignment.CenterHorizontally) {
                        Text(text = "Long press this box to start the drag drop process")
                        Icon(
                            imageVector = Icons.Default.KeyboardDoubleArrowDown,
                            contentDescription = "Down"
                        )
                    }
                }

                Spacer(modifier = Modifier.weight(0.5f))

                // Target box
                var droppedText by remember {
                    mutableStateOf("")
                }
                var borderColor by remember {
                    mutableStateOf(Color.Red)
                }
                val dropCallback = remember {
                    object : DragAndDropTarget {
                        override fun onDrop(event: DragAndDropEvent): Boolean {
                            droppedText =
                                event.toAndroidDragEvent().clipData.getItemAt(0).text.toString()
                            return true
                        }

                        override fun onEntered(event: DragAndDropEvent) {
                            borderColor = Color.Green
                            super.onEntered(event)
                        }

                        override fun onExited(event: DragAndDropEvent) {
                            borderColor = Color.Red
                            super.onExited(event)
                        }
                    }
                }
                Box(
                    Modifier
                        .weight(1f)
                        .aspectRatio(1f)
                        .border(HalfDefault, borderColor)
                        .dragAndDropTarget({ true }, dropCallback)
                        .padding(HalfDefault),
                    contentAlignment = Alignment.Center
                ) {
                    Text(text = droppedText)
                }
            }
        }

        ModifierExample(
            title = "Content Receiver",
            body = "This modifier allows this node to receive content from other apps through Drag-and-Drop, Copy/Paste, or from the Software Keyboard.",
            codeSnippetImage = null,
            onShowCodeSnippetClick = onShowCodeSnippetClick
        ) {
            Text(text = "Copy an image or from another app and try to paste it from keyboard after clicking on the text field.")
            var images by remember {
                mutableStateOf<List<Uri>>(emptyList())
            }
            Row(horizontalArrangement = Arrangement.spacedBy(HalfDefault), modifier = Modifier.horizontalScroll(rememberScrollState())) {
                images.forEach { AsyncImage(model = it, contentDescription = null) }
            }
            Spacer(modifier = Modifier.height(HalfDefault))

            val textFieldState = rememberTextFieldState("This text field accepts images")
            BasicTextField(
                state = textFieldState,
                Modifier
                    .fillMaxWidth()
                    .padding(4.dp)
                    .contentReceiver { transferableContent ->
                        if (!transferableContent.hasMediaType(MediaType.Image)) {
                            return@contentReceiver transferableContent
                        }

                        val newImages = mutableListOf<Uri>()

                        transferableContent
                            .consume { clipData ->
                                clipData.uri?.let { newImages += it; true } ?: false
                            }
                            .also {
                                images += newImages
                            }
                    },
            )
        }
        Spacer(modifier = Modifier.weight(1f))

        ModifierExample(
            title = "Magnifier",
            body = "Allows you to magnify the content for appropriate use-cases",
            codeSnippetImage = null,
            onShowCodeSnippetClick = onShowCodeSnippetClick
        ) {
            var sourceCenter by remember {
                mutableStateOf(Offset.Zero)
            }
            var magnifierZoom by remember {
                mutableFloatStateOf(1.5f)
            }
            val transformationState =
                rememberTransformableState { zoom, panChange, _ ->
                    sourceCenter += panChange

                    magnifierZoom = (magnifierZoom * zoom).coerceIn(1.25f, 3f)
                }
            Box(
                modifier = Modifier
                    .fillMaxWidth()
                    .aspectRatio(1f)
                    .transformable(transformationState)
                    .magnifier(
                        sourceCenter = {
                            sourceCenter
                        },
                        magnifierCenter = {
                            sourceCenter.copy(y = sourceCenter.y - DefaultFont.value)
                        },
                        zoom = magnifierZoom,
                        elevation = 10.dp
                    )
            ) {
                Text(
                    text = "Small text, Small text, Small text, Small text, Small text, Small text, Small text, Small text, ",
                    modifier = Modifier,
                    style = MaterialTheme.typography.labelSmall
                )

                Row(
                    Modifier
                        .align(Alignment.Center)
                        .padding(HalfDefault),
                    horizontalArrangement = Arrangement.spacedBy(HalfDefault)
                ) {
                    Icon(imageVector = Icons.Default.Pinch, contentDescription = "Pinch")
                    Icon(imageVector = Icons.Default.SwipeLeft, contentDescription = "Pinch")
                    Text(
                        text = "Pinch or drag your fingers to play with the magnifier or zoom it",

                        )
                }
            }
        }
    }
}

@Preview
@Composable
fun OtherModifiersPreview() {
    ComposePlaygroundTheme {
        OtherModifiersScreen {}
    }
}