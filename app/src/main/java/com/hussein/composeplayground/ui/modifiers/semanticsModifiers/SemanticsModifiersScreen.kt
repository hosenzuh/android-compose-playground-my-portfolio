package com.hussein.composeplayground.ui.modifiers.semanticsModifiers

import androidx.compose.animation.animateContentSize
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.progressSemantics
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.NoteAlt
import androidx.compose.material3.Button
import androidx.compose.material3.Icon
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableFloatStateOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.drawWithContent
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.semantics.clearAndSetSemantics
import androidx.compose.ui.semantics.collapse
import androidx.compose.ui.semantics.contentDescription
import androidx.compose.ui.semantics.expand
import androidx.compose.ui.semantics.role
import androidx.compose.ui.semantics.semantics
import androidx.compose.ui.semantics.text
import androidx.compose.ui.text.AnnotatedString
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.hussein.composeplayground.ui.base.components.ModifierExample
import com.hussein.composeplayground.ui.base.components.NoteCard
import com.hussein.composeplayground.ui.theme.ComposePlaygroundTheme
import com.hussein.composeplayground.ui.theme.Default
import com.hussein.composeplayground.ui.theme.QuarterDefault
import kotlinx.coroutines.delay
import kotlinx.coroutines.isActive


@Composable
fun SemanticsModifierScreen(
    modifier: Modifier = Modifier,
    onShowCodeSnippetClick: (imageResId: Int) -> Unit
) {
    Column(modifier.verticalScroll(rememberScrollState())) {
        NoteCard {
            Text(text = "Hold the volume up and volume down buttons together for 3 seconds to enable Talkback, to understand these modifiers in a clearer way.")
        }
        ModifierExample(
            title = "Progress Semantics",
            body = "This modifier adds the progressing semantics to help the user to know that there is a progressing happening " +
                    "with (optionally) information about the progressing percent",
            codeSnippetImage = null,
            onShowCodeSnippetClick = onShowCodeSnippetClick
        ) {
            var isProgressing by rememberSaveable {
                mutableStateOf(false)
            }
            var progress by rememberSaveable {
                mutableFloatStateOf(0f)
            }
            LaunchedEffect(key1 = isProgressing) {
                progress = 0f
                if (isProgressing) {
                    while (progress < 100 && isActive) {
                        progress += 1
                        delay(500)
                    }
                }
            }

            Button(onClick = { isProgressing = !isProgressing }) {
                Text(text = "Toggle progressing, currently: $isProgressing")
            }

            Box(
                Modifier
                    .fillMaxWidth()
                    .height(100.dp)
                    .background(Color.Gray)
                    .drawWithContent {
                        drawContent()
                        drawRect(
                            color = Color.Green,
                            size = size.copy(size.width * progress / 100),
                        )
                    }
                    .progressSemantics(progress, 0f..100f)
            )
        }

        ModifierExample(
            title = "Semantics",
            body = "This modifier gives you a scope to add or edit semantic properties and/or semantic actions, \n" +
                    "This will add more information or actions for the people who need some accessibility help, or for testing purposes to access specific node.",
            codeSnippetImage = null,
            onShowCodeSnippetClick = onShowCodeSnippetClick
        ) {
            var isExpanded by rememberSaveable {
                mutableStateOf(false)
            }
            var isDescendantsMerged by rememberSaveable {
                mutableStateOf(false)
            }
            Text(
                text = "In this example we added `expand` and `collapse` actions, which will they add a state (collapsed and expanded), \n" +
                        "Also we added a content description which will describe the composable in a useful context. \n" +
                        "We added a button that will change mergeDescendants parameter, this parameter will merge the children to one meaningful item " +
                        "(For example, a button with its text and leading icon will be a single item that is clickable with merging the information in different mechanisms, you can check them on the internet), In our example it merges the text with the box, otherwise the text will be focusable."
            )
            Spacer(modifier = Modifier.height(Default))
            Button(onClick = { isDescendantsMerged = !isDescendantsMerged }) {
                Text(text = "Change mergeDescendants parameter, currently: $isDescendantsMerged")
            }
            Spacer(modifier = Modifier.height(QuarterDefault))
            Box(
                Modifier
                    .semantics(mergeDescendants = isDescendantsMerged) {
                        contentDescription = "A blue surface with expand and collapse actions."

                        if (isExpanded)
                            collapse {
                                isExpanded = false
                                true
                            }
                        else {
                            expand {
                                isExpanded = true
                                true
                            }
                        }
                    }
                    .animateContentSize()
                    .height(
                        if (isExpanded) 200.dp else 100.dp
                    )
                    .fillMaxWidth()
                    .background(Color.Blue),
            ) {
                Text(text = "Hello box")
            }
        }
        ModifierExample(
            title = "Clear and set Semantics",
            body = "This modifier similar to Semantics modifier, but it clears all the descendants semantics and set a new one",
            codeSnippetImage = null,
            onShowCodeSnippetClick = onShowCodeSnippetClick
        ) {

            Text(
                text = "As you will see in this button, The descendants semantics (The text in this case) is not in the semantics, " +
                        "instead the text that we defined in the modifier is being called on focus."
            )


            Button(
                onClick = { /*TODO*/ },
                Modifier.clearAndSetSemantics {
                    contentDescription =
                        "This is the overridden content description: This button contains a text and an icon."
                }
            ) {
                Icon(
                    imageVector = Icons.Default.NoteAlt,
                    contentDescription = "This content description won't be added to the button semantics"
                )
                Text(text = "This text won't be called when we focus on the button")

            }
        }
    }
}

@Preview
@Composable
fun SemanticsModifierScreenPreview() {
    ComposePlaygroundTheme {
        SemanticsModifierScreen {

        }
    }
}