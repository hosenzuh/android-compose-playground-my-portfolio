package com.hussein.composeplayground.ui.modifiers.scrollModifiers

import androidx.compose.animation.core.Animatable
import androidx.compose.animation.core.spring
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.OverscrollEffect
import androidx.compose.foundation.background
import androidx.compose.foundation.gestures.Orientation
import androidx.compose.foundation.gestures.rememberScrollableState
import androidx.compose.foundation.gestures.scrollable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.overscroll
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.input.nestedscroll.NestedScrollConnection
import androidx.compose.ui.input.nestedscroll.NestedScrollSource
import androidx.compose.ui.input.nestedscroll.nestedScroll
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.IntOffset
import androidx.compose.ui.unit.Velocity
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.hussein.composeplayground.ui.base.components.ModifierExample
import com.hussein.composeplayground.ui.theme.ComposePlaygroundTheme
import com.hussein.composeplayground.ui.theme.HalfDefault
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import kotlin.math.abs
import kotlin.math.roundToInt
import kotlin.math.sign

@OptIn(ExperimentalMaterial3Api::class, ExperimentalFoundationApi::class)
@Composable
fun ScrollModifiersScreen(
    modifier: Modifier = Modifier,
    onShowCodeSnippetClick: (imageResId: Int) -> Unit
) {
    Column(modifier.verticalScroll(rememberScrollState())) {
        ModifierExample(
            title = "Scrollable",
            body = "This modifier needs a scrollable state and orientation, " +
                    "note that this modifier doesn't handle any scrollable effect, " +
                    "it only gives you the delta after a scrolling event and you have to handle the effect by yourself.",
            codeSnippetImage = null,
            onShowCodeSnippetClick = onShowCodeSnippetClick
        ) {
            var offset by remember { mutableStateOf(0f) }
            val scrollState = rememberScrollableState { delta ->
                offset += delta
                delta
            }
            Text(text = "Scroll over the text horizontally to see the offset value changes by the delta value.")
            Spacer(modifier = Modifier.height(HalfDefault))
            Box(
                modifier = Modifier
                    .size(200.dp)
                    .scrollable(
                        state = scrollState,
                        orientation = Orientation.Vertical
                    )
                    .background(Color.White)
            ) {
                Text(
                    text = "Offset: ${offset.roundToInt()}",
                    modifier = Modifier.align(Alignment.Center)
                )
            }
        }

        ModifierExample(
            title = "Nested Scroll",
            body = "This modifier has two roles, first it works as an observer and/or modifier of the children nested scrolling using the NestedScrollConnection interface, \n" +
                    "And optionally if it supports scrolling events, it can dispatch it to the elements above, using NestedScrollDispatcher.",
            codeSnippetImage = null,
            onShowCodeSnippetClick = onShowCodeSnippetClick
        ) {
            // here we use LazyColumn that has build-in nested scroll, but we want to act like a
            // parent for this LazyColumn and participate in its nested scroll.
            // Let's make a collapsing toolbar for LazyColumn
            val toolbarHeight = 48.dp
            val toolbarHeightPx = with(LocalDensity.current) { toolbarHeight.roundToPx().toFloat() }
            // our offset to collapse toolbar
            val toolbarOffsetHeightPx = remember { mutableStateOf(0f) }
            // now, let's create connection to the nested scroll system and listen to the scroll
            // happening inside child LazyColumn
            val nestedScrollConnection = remember {
                object : NestedScrollConnection {
                    override fun onPreScroll(
                        available: Offset,
                        source: NestedScrollSource
                    ): Offset {
                        // try to consume before LazyColumn to collapse toolbar if needed, hence pre-scroll
                        val delta = available.y
                        val newOffset = toolbarOffsetHeightPx.value + delta
                        toolbarOffsetHeightPx.value = newOffset.coerceIn(-toolbarHeightPx, 0f)
                        // here's the catch: let's pretend we consumed 0 in any case, since we want
                        // LazyColumn to scroll anyway for good UX
                        // We're basically watching scroll without taking it
                        return Offset.Zero
                    }
                }
            }
            Text(
                text = "In this example we are observing the scrolling in the column that is dispatched from LazyColumn (LazyColumn has built-in dispatching implementation, \n" +
                        "Then used this observing to collapse and expand our white box."
            )
            Column(
                Modifier
                    .height(400.dp)
                    // attach as a parent to the nested scroll system
                    .nestedScroll(nestedScrollConnection)
            ) {
                Box(
                    modifier = Modifier
                        .fillMaxWidth()
                        .height(toolbarHeight + abs(toolbarOffsetHeightPx.value).dp)
                        .background(
                            Color.White
                        ),
                    contentAlignment = Alignment.Center
                ) { Text("toolbar height is $toolbarHeight + ${abs(toolbarOffsetHeightPx.value).dp}") }

                // our list with build in nested scroll support that will notify us about its scroll
                LazyColumn() {
                    items(100) { index ->
                        Text(
                            "I'm item $index", modifier = Modifier
                                .fillMaxWidth()
                                .padding(16.dp)
                        )
                    }
                }
            }
        }

        ModifierExample(
            title = "Overscroll",
            body = "This modifier allows you to add an overscroll observation to add an effect to this situation, see the code to understand the usage.",
            codeSnippetImage = null,
            onShowCodeSnippetClick = onShowCodeSnippetClick
        ) {

            @OptIn(ExperimentalFoundationApi::class)
            // our custom offset overscroll that offset the element it is applied to when we hit the bound
            // on the scrollable container.
            class OffsetOverscrollEffect(val scope: CoroutineScope) : OverscrollEffect {
                private val overscrollOffset = Animatable(0f)

                override fun applyToScroll(
                    delta: Offset,
                    source: NestedScrollSource,
                    performScroll: (Offset) -> Offset
                ): Offset {
                    // in pre scroll we relax the overscroll if needed
                    // relaxation: when we are in progress of the overscroll and user scrolls in the
                    // different direction = substract the overscroll first
                    val sameDirection = sign(delta.y) == sign(overscrollOffset.value)
                    val consumedByPreScroll =
                        if (abs(overscrollOffset.value) > 0.5 && !sameDirection) {
                            val prevOverscrollValue = overscrollOffset.value
                            val newOverscrollValue = overscrollOffset.value + delta.y
                            if (sign(prevOverscrollValue) != sign(newOverscrollValue)) {
                                // sign changed, coerce to start scrolling and exit
                                scope.launch { overscrollOffset.snapTo(0f) }
                                Offset(x = 0f, y = delta.y + prevOverscrollValue)
                            } else {
                                scope.launch {
                                    overscrollOffset.snapTo(overscrollOffset.value + delta.y)
                                }
                                delta.copy(x = 0f)
                            }
                        } else {
                            Offset.Zero
                        }
                    val leftForScroll = delta - consumedByPreScroll
                    val consumedByScroll = performScroll(leftForScroll)
                    val overscrollDelta = leftForScroll - consumedByScroll
                    // if it is a drag, not a fling, add the delta left to our over scroll value
                    if (abs(overscrollDelta.y) > 0.5 && source == NestedScrollSource.Drag) {
                        scope.launch {
                            // multiply by 0.1 for the sake of parallax effect
                            overscrollOffset.snapTo(overscrollOffset.value + overscrollDelta.y * 0.1f)
                        }
                    }
                    return consumedByPreScroll + consumedByScroll
                }

                override suspend fun applyToFling(
                    velocity: Velocity,
                    performFling: suspend (Velocity) -> Velocity
                ) {
                    val consumed = performFling(velocity)
                    // when the fling happens - we just gradually animate our overscroll to 0
                    val remaining = velocity - consumed
                    overscrollOffset.animateTo(
                        targetValue = 0f,
                        initialVelocity = remaining.y,
                        animationSpec = spring()
                    )
                }

                override val isInProgress: Boolean
                    get() = overscrollOffset.value != 0f

                // as we're building an offset modifiers, let's offset of our value we calculated
                override val effectModifier: Modifier = Modifier.offset {
                    IntOffset(x = 0, y = overscrollOffset.value.roundToInt())
                }
            }


            val offset = remember { mutableStateOf(0f) }
            val scope = rememberCoroutineScope()
            // Create the overscroll controller
            val overscroll = remember(scope) { OffsetOverscrollEffect(scope) }
            // let's build a scrollable that scroll until -512 to 512
            val scrollStateRange = (-512f).rangeTo(512f)
            Box(
                Modifier
                    .size(150.dp)
                    .scrollable(
                        orientation = Orientation.Vertical,
                        state = rememberScrollableState { delta ->
                            // use the scroll data and indicate how much this element consumed.
                            val oldValue = offset.value
                            // coerce to our range
                            offset.value = (offset.value + delta).coerceIn(scrollStateRange)

                            offset.value - oldValue // indicate that we consumed what's needed
                        },
                        // pass the overscroll to the scrollable so the data is updated
                        overscrollEffect = overscroll
                    )
                    .background(Color.LightGray),
                contentAlignment = Alignment.Center
            ) {
                Text(
                    offset.value.roundToInt().toString(),
                    style = TextStyle(fontSize = 32.sp),
                    modifier = Modifier
                        // show the overscroll only on the text, not the containers (just for fun)
                        .overscroll(overscroll)
                )
            }
        }

        ModifierExample(
            title = "On Pre Rotary Scroll Event and on Rotary Scroll Event",
            body = "These two modifiers allows you to detect the rotation scrolling that happens in smart watches,",
            codeSnippetImage = null,
            onShowCodeSnippetClick = onShowCodeSnippetClick
        ) {
            Text(text = "No example exist")
        }
    }
}

@Preview
@Composable
fun ScrollModifiersScreenPreview() {
    ComposePlaygroundTheme {
        ScrollModifiersScreen {}
    }
}