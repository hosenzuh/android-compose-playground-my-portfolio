package com.hussein.composeplayground.ui.modifiers.graphicModifiers

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.ColumnScope
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.ClickableText
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.drawBehind
import androidx.compose.ui.draw.drawWithCache
import androidx.compose.ui.draw.drawWithContent
import androidx.compose.ui.draw.paint
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.geometry.CornerRadius
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.geometry.Size
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.graphics.TileMode
import androidx.compose.ui.graphics.drawscope.DrawScope
import androidx.compose.ui.graphics.painter.Painter
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.ExperimentalTextApi
import androidx.compose.ui.text.UrlAnnotation
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextDecoration
import androidx.compose.ui.text.withStyle
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.zIndex
import com.hussein.composeplayground.R
import com.hussein.composeplayground.ui.base.components.ModifierExample
import com.hussein.composeplayground.ui.theme.Default
import com.hussein.composeplayground.ui.theme.HalfDefault
import com.hussein.composeplayground.util.IntentDirection
import com.hussein.composeplayground.util.IntentManager.startIntentDirection


@OptIn(ExperimentalTextApi::class)
@Composable
fun ColumnScope.DrawingModifiers(onShowCodeSnippetClick: (Int) -> Unit) {
    Text(
        text = "Drawing Modifiers",
        style = MaterialTheme.typography.titleLarge.copy(fontWeight = FontWeight.Bold)
    )
    Spacer(modifier = Modifier.height(Default))
    ModifierExample(
        title = "Draw With Content",
        body = "This modifier give you the ability to draw something before drawing the content or after, please notice that you have to call drawContent() to avoid losing it",
        codeSnippetImage = R.drawable.modifier_draw_with_content,
        documentationLink = "https://developer.android.com/reference/kotlin/androidx/compose/ui/draw/package-summary#drawwithcontent",
        onShowCodeSnippetClick = onShowCodeSnippetClick,
    ) {
        Text(text = "Look at this text that contains two rectangles one is drawn before (Behind) and another after (In front of) the content",
            color = Color.Black,
            modifier = Modifier
                .background(Color.White)
                .drawWithContent {
                    drawRect(Color.Yellow, size = size.copy(width = size.width / 2))
                    drawContent()
                    drawRect(
                        Color.Yellow,
                        alpha = 0.8f,
                        topLeft = Offset(size.width / 2, 0f),
                        size = size.copy(width = size.width / 2)
                    )
                }
                .padding(HalfDefault)
        )
    }
    ModifierExample(
        title = "Draw Behind",
        body = "This modifier is similar to the previous one but in it you only define the (Behind Content) part",
        codeSnippetImage = R.drawable.modifier_draw_behiend,
        documentationLink = "https://developer.android.com/reference/kotlin/androidx/compose/ui/draw/package-summary#drawbehind",
        onShowCodeSnippetClick = onShowCodeSnippetClick,
    ) {
        Text(
            text = "I used the drawBehind modifier to draw a rounded rectangle behind the content",
            color = Color.Black,
            modifier = Modifier
                .drawBehind {
                    drawRoundRect(
                        Color.Yellow,
                        cornerRadius = CornerRadius(Default.value, Default.value)
                    )
                }
                .padding(HalfDefault)
        )
    }
    ModifierExample(
        title = "Draw With Cache",
        body = "This modifier caches the objects created inside of it as long as the drawing area not changed or any state objects that are read have not changed, this will improve the performance as it avoids the need to reallocate the objects created for drawing",
        codeSnippetImage = R.drawable.modifier_draw_with_cache,
        documentationLink = "https://developer.android.com/jetpack/compose/graphics/draw/modifiers#drawwithcache",
        onShowCodeSnippetClick = onShowCodeSnippetClick,
    ) {
        Text(
            text = "We cache the brush object in this modifier",
            color = Color.Black,
            modifier = Modifier
                .fillMaxWidth()
                .drawWithCache {
                    val brush = Brush.linearGradient(
                        listOf(Color.Yellow, Color.Transparent),
                        tileMode = TileMode.Mirror
                    )
                    onDrawBehind {
                        drawRect(brush)
                    }
                }
                .padding(HalfDefault)
        )
    }
    ModifierExample(
        title = "Paint",
        body = "This modifier allows you to draw using Painter, this gives you a draw scope with additional features such as applying color filters.\n" +
                "For example I drew a Red Circle in this box, then applied a color filter to it to be Yellow\n" +
                "There is a parameter in this modifier called contentScale, which define what will happen if the painting coordinates and the composable boundaries not the same (Like for images: Fit,Crop,FitHeight etc..)",
        codeSnippetImage = R.drawable.modifier_paint,
        onShowCodeSnippetClick = onShowCodeSnippetClick
    ) {
        val painter = object : Painter() {
            override val intrinsicSize: Size
                get() = Size(100.dp.value, 100.dp.value)

            override fun DrawScope.onDraw() {
                drawCircle(Color.Red)
            }

        }
        Box(
            modifier = Modifier
                .paint(
                    painter,
                    colorFilter = ColorFilter.lighting(Color.White, Color.Green),
                )
        )
    }
    ModifierExample(
        title = "Shadow",
        body = "This modifier uses graphicLayer internally to create a shadow",
        codeSnippetImage = R.drawable.modifier_shadow,
        onShowCodeSnippetClick = onShowCodeSnippetClick
    ) {
        Box(
            Modifier
                .fillMaxWidth(0.3f)
                .aspectRatio(1f)
                .padding(Default)
                .shadow(
                    elevation = 8.dp,
                    shape = RoundedCornerShape(16.dp),
                    ambientColor = Color.Red,
                    spotColor = Color.Blue
                )
        )
    }
    ModifierExample(
        title = "Safe Drawing Padding",
        body = "This modifier adds padding of the System UI components that affect the drawing area if needed\n" +
                "This includes the insets areas where content may be covered by other drawn content. This includes all systemBars, displayCutout, and ime.",
        codeSnippetImage = R.drawable.modifier_safe_drawing_padding,
        onShowCodeSnippetClick = onShowCodeSnippetClick
    ) {
        val text = buildAnnotatedString {
            append(
                "It's hard to show the use-case of this modifier when using the defaults of Material3 in your components, especially the large ones such as Scaffold and TopAppBar\n" +
                        "cause these components already add the required padding of system UI components\n" +
                        "You can override these default insets to examine this modifier\n"
            )
            appendLine()
            append("You can check this page to know more about these ")
            pushUrlAnnotation(UrlAnnotation("https://developer.android.com/jetpack/compose/layouts/insets"))
            withStyle(
                MaterialTheme.typography.bodyMedium.toSpanStyle()
                    .copy(Color.Blue, textDecoration = TextDecoration.Underline)
            ) {
                append("WindowInsets")
            }
            pop()
        }
        val context = LocalContext.current
        ClickableText(text = text) { offset ->
            if (text.getUrlAnnotations(offset, offset)
                    .first().item.url == "https://developer.android.com/jetpack/compose/layouts/insets"
            ) {
                context.startIntentDirection(
                    IntentDirection.ViewUrl("https://developer.android.com/jetpack/compose/layouts/insets")
                )
            }
        }
    }
    ModifierExample(
        title = "Z Index",
        body = "This modifier decide which composable above the others, the higher value means higher composable\n" +
                "In the example below, we give the first item zIndex 3 and the second 2 and third 1\n" +
                "though the default in Box composable that the last written composable inside it is the highest one, " +
                "but by specifying the zIndex, it's override this behavior.\n" +
                "This is just an example, you can use it in different cases for sure.",
        codeSnippetImage = R.drawable.modifier_z_index,
        onShowCodeSnippetClick = onShowCodeSnippetClick
    ) {
        Box(
            Modifier
                .fillMaxWidth()
                .aspectRatio(1f)
        ) {
            Box(
                Modifier
                    .fillMaxWidth(0.4f)
                    .aspectRatio(1f)
                    .align(Alignment.TopStart)
                    .background(Color.Red)
                    .zIndex(3f)
            )
            Box(
                Modifier
                    .fillMaxWidth(0.4f)
                    .aspectRatio(1f)
                    .align(Alignment.Center)
                    .background(Color.Blue)
                    .zIndex(2f)
            )
            Box(
                Modifier
                    .fillMaxWidth(0.4f)
                    .aspectRatio(1f)
                    .align(Alignment.BottomEnd)
                    .background(Color.Green)
                    .zIndex(1f)
            )
        }
    }
}

@Preview()
@Composable
fun DrawingModifiersPreview() {
    Column(Modifier.verticalScroll(rememberScrollState())) {
        DrawingModifiers({})
    }
}