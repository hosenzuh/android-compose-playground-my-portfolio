package com.hussein.composeplayground.ui.main

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.animateColor
import androidx.compose.animation.core.RepeatMode
import androidx.compose.animation.core.infiniteRepeatable
import androidx.compose.animation.core.rememberInfiniteTransition
import androidx.compose.animation.core.tween
import androidx.compose.animation.fadeIn
import androidx.compose.animation.fadeOut
import androidx.compose.animation.scaleIn
import androidx.compose.animation.scaleOut
import androidx.compose.animation.slideInHorizontally
import androidx.compose.animation.slideOutHorizontally
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.automirrored.filled.ArrowBack
import androidx.compose.material.icons.filled.Menu
import androidx.compose.material3.CenterAlignedTopAppBar
import androidx.compose.material3.DrawerValue
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.ModalNavigationDrawer
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.material3.rememberDrawerState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.input.nestedscroll.NestedScrollConnection
import androidx.compose.ui.input.nestedscroll.NestedScrollSource
import androidx.compose.ui.tooling.preview.Preview
import androidx.core.splashscreen.SplashScreen.Companion.installSplashScreen
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navOptions
import androidx.navigation.toRoute
import com.hussein.composeplayground.domain.base.State
import com.hussein.composeplayground.ui.modifiers.actionModifiers.ActionModifiersScreen
import com.hussein.composeplayground.ui.modifiers.alignmentModifiers.AlignmentModifiersScreen
import com.hussein.composeplayground.ui.modifiers.animationModifiers.AnimationModifiersScreen
import com.hussein.composeplayground.ui.articles.ArticlesScreen
import com.hussein.composeplayground.ui.articles.ArticlesViewModel
import com.hussein.composeplayground.ui.base.components.CodeSnippetImage
import com.hussein.composeplayground.ui.modifiers.borderModifiers.BorderModifiersScree
import com.hussein.composeplayground.ui.modifiers.focusModifiers.FocusModifiersScreen
import com.hussein.composeplayground.ui.modifiers.graphicModifiers.GraphicModifiersScreen
import com.hussein.composeplayground.ui.home.HomeScreen
import com.hussein.composeplayground.ui.modifiers.keyboardModifiers.KeyboardModifiersScreen
import com.hussein.composeplayground.ui.modifiers.layoutModifiers.LayoutModifiersScreen
import com.hussein.composeplayground.ui.main.NavDestination.Home
import com.hussein.composeplayground.ui.modifiers.pointerModifiers.PointerModifiersScreen
import com.hussein.composeplayground.ui.modifiers.positionModifiers.PositionModifiersScreen
import com.hussein.composeplayground.ui.projects.details.ProjectDetailsScreen
import com.hussein.composeplayground.ui.projects.list.ProjectsScreen
import com.hussein.composeplayground.ui.projects.list.ProjectsViewModel
import com.hussein.composeplayground.ui.modifiers.semanticsModifiers.SemanticsModifierScreen
import com.hussein.composeplayground.ui.theme.ComposePlaygroundTheme
import com.hussein.composeplayground.ui.theme.HalfDefault
import com.hussein.composeplayground.ui.workExperities.WorkExperiencesScreen
import com.hussein.composeplayground.ui.workExperities.WorkExperiencesViewModel
import com.hussein.composeplayground.util.ConnectionManager
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.transform
import kotlinx.coroutines.flow.transformLatest
import kotlinx.coroutines.launch
import javax.inject.Inject

@AndroidEntryPoint
class MainActivity : ComponentActivity() {

    @Inject
    lateinit var connectionManager: ConnectionManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        installSplashScreen().apply {

        }

        lifecycle.addObserver(connectionManager)

        setContent {
            Main()
        }
    }
}

@OptIn(ExperimentalMaterial3Api::class, ExperimentalCoroutinesApi::class)
@Composable
private fun Main() {

    val coroutineScope = rememberCoroutineScope()
    val drawerState = rememberDrawerState(initialValue = DrawerValue.Closed)
    val mainViewModel: MainViewModel = viewModel()
    val navController = rememberNavController()
    val topAppBarScrollBehavior = TopAppBarDefaults.enterAlwaysScrollBehavior()

    val currentNavDestination = Home
    // TODO: Check this if it will be solved in newer versions
//    val currentNavDestination:NavDestination by navController
//        .currentBackStackEntryFlow
//        .map<_, NavDestination> { it.toRoute() }
//        .collectAsState(initial = Home)

    var currentCodeSnippet by rememberSaveable {
        mutableStateOf<Int?>(null)
    }
    val onShowCodeSnippetClick = { codeImageRes: Int -> currentCodeSnippet = codeImageRes }

    ComposePlaygroundTheme {
        var isBottomBarVisible by remember {
            mutableStateOf(true)
        }
        val bottomBarScrollConnection = remember {
            object : NestedScrollConnection {
                override fun onPreScroll(available: Offset, source: NestedScrollSource): Offset {
                    isBottomBarVisible = available.y > 0
                    return super.onPreScroll(available, source)
                }
            }
        }

        ModalNavigationDrawer(
            drawerState = drawerState,
            gesturesEnabled = currentCodeSnippet == null,
            drawerContent = {
                Drawer(
                    navController = navController,
                    onItemClicked = { clickedDestination ->
                        navController.navigate(
                            clickedDestination,
                            navOptions { launchSingleTop = true })
                        coroutineScope.launch {
                            drawerState.close()
                        }
                    }, onBackClicked = {
                        drawerState.close()
                    })
            }) {
            Scaffold(
                modifier = Modifier
//                    .nestedScroll(bottomBarScrollConnection).run {
//                        if (currentNavDestination?.title != null) {
//                            then(nestedScroll(topAppBarScrollBehavior.nestedScrollConnection))
//                        } else this
//                    }
                , bottomBar = {
//                    AnimatedVisibility(
//                        visible = isBottomBarVisible,
//                        enter = slideInVertically { it } + expandVertically(),
//                        exit = slideOutVertically { it } + shrinkVertically()
//                    ) {
                    BottomBar()
//                    }
                }, topBar = {
                    currentNavDestination?.title?.let { currentTitle ->

                        val infiniteTransition = rememberInfiniteTransition(label = "")

                        val topBarContentColor by infiniteTransition.animateColor(
                            initialValue = MaterialTheme.colorScheme.primary,
                            targetValue = MaterialTheme.colorScheme.inversePrimary,
                            animationSpec = infiniteRepeatable(
                                tween(1000),
                                repeatMode = RepeatMode.Reverse
                            ), label = ""
                        )
                        CenterAlignedTopAppBar(
                            title = {
                                Text(
                                    text = currentTitle,
                                )
                            },
//                            colors = TopAppBarDefaults.centerAlignedTopAppBarColors(
//                                titleContentColor = topBarContentColor,
//                                navigationIconContentColor = topBarContentColor,
//                            )
//                            ,
                            scrollBehavior = topAppBarScrollBehavior,
                            navigationIcon = {
                                if (currentNavDestination?.isTopLevelDestination == true) {
                                    IconButton(onClick = { coroutineScope.launch { drawerState.open() } }) {
                                        Icon(
                                            imageVector = Icons.Default.Menu,
                                            contentDescription = "Open menu"
                                        )
                                    }
                                } else {
                                    IconButton(onClick = { navController.navigateUp() }) {
                                        Icon(
                                            imageVector = Icons.AutoMirrored.Default.ArrowBack,
                                            contentDescription = "Navigate back"
                                        )
                                    }
                                }
                            }
                        )
                    }
                }) { contentPadding ->
                Box {
                    MainNavHost(
                        contentPadding = contentPadding,
                        navController = navController,
                        onShowCodeSnippetClick = onShowCodeSnippetClick
                    )
                    AnimatedVisibility(
                        visible = currentCodeSnippet != null,
                        enter = fadeIn(),
                        exit = fadeOut()
                    ) {
                        CodeSnippetImage(currentCodeSnippet) {
                            currentCodeSnippet = null
                        }
                    }
                }
            }
        }
    }
}

@Preview
@Composable
fun MainPreview() {
    Main()
}