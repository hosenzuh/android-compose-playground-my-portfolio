package com.hussein.composeplayground.ui.modifiers.focusModifiers

import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.focusGroup
import androidx.compose.foundation.focusable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.ElevatedButton
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateListOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment.Companion.CenterHorizontally
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.FocusDirection
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.focus.FocusState
import androidx.compose.ui.focus.focusProperties
import androidx.compose.ui.focus.focusRequester
import androidx.compose.ui.focus.focusTarget
import androidx.compose.ui.focus.onFocusChanged
import androidx.compose.ui.focus.onFocusEvent
import androidx.compose.ui.graphics.Color.Companion.Black
import androidx.compose.ui.graphics.Color.Companion.Blue
import androidx.compose.ui.graphics.Color.Companion.Green
import androidx.compose.ui.graphics.Color.Companion.Red
import androidx.compose.ui.graphics.Color.Companion.Yellow
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.hussein.composeplayground.R
import com.hussein.composeplayground.ui.base.components.ModifierExample
import com.hussein.composeplayground.ui.base.components.NoteCard
import com.hussein.composeplayground.ui.theme.ComposePlaygroundTheme
import com.hussein.composeplayground.ui.theme.Default
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

@OptIn(ExperimentalComposeUiApi::class, ExperimentalFoundationApi::class)
@Composable
fun FocusModifiersScreen(
    modifier: Modifier = Modifier,
    onShowCodeSnippetClick: (imageResId: Int) -> Unit
) {
    Column(modifier = modifier.verticalScroll(rememberScrollState())) {
        NoteCard {
            Text(
                text = "There are four focus states to any component: Active, Active Parent, Captured and Inactive\n" +
                        "- Active means this is a focused component and it receives the key events\n" +
                        "- Active Parent means one of the descendants of the focusable component is Active\n" +
                        "- Captured means this component has focus and won't give up this focus, you can use focus requester to free this type of focus\n" +
                        "- Inactive means this component won't receive any key events, it does not has focus nor one of its children"
            )
        }

        ModifierExample(
            title = "Focus Requester",
            body = "We pass a FocusRequester object to this modifier, when this object request focus," +
                    " the composable that passed the object to its modifier will be focused if it's focusable\n" +
                    "It can also capture the focus, free the focus, or save the ",
            codeSnippetImage = R.drawable.modifier_focus_requster,
            onShowCodeSnippetClick = onShowCodeSnippetClick
        ) {

            var color by remember { mutableStateOf(Black) }
            val focusRequester = remember {
                FocusRequester()
            }
            Box(
                Modifier
                    .fillMaxWidth(0.25f)
                    .aspectRatio(1f)
                    .align(CenterHorizontally)
                    .border(2.dp, color)
                    .focusRequester(focusRequester)
                    // The onFocusChanged should be added BEFORE the focusTarget that is being observed.
                    .onFocusChanged { color = if (it.isFocused) Green else Black }
                    .focusTarget()
                    .clickable {
                        focusRequester.requestFocus()
                    }
            )
        }
        ModifierExample(
            title = "On Focus Change Listener or On Focus Event Listener",
            body = "You can observe the focus state events to this composable using this modifier\n" +
                    "In this example we write the state of the text fields in its fields",
            codeSnippetImage = R.drawable.modifier_on_focus_changed_listener,
            onShowCodeSnippetClick = onShowCodeSnippetClick
        ) {

            var firstTextFieldFocusState by remember {
                mutableStateOf<FocusState?>(null)
            }
            var secondTextFieldFocusState by remember {
                mutableStateOf<FocusState?>(null)
            }
            val focusRequester = remember {
                FocusRequester()
            }
            val coroutineScope = rememberCoroutineScope()

            ElevatedButton(onClick = {
                focusRequester.requestFocus()
                focusRequester.captureFocus()
                coroutineScope.launch {
                    delay(2000)
                    focusRequester.freeFocus()
                }
            }) {
                Text(text = "Capture Focus for 2 Seconds")
            }
            TextField(value = when {
                firstTextFieldFocusState?.isCaptured == true -> "Captured"
                firstTextFieldFocusState?.isFocused == true -> "Focused"
                else -> "Not Focused"
            },
                onValueChange = {},
                modifier = Modifier
                    .onFocusChanged { focusState ->
                        firstTextFieldFocusState = focusState
                    }
                    .focusRequester(focusRequester))

            TextField(value = when {
                secondTextFieldFocusState?.isCaptured == true -> "Captured"
                secondTextFieldFocusState?.isFocused == true -> "Focused"
                else -> "Not Focused"
            }, onValueChange = {}, modifier = Modifier
                .onFocusEvent { focusState ->
                    secondTextFieldFocusState = focusState
                })
        }
        ModifierExample(
            title = "Focus Target, Focusable",
            body = "These modifier make this component focusable\n" +
                    "Note that if you want to observe the focus events, you must add the observer modifier before the ",
            codeSnippetImage = R.drawable.modifier_focus_target,
            onShowCodeSnippetClick = onShowCodeSnippetClick
        ) {

            var color by remember { mutableStateOf(Black) }
            val focusRequester = remember {
                FocusRequester()
            }
            Box(
                Modifier
                    .fillMaxWidth(0.25f)
                    .aspectRatio(1f)
                    .align(CenterHorizontally)
                    .border(2.dp, color)
                    .focusRequester(focusRequester)
                    // The onFocusChanged should be added BEFORE the focusTarget that is being observed.
                    .onFocusChanged { color = if (it.isFocused) Green else Black }
                    .focusTarget()
                    .clickable {
                        focusRequester.requestFocus()
                    }
            )
        }
        ModifierExample(
            title = "Focus Properties",
            body = "This modifier allows you to specify properties that are accessible to focusTargets further down the modifier chain or on child layout nodes.\n" +
                    "In this example we made the next action move to the third TextField",
            codeSnippetImage = R.drawable.modifier_focus_properties,
            onShowCodeSnippetClick = onShowCodeSnippetClick
        ) {
            val requester = remember {
                FocusRequester()
            }
            Column(Modifier.focusProperties {
                next = requester
            }) {
                TextField(
                    value = "",
                    onValueChange = {},
                    keyboardOptions = KeyboardOptions(imeAction = ImeAction.Next)
                )

                TextField(value = "", onValueChange = {})

                TextField(value = "", onValueChange = {}, Modifier.focusRequester(requester))
            }
        }
        ModifierExample(
            title = "Focus Group",
            body = "Creates a focus group or marks this component as a focus group. This means that when we move focus using the keyboard or programmatically using FocusManager.moveFocus(), the items within the focus group will be given a higher priority before focus moves to items outside the focus group.",
            codeSnippetImage = R.drawable.modifier_focus_group,
            onShowCodeSnippetClick = onShowCodeSnippetClick
        ) {
            val focusManager = LocalFocusManager.current

            val listOfIds = listOf<String>(
                "FirstColumn",
                "Button 1 1",
                "Button 1 2",
                "SecondColumn",
                "Button 2 1",
                "Button 2 2",
            )
            val currentFocusedIds = remember {
                mutableStateListOf<String>()
            }

            println(currentFocusedIds.joinToString { it })
            val focusRequester = remember {
                FocusRequester()
            }
            ElevatedButton(onClick = { focusRequester.requestFocus() }) {
                Text(text = "Focus on the first column")
            }
            ElevatedButton(onClick = { focusManager.moveFocus(FocusDirection.Next) }) {
                Text(text = "Move To Next Focus")
            }
            Row {
                Column(
                    Modifier
                        .focusGroup()
                        .background(if (currentFocusedIds.contains(listOfIds[0])) Green else Red)
                        .onFocusEvent {
                            if (it.isFocused && listOfIds[0] !in currentFocusedIds)
                                currentFocusedIds.add(listOfIds[0])
                            else if (!it.isFocused) currentFocusedIds.remove(listOfIds[0])
                        }
                        .focusRequester(focusRequester)
                ) {
                    Button(
                        {},
                        Modifier
                            .onFocusEvent {
                                if (it.isFocused && listOfIds[1] !in currentFocusedIds) currentFocusedIds.add(
                                    listOfIds[1]
                                )
                                else if (!it.isFocused) currentFocusedIds.remove(listOfIds[1])
                            }
                            .focusable(),
                        colors = ButtonDefaults.buttonColors(
                            containerColor = if (currentFocusedIds.contains(listOfIds[1])) Yellow else Blue
                        ),
                    ) { Text("Row1 Col1") }
                    Button(
                        {},
                        Modifier
                            .onFocusEvent {
                                if (it.isFocused && listOfIds[2] !in currentFocusedIds) currentFocusedIds.add(
                                    listOfIds[2]
                                )
                                else if (!it.isFocused) currentFocusedIds.remove(listOfIds[2])
                            }
                            .focusable(),
                        colors = ButtonDefaults.buttonColors(
                            containerColor = if (currentFocusedIds.contains(listOfIds[2])) Yellow else Blue
                        ),
                    ) { Text("Row2 Col1") }
                }
                Spacer(modifier = Modifier.width(Default))
                Column(
                    Modifier
                        .focusGroup()
                        .onFocusEvent {
                            if (it.isFocused && listOfIds[3] !in currentFocusedIds) currentFocusedIds.add(
                                listOfIds[3]
                            )
                            else if (!it.isFocused) currentFocusedIds.remove(listOfIds[3])
                        }
                        .background(
                            if (currentFocusedIds.contains(listOfIds[3])) Green else Red
                        )
                ) {
                    Button(
                        {},
                        Modifier
                            .onFocusEvent {
                                if (it.isFocused && listOfIds[4] !in currentFocusedIds) currentFocusedIds.add(
                                    listOfIds[4]
                                )
                                else if (!it.isFocused) currentFocusedIds.remove(listOfIds[4])
                            }
                            .focusable(),
                        colors = ButtonDefaults.buttonColors(
                            containerColor = if (currentFocusedIds.contains(
                                    listOfIds[4]
                                )
                            ) Yellow else Blue
                        ),
                    ) { Text("Row1 Col2") }
                    Button(
                        {},
                        Modifier
                            .onFocusEvent {
                                if (it.isFocused && listOfIds[5] !in currentFocusedIds) currentFocusedIds.add(
                                    listOfIds[5]
                                )
                                else if (!it.isFocused) currentFocusedIds.remove(listOfIds[5])
                            }
                            .focusable(),
                        colors = ButtonDefaults.buttonColors(
                            containerColor = if (currentFocusedIds.contains(listOfIds[5])) Yellow else Blue
                        ),
                    ) { Text("Row2 Col2") }
                }
            }
        }
//        ModifierExample(
//            title = "On Focus Bounds Changed",
//            body = "Provides a callback of the new layout coordinates after the focused area being changed",
//            codeSnippetImage = null,
//            onShowCodeSnippetClick = onShowCodeSnippetClick
//        ) {
//            var color by remember { mutableStateOf(Black) }
//            var isScaled by remember {
//                mutableStateOf(false)
//            }
//            val focusRequester = remember {
//                FocusRequester()
//            }
//            var boxSize by remember {
//                mutableStateOf(IntSize.Zero)
//            }
//            val coroutineScope = rememberCoroutineScope()
//            Box(
//                Modifier
//                    .fillMaxWidth(if (isScaled) 0.5f else 0.25f)
//                    .aspectRatio(1f)
//                    .align(CenterHorizontally)
//                    .border(2.dp, color)
//                    .focusRequester(focusRequester)
//                    // The onFocusChanged should be added BEFORE the focusTarget that is being observed.
//                    .onFocusChanged { color = if (it.isFocused) Green else Black }
//                    .onFocusedBoundsChanged {
//                        println("%% ${it?.size}")
//                        if (it != null) {
//                            boxSize = it.size
//                        }
//                    }
//                    .onGloballyPositioned {
//                        println("$$ ${it.size}")
//                    }
//                    .focusTarget()
//                    .clickable {
//                        coroutineScope.launch {
//                            focusRequester.requestFocus()
//                            delay(1000)
//                            isScaled = !isScaled
//                        }
//                    }
//            ) {
//                Text(text = "Box Size: $boxSize", Modifier.align(Center))
//            }
//        }
    }
}

@Preview
@Composable
fun FocusModifiersScreenPreview() {
    ComposePlaygroundTheme {
        FocusModifiersScreen {}
    }
}