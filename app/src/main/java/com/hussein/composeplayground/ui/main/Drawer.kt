package com.hussein.composeplayground.ui.main

import androidx.activity.compose.BackHandler
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.automirrored.filled.Article
import androidx.compose.material.icons.filled.Accessibility
import androidx.compose.material.icons.filled.Animation
import androidx.compose.material.icons.filled.Api
import androidx.compose.material.icons.filled.BorderOuter
import androidx.compose.material.icons.filled.Build
import androidx.compose.material.icons.filled.Category
import androidx.compose.material.icons.filled.CenterFocusStrong
import androidx.compose.material.icons.filled.Draw
import androidx.compose.material.icons.filled.FormatAlignCenter
import androidx.compose.material.icons.filled.Home
import androidx.compose.material.icons.filled.KeyboardAlt
import androidx.compose.material.icons.filled.Layers
import androidx.compose.material.icons.filled.Mouse
import androidx.compose.material.icons.filled.Work
import androidx.compose.material3.HorizontalDivider
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.ModalDrawerSheet
import androidx.compose.material3.NavigationDrawerItem
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.derivedStateOf
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.tooling.preview.Preview
import androidx.navigation.NavController
import androidx.navigation.compose.currentBackStackEntryAsState
import androidx.navigation.compose.rememberNavController
import androidx.navigation.toRoute
import com.hussein.composeplayground.ui.theme.Default
import com.hussein.composeplayground.ui.theme.QuarterDefault
import kotlinx.coroutines.launch

@Composable
fun Drawer(
    modifier: Modifier = Modifier,
    navController: NavController,
    onItemClicked: (NavDestination) -> Unit,
    onBackClicked: suspend () -> Unit,
) {
    val coroutineScope = rememberCoroutineScope()
    BackHandler(true) {
        coroutineScope.launch {
            onBackClicked()
        }
    }
    val currentDestination by navController.currentBackStackEntryAsState()
    val currentRoute: NavDestination? by remember {
        derivedStateOf {
            currentDestination?.toRoute()
        }
    }
    ModalDrawerSheet(modifier = modifier) {
        Column(Modifier.verticalScroll(rememberScrollState())) {
            Text(
                text = "Thanks for visiting my portfolio!",
                Modifier.padding(Default),
                style = MaterialTheme.typography.titleMedium,
                color = MaterialTheme.colorScheme.primary
            )
            HorizontalDivider()
            DrawerItem(
                title = "Home",
                icon = Icons.Default.Home,
                navDestination = NavDestination.Home,
                currentDestination = currentRoute,
                onItemClicked = onItemClicked
            )
            DrawerItem(
                "Projects",
                Icons.Default.Build,
                NavDestination.Projects,
                currentRoute,
                onItemClicked
            )
            DrawerItem(
                title = "Work Expertise",
                icon = Icons.Default.Work,
                navDestination = NavDestination.WorkExpertise,
                currentDestination = currentRoute,
                onItemClicked = onItemClicked
            )
            DrawerItem(
                title = "Articles",
                icon = Icons.AutoMirrored.Default.Article,
                navDestination = NavDestination.Articles,
                currentDestination = currentRoute,
                onItemClicked = onItemClicked
            )
            HorizontalDivider()
            Text(
                text = "Use-cases and APIs experimenting",
                Modifier.padding(Default),
                style = MaterialTheme.typography.titleMedium,
                color = MaterialTheme.colorScheme.secondary
            )
            HorizontalDivider()
            DrawerItem(
                title = "Graphic Modifiers",
                icon = Icons.Default.Draw,
                navDestination = NavDestination.GraphicModifiers,
                currentDestination = currentRoute,
                onItemClicked = onItemClicked
            )
            DrawerItem(
                title = "Alignment Modifiers",
                icon = Icons.Default.FormatAlignCenter,
                navDestination = NavDestination.AlignmentModifiers,
                currentDestination = currentRoute,
                onItemClicked = onItemClicked
            )
            DrawerItem(
                title = "Animation Modifiers",
                icon = Icons.Default.Animation,
                navDestination = NavDestination.AnimationModifiers,
                currentDestination = currentRoute,
                onItemClicked = onItemClicked
            )
            DrawerItem(
                title = "Border Modifiers",
                icon = Icons.Default.BorderOuter,
                navDestination = NavDestination.BorderModifiers,
                currentDestination = currentRoute,
                onItemClicked = onItemClicked
            )
            DrawerItem(
                title = "Focus Modifiers",
                icon = Icons.Default.CenterFocusStrong,
                navDestination = NavDestination.FocusModifiers,
                currentDestination = currentRoute,
                onItemClicked = onItemClicked
            )
            DrawerItem(
                title = "Keyboard Modifiers",
                icon = Icons.Default.KeyboardAlt,
                navDestination = NavDestination.KeyboardModifiers,
                currentDestination = currentRoute,
                onItemClicked = onItemClicked
            )
            DrawerItem(
                title = "Layout Modifiers",
                icon = Icons.Default.Layers,
                navDestination = NavDestination.LayoutModifiers,
                currentDestination = currentRoute,
                onItemClicked = onItemClicked
            )
            DrawerItem(
                title = "Pointer Modifiers",
                icon = Icons.Default.Mouse,
                navDestination = NavDestination.PointerModifiers,
                currentDestination = currentRoute,
                onItemClicked = onItemClicked
            )
            DrawerItem(
                title = "Position Modifiers",
                icon = Icons.Default.Api,
                navDestination = NavDestination.PositionModifiers,
                currentDestination = currentRoute,
                onItemClicked = onItemClicked
            )
            DrawerItem(
                title = "Semantics Modifiers",
                icon = Icons.Default.Accessibility,
                navDestination = NavDestination.SemanticsModifiers,
                currentDestination = currentRoute,
                onItemClicked = onItemClicked
            )
            DrawerItem(
                title = "Other Modifiers",
                icon = Icons.Default.Category,
                navDestination = NavDestination.OtherModifiers,
                currentDestination = currentRoute,
                onItemClicked = onItemClicked
            )
        }
    }
}

@Preview
@Composable
fun DrawerPreview() {
    Drawer(navController = rememberNavController(), onItemClicked = {}) {

    }
}

@Composable
private fun DrawerItem(
    title: String,
    icon: ImageVector,
    navDestination: NavDestination,
    currentDestination: NavDestination?,
    onItemClicked: (NavDestination) -> Unit,
) {
    NavigationDrawerItem(
        label = {
            Row(verticalAlignment = Alignment.CenterVertically) {
                Icon(imageVector = icon, contentDescription = "Home Icon")
                Spacer(modifier = Modifier.width(QuarterDefault))
                Text(text = title)
            }
        },
        selected = currentDestination == navDestination,
        onClick = { onItemClicked(navDestination) })
}

