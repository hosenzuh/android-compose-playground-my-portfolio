package com.hussein.composeplayground.ui.workExperities

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.IntrinsicSize
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material3.Divider
import androidx.compose.material3.ElevatedButton
import androidx.compose.material3.ElevatedCard
import androidx.compose.material3.HorizontalDivider
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Snackbar
import androidx.compose.material3.SnackbarHost
import androidx.compose.material3.SnackbarHostState
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import coil.compose.AsyncImage
import com.hussein.composeplayground.domain.base.State
import com.hussein.composeplayground.model.WorkExpertise
import com.hussein.composeplayground.ui.base.components.LoadingText
import com.hussein.composeplayground.ui.theme.Default
import com.hussein.composeplayground.ui.theme.HalfDefault
import com.hussein.composeplayground.ui.theme.QuarterDefault
import com.hussein.composeplayground.util.collectWithLifeCycle
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow


@Composable
fun WorkExperiencesScreen(
    modifier: Modifier = Modifier,
    workExpertisesState: State<List<WorkExpertise>>,
    messages: Flow<String>,
    onRetryClicked: () -> Unit
) {
    val snackbarState = remember {
        SnackbarHostState()
    }


    messages.collectWithLifeCycle {
        snackbarState.showSnackbar(it)
    }

    Box(Modifier.fillMaxSize()) {
        if(workExpertisesState is State.Success.Data) {
            workExpertisesState.data
        }
        when (workExpertisesState) {
            is State.Loading.Generic -> {
                LoadingText(Modifier.align(Alignment.Center))
            }

            is State.Success.Data -> {
                LazyColumn(
                    modifier.fillMaxSize(),
                    verticalArrangement = Arrangement.spacedBy(HalfDefault)
                ) {
                    items(items = workExpertisesState.data, key = { it.id }) {
                        WorkExpertiseItem(expertise = it)
                    }
                }
            }

            is State.Failure -> {

                ElevatedButton(
                    onClick = onRetryClicked,
                    modifier = Modifier.align(Alignment.Center)
                ) {
                    Text(text = "Retry")
                }

                SnackbarHost(
                    snackbarState, modifier =
                    Modifier
                        .padding(Default)
                        .align(Alignment.BottomCenter)
                ) {
                    Snackbar(it)
                }
            }

            else -> {}
        }
    }
}


@Preview
@Composable
private fun WorkExperiencesPreview() {
    WorkExperiencesScreen(
        workExpertisesState = State.initial(),
        messages = flow { }
    ) {}
}


@Composable
private fun WorkExpertiseItem(modifier: Modifier = Modifier, expertise: WorkExpertise) {
    ElevatedCard {
        Column(
            modifier
                .fillMaxWidth()
                .padding(HalfDefault)
        ) {
            Row(
                verticalAlignment = Alignment.CenterVertically,
                modifier = Modifier.height(IntrinsicSize.Min)
            ) {
                Box(
                    modifier = Modifier
                        .padding(HalfDefault)
                        .clip(CircleShape)
                        .wrapContentHeight()
                        .aspectRatio(1f)
                        .background(MaterialTheme.colorScheme.primary)

                )
                if (expertise.companyImage != null) {
                    AsyncImage(
                        modifier = Modifier
                            .padding(end = QuarterDefault)
                            .size(20.dp),
                        model = expertise.companyImage,
                        contentDescription = "Image for company: ${expertise.companyName}"
                    )
                }
                Text(
                    text = expertise.companyName, style = MaterialTheme.typography.titleLarge,
                    color = MaterialTheme.colorScheme.primary
                )
            }
            Row(Modifier.height(IntrinsicSize.Min)) {
                HorizontalDivider(
                    Modifier
                        .padding(
                            start = 14.dp,
                            end = 14.dp,
                            top = 6.dp
                        )
                        .fillMaxHeight()
                        .width(1.dp),
                    color = MaterialTheme.colorScheme.tertiary
                )
                Column {
                    Spacer(modifier = Modifier.height(QuarterDefault))
                    Text(
                        text = "${expertise.startDate} - ${expertise.endDate ?: "Present"}",
                        style = MaterialTheme.typography.labelSmall,
                        color = MaterialTheme.colorScheme.tertiary,
                    )
                    Spacer(modifier = Modifier.height(Default))
                    Text(
                        text = expertise.description,
                        style = MaterialTheme.typography.bodyMedium
                    )
                }
            }
            Spacer(modifier = Modifier.height(Default))
        }
    }
}

@Preview
@Composable
private fun WorkExpertiseItemPreview() {
    WorkExpertiseItem(
        expertise = WorkExpertise(
            id = 7241,
            companyName = "Isabelle Gomez",
            companyImage = null,
            startDate = "ceteros",
            endDate = null,
            description = "magna",
            link = null,
            projects = listOf()
        )
    )
}