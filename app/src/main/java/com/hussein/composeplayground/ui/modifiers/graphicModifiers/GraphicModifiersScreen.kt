package com.hussein.composeplayground.ui.modifiers.graphicModifiers

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.ui.Modifier
import com.hussein.composeplayground.ui.theme.Default
import com.hussein.composeplayground.ui.theme.LightAndNightPreview

@androidx.compose.runtime.Composable
fun GraphicModifiersScreen(modifier: Modifier = Modifier, onShowCodeSnippetClick: (Int) -> Unit) {
    val scrollState = rememberScrollState()

    Column(modifier.verticalScroll(scrollState)) {
        DrawingModifiers(onShowCodeSnippetClick)
        Spacer(modifier = Modifier.height(Default))
        GraphicModifiers(onShowCodeSnippetClick)
    }
}

@LightAndNightPreview
@androidx.compose.runtime.Composable
private fun GraphicModifiersScreenPreview() {
    GraphicModifiersScreen() {}
}