package com.hussein.composeplayground.ui.base

import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow

abstract class StatefulViewModel<T>(initialValue: T) : BaseViewModel() {

    protected val _state = MutableStateFlow<T>(initialValue)
    val state = _state.asStateFlow()
}