package com.hussein.composeplayground.ui.articles


import androidx.lifecycle.viewModelScope
import com.hussein.composeplayground.domain.base.State
import com.hussein.composeplayground.domain.usecases.articles.GetLinkedInPostsUseCase
import com.hussein.composeplayground.domain.usecases.articles.GetMediumArticlesUseCase
import com.hussein.composeplayground.model.LinkedInPost
import com.hussein.composeplayground.model.MediumArticle
import com.hussein.composeplayground.ui.base.StatefulViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.emptyFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ArticlesViewModel @Inject constructor(
    private val getMediumArticlesUseCase: GetMediumArticlesUseCase,
    private val getLinkedInPostsUseCase: GetLinkedInPostsUseCase,
) : StatefulViewModel<ArticlesViewModel.ArticlesScreenState>(
    ArticlesScreenState(
        MediumArticlesState.Initial,
        LinkedInPostsState.Initial
    )
) {

    init {
        getArticles(*ArticlesTypes.entries.toTypedArray())
    }

    fun getArticles(vararg typesToBeUpdated: ArticlesTypes) {
        viewModelScope.launch {
            if (ArticlesTypes.MEDIUM in typesToBeUpdated) {
                getMediumArticlesUseCase().collect { mediumArticleState ->
                    _state.update {
                        it.copy(
                            mediumArticlesState = when (mediumArticleState) {
                                is State.Initial -> MediumArticlesState.Initial
                                is State.Loading -> MediumArticlesState.Loading
                                is State.Success.Data<List<MediumArticle>> -> {
                                    MediumArticlesState.Loaded(mediumArticleState.data)
                                }

                                is State.Failure -> {
                                    MediumArticlesState.Failure(mediumArticleState.message)
                                }

                                else -> MediumArticlesState.Initial
                            },
                        )
                    }
                }
            }

            if (ArticlesTypes.LINKED_IN_POSTS in typesToBeUpdated) {
                getLinkedInPostsUseCase().collect { linkedInPostsState ->
                    _state.update {
                        it.copy(
                            linkedInPostsState =
                            when (linkedInPostsState) {
                                is State.Initial -> LinkedInPostsState.Initial
                                is State.Loading -> LinkedInPostsState.Loading
                                is State.Success.Data<List<LinkedInPost>> -> {
                                    LinkedInPostsState.Loaded(linkedInPostsState.data)
                                }

                                is State.Failure -> {
                                    LinkedInPostsState.Failure(linkedInPostsState.message)
                                }

                                else -> LinkedInPostsState.Initial
                            }
                        )
                    }
                }
            }
        }
    }

    sealed interface MediumArticlesState {
        data object Initial : MediumArticlesState
        data object Loading : MediumArticlesState
        data class Loaded(val mediumArticles: List<MediumArticle>) : MediumArticlesState
        data class Failure(val message: String?) : MediumArticlesState
    }

    sealed interface LinkedInPostsState {
        data object Initial : LinkedInPostsState
        data object Loading : LinkedInPostsState
        data class Loaded(val posts: List<LinkedInPost>) : LinkedInPostsState
        data class Failure(val message: String?) : LinkedInPostsState
    }

    data class ArticlesScreenState(
        val mediumArticlesState: MediumArticlesState,
        val linkedInPostsState: LinkedInPostsState
    )
}