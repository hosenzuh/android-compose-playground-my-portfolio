package com.hussein.composeplayground.ui.modifiers.paddingModifiers

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.absolutePadding
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.paddingFrom
import androidx.compose.foundation.layout.paddingFromBaseline
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.SpaceBar
import androidx.compose.material3.Button
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.FirstBaseline
import androidx.compose.ui.platform.LocalLayoutDirection
import androidx.compose.ui.text.style.TextDecoration
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.LayoutDirection
import androidx.compose.ui.unit.dp
import com.hussein.composeplayground.ui.base.components.ModifierExample
import com.hussein.composeplayground.ui.base.components.NoteCard
import com.hussein.composeplayground.ui.theme.ComposePlaygroundTheme
import com.hussein.composeplayground.ui.theme.HalfDefault

@Composable
fun PaddingModifiersScreen(
    modifier: Modifier = Modifier,
    onShowCodeSnippetClick: (imageResId: Int) -> Unit
) {
    Column(modifier.verticalScroll(rememberScrollState())) {
        ModifierExample(
            title = "Padding From alignmentLine",
            body = "This modifier adds padding from the content bounds to the alignment line that we pass",
            codeSnippetImage = null,
            onShowCodeSnippetClick = onShowCodeSnippetClick
        ) {

            Text(
                text = "For example we used the FirstBaseLine here, Because the FirstBaseLine is the line that under the letters of the first line in a Text, " +
                        "it will pad from top less than bottom"
            )

            Text(
                text = "This text padded by First base line",
                Modifier
                    .background(MaterialTheme.colorScheme.surface)
                    .paddingFrom(
                        FirstBaseline, before = 30.dp, after = 30.dp
                    ),
                textDecoration = TextDecoration.Underline
            )

        }

        ModifierExample(
            title = "Padding From Baseline",
            body = "This modifier takes two parameters:\n  top: the padding between the top of the layout and the baseline " +
                    "of the first line of text in the content (FirstBaseLine)\n" +
                    "  bottom: the padding00 between the bottom of the layout and the baseline of the last text in the content",
            codeSnippetImage = null,
            onShowCodeSnippetClick = onShowCodeSnippetClick
        ) {

            Column(
                Modifier
                    .background(MaterialTheme.colorScheme.surface)
                    .paddingFromBaseline(
                        top = 30.dp, bottom = 30.dp
                    )
            ) {
                Text(
                    text = "The first line passes this text from under it padded from the top by 30.dp",
                    textDecoration = TextDecoration.Underline
                )
                Icon(imageVector = Icons.Default.SpaceBar, contentDescription = "")
                Text(
                    text = "The last line passes this text from under it padded from the bottom by 30.dp",
                    textDecoration = TextDecoration.Underline
                )
            }

        }

        ModifierExample(
            title = "Absolute padding",
            body = "This padding doesn't take RTL into account, it contains left, right, top and bottom paddings, " +
                    "This it useful when you want the padding to be always from specific direction instead of the start and end of the current screen direction",
            codeSnippetImage = null,
            onShowCodeSnippetClick = onShowCodeSnippetClick
        ) {

            val defaultDirection = LocalLayoutDirection.current
            var layoutDirection by remember {
                mutableStateOf(defaultDirection)
            }
            Button(onClick = {
                layoutDirection = if (layoutDirection == LayoutDirection.Ltr) {
                    LayoutDirection.Rtl
                } else {
                    LayoutDirection.Ltr
                }
            }) {
                Text(
                    text = "Current layout Direction: ${layoutDirection.name}, Reverse layout direction"
                )
            }
            CompositionLocalProvider(value = LocalLayoutDirection provides layoutDirection) {
                Box(
                    modifier = Modifier
                        .fillMaxWidth()
                        .absolutePadding(left = HalfDefault)
                        .background(MaterialTheme.colorScheme.primary)
                ) {
                    Text(text = "Box with absolute padding")
                }
                Spacer(modifier = Modifier.height(HalfDefault))
                Box(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(start = HalfDefault)
                        .background(MaterialTheme.colorScheme.primary)
                ) {
                    Text(text = "Box with normal (relative) padding")
                }
            }
        }

        NoteCard {
            Text(text = "There are many other padding modifiers, which add padding to accommodate system UI paddings.\n" +
                    "E.x: statusBarsPadding(), navigationBarsPadding(), imePadding() etc..")
        }
    }
}

@Preview
@Composable
fun PaddingModifiersScreenPreview() {
    ComposePlaygroundTheme {
        PaddingModifiersScreen {}
    }
}