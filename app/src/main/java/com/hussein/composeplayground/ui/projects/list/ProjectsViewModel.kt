package com.hussein.composeplayground.ui.projects.list


import androidx.lifecycle.viewModelScope
import com.hussein.composeplayground.domain.base.State
import com.hussein.composeplayground.domain.usecases.projects.GetProjectsUseCase
import com.hussein.composeplayground.model.Project
import com.hussein.composeplayground.ui.base.BaseViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ProjectsViewModel @Inject constructor(
    private val getProjectsUseCase: GetProjectsUseCase,
) : BaseViewModel() {

    lateinit var screenState: StateFlow<State<List<Project>>>

    init {
        getProjects()
    }

    fun getProjects() {
        viewModelScope.launch {
            screenState =
                getProjectsUseCase().onEach {
                    if (it is State.Failure) {
                        it.message?.let { message -> _messages.send(message) }
                    }
                }.stateIn(viewModelScope, SharingStarted.Lazily, State.initial())
        }
    }

}