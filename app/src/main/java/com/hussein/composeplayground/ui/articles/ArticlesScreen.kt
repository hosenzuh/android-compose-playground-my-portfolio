package com.hussein.composeplayground.ui.articles

import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.pager.HorizontalPager
import androidx.compose.foundation.pager.rememberPagerState
import androidx.compose.material3.Tab
import androidx.compose.material3.TabRow
import androidx.compose.material3.TabRowDefaults
import androidx.compose.material3.TabRowDefaults.tabIndicatorOffset
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import com.hussein.composeplayground.model.LinkedInPost
import com.hussein.composeplayground.model.MediumArticle
import com.hussein.composeplayground.ui.articles.ArticlesTypes.*
import com.hussein.composeplayground.ui.theme.HalfDefault

enum class ArticlesTypes(val index: Int, val title: String) {
    MEDIUM(0, "Medium"),
    LINKED_IN_POSTS(1, "Linkedin Posts")
}

@OptIn(ExperimentalFoundationApi::class)
@Composable
fun ArticlesScreen(
    modifier: Modifier = Modifier,
    articlesScreenState: ArticlesViewModel.ArticlesScreenState,
    onRetryClicked: (typesToBeRetried: ArticlesTypes) -> Unit,
) {
    var selectedTab by remember {
        mutableStateOf(MEDIUM)
    }
    val pagerState = rememberPagerState { 2 }
    val mediumArticlesState = articlesScreenState.mediumArticlesState
    val linkedinPostState = articlesScreenState.linkedInPostsState
    Column(
        modifier
            .fillMaxSize(),
    ) {

        LaunchedEffect(key1 = selectedTab, block = {
            pagerState.animateScrollToPage(selectedTab.index)
        })

        TabRow(selectedTabIndex = selectedTab.index,
            indicator = {
                TabRowDefaults.Indicator(
                    Modifier.tabIndicatorOffset(it[pagerState.currentPage])
                )
            }
        ) {
            ArticlesTypes.entries.forEach {
                Tab(
                    selected = selectedTab == it,
                    onClick = {
                        selectedTab = it
                    }
                ) {
                    Text(text = it.title, Modifier.padding(HalfDefault))
                }
            }
        }
        HorizontalPager(
            state = pagerState,
            modifier = Modifier
                .fillMaxSize(),
        ) {
            when (it) {
                MEDIUM.index -> MediumArticles(mediumArticlesState, onRetryClicked)
                LINKED_IN_POSTS.index -> LinkedInPosts(
                    linkedInPostsState = linkedinPostState,
                    onRetryClicked
                )
            }
        }
    }
}


@Preview
@Composable
fun ArticlesPreview() {
    ArticlesScreen(
        Modifier,
        ArticlesViewModel.ArticlesScreenState(
            ArticlesViewModel.MediumArticlesState.Loaded(
                listOf(
                    MediumArticle(
                        url = "https://search.yahoo.com/search?p=venenatis",
                        title = "dictas",
                        contentHtml = "dico",
                        publishingDate = "20/10/2020"
                    ),
                    MediumArticle(
                        url = "http://www.bing.com/search?q=mentitum",
                        title = "discere",
                        contentHtml = "tempus",
                        publishingDate = "20/11/2020"
                    )
                )
            ),
            ArticlesViewModel.LinkedInPostsState.Loaded(
                listOf(
                    LinkedInPost(
                        title = "Title 1",
                        content = "tacimates tacimates tacimates tacimates tacimates tacimates tacimates ",
                        images = listOf(),
                        url = "https://search.yahoo.com/search?p=expetendis"
                    ),
                    LinkedInPost(
                        title = "Title 2",
                        content = "tacimates tacimates tacimates tacimates tacimates tacimates tacimates ",
                        images = listOf(),
                        url = "https://search.yahoo.com/search?p=expetendis"
                    ),
                )
            )
        ),
        onRetryClicked = {})
}

