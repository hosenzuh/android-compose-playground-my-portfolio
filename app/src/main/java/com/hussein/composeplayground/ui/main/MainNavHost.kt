package com.hussein.composeplayground.ui.main

import androidx.activity.compose.BackHandler
import androidx.compose.animation.core.tween
import androidx.compose.animation.fadeIn
import androidx.compose.animation.fadeOut
import androidx.compose.animation.scaleIn
import androidx.compose.animation.scaleOut
import androidx.compose.animation.slideInHorizontally
import androidx.compose.animation.slideOutHorizontally
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Modifier
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.toRoute
import com.hussein.composeplayground.domain.base.State
import com.hussein.composeplayground.model.Project
import com.hussein.composeplayground.ui.articles.ArticlesScreen
import com.hussein.composeplayground.ui.articles.ArticlesViewModel
import com.hussein.composeplayground.ui.home.HomeScreen
import com.hussein.composeplayground.ui.main.NavDestination.*
import com.hussein.composeplayground.ui.modifiers.actionModifiers.ActionModifiersScreen
import com.hussein.composeplayground.ui.modifiers.alignmentModifiers.AlignmentModifiersScreen
import com.hussein.composeplayground.ui.modifiers.animationModifiers.AnimationModifiersScreen
import com.hussein.composeplayground.ui.modifiers.borderModifiers.BorderModifiersScree
import com.hussein.composeplayground.ui.modifiers.focusModifiers.FocusModifiersScreen
import com.hussein.composeplayground.ui.modifiers.graphicModifiers.GraphicModifiersScreen
import com.hussein.composeplayground.ui.modifiers.keyboardModifiers.KeyboardModifiersScreen
import com.hussein.composeplayground.ui.modifiers.layoutModifiers.LayoutModifiersScreen
import com.hussein.composeplayground.ui.modifiers.otherModifiers.OtherModifiersScreen
import com.hussein.composeplayground.ui.modifiers.pointerModifiers.PointerModifiersScreen
import com.hussein.composeplayground.ui.modifiers.positionModifiers.PositionModifiersScreen
import com.hussein.composeplayground.ui.modifiers.semanticsModifiers.SemanticsModifierScreen
import com.hussein.composeplayground.ui.projects.details.ProjectDetailsScreen
import com.hussein.composeplayground.ui.projects.list.ProjectsScreen
import com.hussein.composeplayground.ui.projects.list.ProjectsViewModel
import com.hussein.composeplayground.ui.theme.HalfDefault
import com.hussein.composeplayground.ui.workExperities.WorkExperiencesScreen
import com.hussein.composeplayground.ui.workExperities.WorkExperiencesViewModel
import com.hussein.composeplayground.util.parcelableType
import kotlin.reflect.typeOf

@Composable
fun MainNavHost(
    modifier: Modifier = Modifier,
    contentPadding: PaddingValues,
    navController: NavHostController,
    onShowCodeSnippetClick: (imageResId: Int) -> Unit,
) {
    NavHost(
        modifier = modifier
            .fillMaxSize()
            .padding(horizontal = HalfDefault)
            .padding(contentPadding),
        navController = navController,
        startDestination = Home,
        enterTransition = { scaleIn(tween(1000)) + fadeIn(tween(700)) },
        exitTransition = {
            slideOutHorizontally(tween(500)) { -it } + fadeOut(tween(500))
        },
        popEnterTransition = {
            slideInHorizontally(tween(1000)) { -it } + fadeIn(
                tween(
                    700
                )
            )
        },
        popExitTransition = { scaleOut(tween(500)) + fadeOut(tween(500)) },
    ) {
        composable<Home> {
            HomeScreen()
        }

        composable<Projects> {
            val viewModel = hiltViewModel<ProjectsViewModel>()
            ProjectsScreen(
                projectsState = viewModel.screenState.collectAsState().value,
                onProjectClick = {
                    navController.navigate(ProjectDetails(project = it))
                },
                messages = viewModel.messages,
                onRetryClicked = viewModel::getProjects,
            )
        }

        composable<ProjectDetails>(
            typeMap = mapOf(typeOf<Project>() to parcelableType<Project>(isNullableAllowed = false))
        ) {
            val project = it.toRoute<ProjectDetails>().project

            ProjectDetailsScreen(
                projectState = State.success(project),
            )

        }


        composable<WorkExpertise> {
            val viewModel: WorkExperiencesViewModel = hiltViewModel()
            WorkExperiencesScreen(
                workExpertisesState = viewModel.screenState.collectAsState().value,
                onRetryClicked = viewModel::fetchWorkExpertises,
                messages = viewModel.messages
            )
        }
        composable<Articles> {
            val articlesViewModel = hiltViewModel<ArticlesViewModel>()
            ArticlesScreen(
                articlesScreenState = articlesViewModel.state.collectAsState().value,
                onRetryClicked = {
                    articlesViewModel.getArticles(it)
                },
            )
        }
        composable<GraphicModifiers> {
            GraphicModifiersScreen(onShowCodeSnippetClick = onShowCodeSnippetClick)
        }
        composable<ActionModifiers> {
            ActionModifiersScreen(onShowCodeSnippetClick = onShowCodeSnippetClick)
        }
        composable<AlignmentModifiers> {
            AlignmentModifiersScreen(onShowCodeSnippetClick = onShowCodeSnippetClick)
        }
        composable<AnimationModifiers> {
            AnimationModifiersScreen(onShowCodeSnippetClick = onShowCodeSnippetClick)
        }
        composable<BorderModifiers> {
            BorderModifiersScree(onShowCodeSnippetClick = onShowCodeSnippetClick)
        }
        composable<FocusModifiers> {
            FocusModifiersScreen(onShowCodeSnippetClick = onShowCodeSnippetClick)
        }
        composable<KeyboardModifiers> {
            KeyboardModifiersScreen(onShowCodeSnippetClick = onShowCodeSnippetClick)
        }
        composable<LayoutModifiers> {
            LayoutModifiersScreen(onShowCodeSnippetClick = onShowCodeSnippetClick)
        }
        composable<PointerModifiers> {
            PointerModifiersScreen(onShowCodeSnippetClick = onShowCodeSnippetClick)
        }
        composable<PositionModifiers> {
            PositionModifiersScreen(onShowCodeSnippetClick = onShowCodeSnippetClick)
        }
        composable<SemanticsModifiers> {
            SemanticsModifierScreen(onShowCodeSnippetClick = onShowCodeSnippetClick)
        }
        composable<OtherModifiers> {
            OtherModifiersScreen(onShowCodeSnippetClick = onShowCodeSnippetClick)
        }
    }
}