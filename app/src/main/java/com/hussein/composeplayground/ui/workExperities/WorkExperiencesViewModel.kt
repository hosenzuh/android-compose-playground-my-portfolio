package com.hussein.composeplayground.ui.workExperities


import androidx.lifecycle.viewModelScope
import com.hussein.composeplayground.domain.base.State
import com.hussein.composeplayground.domain.usecases.workExpertises.GetWorkExpertisesUseCase
import com.hussein.composeplayground.model.WorkExpertise
import com.hussein.composeplayground.ui.base.BaseViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class WorkExperiencesViewModel @Inject constructor(
    private val getWorkExpertisesUseCase: GetWorkExpertisesUseCase
) : BaseViewModel() {

    private val _screenState: MutableStateFlow<State<List<WorkExpertise>>> =
        MutableStateFlow<State<List<WorkExpertise>>>(State.initial()).apply {
            onEach {
                if (it is State.Failure) {
                    it.message?.let { message ->
                        _messages.send(message)
                    }
                }
            }.launchIn(viewModelScope)
        }
    val screenState: StateFlow<State<List<WorkExpertise>>> =
        _screenState.asStateFlow()

    init {
        fetchWorkExpertises()
    }

    fun fetchWorkExpertises() {
        viewModelScope.launch {
            getWorkExpertisesUseCase().collect {
                _screenState.value = it
            }
        }
    }
}