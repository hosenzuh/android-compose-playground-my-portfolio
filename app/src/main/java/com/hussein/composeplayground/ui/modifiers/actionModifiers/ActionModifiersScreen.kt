package com.hussein.composeplayground.ui.modifiers.actionModifiers

import androidx.compose.animation.core.Spring
import androidx.compose.animation.core.exponentialDecay
import androidx.compose.animation.core.spring
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.combinedClickable
import androidx.compose.foundation.gestures.AnchoredDraggableState
import androidx.compose.foundation.gestures.DraggableAnchors
import androidx.compose.foundation.gestures.Orientation
import androidx.compose.foundation.gestures.anchoredDraggable
import androidx.compose.foundation.gestures.draggable
import androidx.compose.foundation.gestures.draggable2D
import androidx.compose.foundation.gestures.rememberDraggable2DState
import androidx.compose.foundation.gestures.rememberDraggableState
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.selection.selectable
import androidx.compose.foundation.selection.selectableGroup
import androidx.compose.foundation.selection.toggleable
import androidx.compose.foundation.selection.triStateToggleable
import androidx.compose.foundation.text.ClickableText
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Brightness6
import androidx.compose.material.icons.filled.BrightnessAuto
import androidx.compose.material.icons.filled.House
import androidx.compose.material.icons.filled.LightMode
import androidx.compose.material.icons.filled.ModeNight
import androidx.compose.material.icons.outlined.House
import androidx.compose.material.ripple.rememberRipple
import androidx.compose.material3.Divider
import androidx.compose.material3.HorizontalDivider
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Snackbar
import androidx.compose.material3.SnackbarHost
import androidx.compose.material3.SnackbarHostState
import androidx.compose.material3.Switch
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableIntStateOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.onGloballyPositioned
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.state.ToggleableState.*
import androidx.compose.ui.text.AnnotatedString
import androidx.compose.ui.text.ExperimentalTextApi
import androidx.compose.ui.text.UrlAnnotation
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextDecoration
import androidx.compose.ui.text.withStyle
import androidx.compose.ui.unit.IntOffset
import androidx.compose.ui.unit.dp
import com.hussein.composeplayground.R
import com.hussein.composeplayground.ui.modifiers.actionModifiers.DragAnchors.*
import com.hussein.composeplayground.ui.base.components.ModifierExample
import com.hussein.composeplayground.ui.base.components.NoteCard
import com.hussein.composeplayground.ui.theme.ComposePlaygroundTheme
import com.hussein.composeplayground.ui.theme.Default
import com.hussein.composeplayground.ui.theme.LightAndNightPreview
import com.hussein.composeplayground.util.IntentManager.viewUrl
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import kotlin.math.roundToInt


@OptIn(ExperimentalTextApi::class)
@Composable
fun ActionModifiersScreen(modifier: Modifier = Modifier, onShowCodeSnippetClick: (Int) -> Unit) {

    val coroutineScope = rememberCoroutineScope()
    val snackbarHostState = remember {
        SnackbarHostState()
    }
    Box(modifier.fillMaxHeight()) {
        Column(Modifier.verticalScroll(rememberScrollState())) {
            Clickable(onShowCodeSnippetClick, coroutineScope, snackbarHostState)
            CombinedClickable(onShowCodeSnippetClick, coroutineScope, snackbarHostState)
            Selectable(onShowCodeSnippetClick)
            Toggleable(onShowCodeSnippetClick)
            TriStateToggleable(onShowCodeSnippetClick)
            NoteCard(Modifier.fillMaxWidth()) {
                val semanticTreeDocumentationUrl =
                    "https://developer.android.com/jetpack/compose/semantics"
                val annotatedString = buildAnnotatedString {
                    append(
                        "The main difference between using selectable with selectable group, toggleable or handling the selecting manually" +
                                " is the "
                    )
                    pushUrlAnnotation(UrlAnnotation(semanticTreeDocumentationUrl))
                    withStyle(
                        MaterialTheme.typography.bodyMedium.toSpanStyle()
                            .copy(Color.Blue, textDecoration = TextDecoration.Underline)
                    ) {
                        append("Semantic Tree")
                    }
                    pop()
                    append(" This helps the accessibility and testing purposes to be very useful")
                }
                val context = LocalContext.current

                ClickableText(text = annotatedString, onClick = {
                    annotatedString.getUrlAnnotations(it, it)
                        .find { it.item.url == semanticTreeDocumentationUrl }?.let {
                            context.viewUrl(semanticTreeDocumentationUrl)
                        }
                })
            }
            HorizontalDivider()
            Draggable(onShowCodeSnippetClick)
            AnchoredDraggable(onShowCodeSnippetClick)
            Draggable2D(onShowCodeSnippetClick)
        }
        SnackbarHost(snackbarHostState, modifier = Modifier.align(Alignment.BottomCenter)) {
            Snackbar(snackbarData = it)
        }
    }
}

@OptIn(ExperimentalFoundationApi::class)
@Composable
fun Draggable2D(onShowCodeSnippetClick: (Int) -> Unit) {
    ModifierExample(
        title = "Draggable 2D",
        body = "This Modifier allows you to apply dragging action on the composable in 2D space",
        codeSnippetImage = null,
        onShowCodeSnippetClick = onShowCodeSnippetClick
    ) {
        var draggingOffset by remember {
            mutableStateOf(Offset.Zero)
        }
        val draggable2DState = rememberDraggable2DState {
            draggingOffset += it
        }
        println("!!!!! $draggingOffset")
        Box(
            modifier = Modifier
                .fillMaxWidth()
                .aspectRatio(1.5f)
        ) {
            Box(
                modifier = Modifier
                    .size(100.dp)
                    .align(Alignment.Center)
                    .draggable2D(draggable2DState)
                    .offset {
                        IntOffset(
                            x = draggingOffset.x.roundToInt(),
                            y = draggingOffset.y.roundToInt()
                        )
                    }
                    .background(Color.Red)
            )
        }
    }
}

enum class DragAnchors(val fraction: Float) { START(0f), CENTER(.5f), END(1f) }

@OptIn(ExperimentalFoundationApi::class)
@Composable
private fun AnchoredDraggable(onShowCodeSnippetClick: (Int) -> Unit) {

    ModifierExample(
        title = "Anchored Draggable",
        body = "Enable dragging between a set of predefined positions and states.",
        codeSnippetImage = R.drawable.modifier_anchored_draggable,
        onShowCodeSnippetClick = onShowCodeSnippetClick,
        documentationLink = "https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=&cad=rja&uact=8&ved=2ahUKEwidxKHstpaBAxV7k4kEHQixABgQFnoECA8QAQ&url=https%3A%2F%2Ffvilarino.medium.com%2Fexploring-jetpack-compose-anchored-draggable-modifier-5fdb21a0c64c&usg=AOvVaw2Q2GiO5XU0NwN0GWCM8-eN&opi=89978449"
    ) {

        val density = LocalDensity.current

        val anchoredDraggableState = remember {
            AnchoredDraggableState<DragAnchors>(
                initialValue = START,
                positionalThreshold = { totalDistance: Float ->
                    totalDistance * 0.5f
                },
                velocityThreshold = {
                    with(density) { 100.dp.toPx() }
                },
                snapAnimationSpec = spring(dampingRatio = Spring.DampingRatioLowBouncy),
                decayAnimationSpec = exponentialDecay(),
                confirmValueChange = { true }
            )
        }

        var textWidthInPx by remember { mutableIntStateOf(0) }
        val screenWidthInPx =
            with(density) { LocalConfiguration.current.screenWidthDp.dp.toPx() }
        val dragEndPoint = screenWidthInPx - textWidthInPx
        LaunchedEffect(key1 = textWidthInPx) {
            anchoredDraggableState.updateAnchors(DraggableAnchors {
                DragAnchors.entries
                    .forEach { anchor ->
                        anchor at (dragEndPoint * anchor.fraction)
                    }
            })
        }

        Text(
            text = "This text is draggable\n with anchors",
            Modifier
                .onGloballyPositioned {
                    textWidthInPx = it.size.width
                    anchoredDraggableState.updateAnchors(DraggableAnchors {
                        DragAnchors.entries
                            .forEach { anchor ->
                                anchor at (dragEndPoint * anchor.fraction)
                            }
                    })
                }
                .offset {
                    IntOffset(
                        x = anchoredDraggableState.offset
                            .takeUnless { it.isNaN() }
                            ?.roundToInt() ?: 0, y = 0)
                }
                .anchoredDraggable(
                    anchoredDraggableState,
                    Orientation.Horizontal,
                )
        )
    }
}

@Composable
private fun Draggable(onShowCodeSnippetClick: (Int) -> Unit) {
    ModifierExample(
        title = "Draggable",
        body = "This Modifier allows you to apply dragging action on the composable",
        codeSnippetImage = R.drawable.modifier_draggable,
        onShowCodeSnippetClick = onShowCodeSnippetClick,
        documentationLink = "https://developer.android.com/reference/kotlin/androidx/compose/ui/Modifier#(androidx.compose.ui.Modifier).draggable(androidx.compose.foundation.gestures.DraggableState,androidx.compose.foundation.gestures.Orientation,kotlin.Boolean,androidx.compose.foundation.interaction.MutableInteractionSource,kotlin.Boolean,kotlin.coroutines.SuspendFunction2,kotlin.coroutines.SuspendFunction2,kotlin.Boolean)"
    ) {
        var offset by remember {
            mutableStateOf(Offset(0f, 0f))
        }
        var textWidthInPx by remember {
            mutableIntStateOf(0)
        }
        val screenWidthInPx =
            with(LocalDensity.current) { LocalConfiguration.current.screenWidthDp.dp.toPx() }
        val horizontalDraggableState = rememberDraggableState(onDelta = {
            offset =
                offset.copy(x = (offset.x + it).coerceIn(0f..(screenWidthInPx - textWidthInPx)))
        })
        Text(
            text = "This text is draggable horizontally",
            modifier = Modifier
                .onGloballyPositioned {
                    textWidthInPx = it.size.width
                }
                .offset {
                    IntOffset(x = offset.x.roundToInt(), y = 0)
                }
                .draggable(
                    horizontalDraggableState,
                    orientation = Orientation.Horizontal,
                    onDragStopped = {
                        // you can add this code to make it back to the initial stat after finish the dragging
                        /*
                                horizontalDraggableState.drag {
                                    animate(it, 0f) { value, _ ->
                                        offset = offset.copy(x = value)
                                    }
                                }
                                */
                    }
                )
        )
    }
}

@Composable
private fun TriStateToggleable(onShowCodeSnippetClick: (Int) -> Unit) {
    ModifierExample(
        title = AnnotatedString("Tri State Toggleable"),
        body = buildAnnotatedString {
            append("Same as Toggleable but with ")
            withStyle(
                MaterialTheme.typography.bodyMedium.toSpanStyle()
                    .copy(fontStyle = FontStyle.Italic)
            ) {
                append("indeterminate ")
            }
            appendLine("state between on/off")

            append("There is ")
            withStyle(
                MaterialTheme.typography.bodyMedium.toSpanStyle()
                    .copy(fontWeight = FontWeight.Bold)
            ) {
                append("TriStateCheckbox ")
            }
            append("component for this use case, when its nested states not fully On and not fully Off")
        },
        codeSnippetImage = R.drawable.modifier_tri_state_toggleable,
        onShowCodeSnippetClick = onShowCodeSnippetClick,
        documentationLink = "https://developer.android.com/reference/kotlin/androidx/compose/ui/Modifier#(androidx.compose.ui.Modifier).triStateToggleable(androidx.compose.ui.state.ToggleableState,kotlin.Boolean,androidx.compose.ui.semantics.Role,kotlin.Function0)"
    ) {
        var isUiModeLightState by remember {
            mutableStateOf(Indeterminate)
        }
        Row(
            Modifier
                .triStateToggleable(isUiModeLightState) {
                    isUiModeLightState = when (isUiModeLightState) {
                        On -> Off
                        Off -> On
                        Indeterminate -> On
                    }
                }
                .padding(Default),
            verticalAlignment = Alignment.CenterVertically,
        ) {
            Switch(
                checked = isUiModeLightState == On,
                onCheckedChange = {
                    isUiModeLightState = when (isUiModeLightState) {
                        On -> Off
                        Off -> On
                        Indeterminate -> On
                    }
                },
                thumbContent = {
                    Icon(
                        imageVector =
                        when (isUiModeLightState) {
                            On -> Icons.Default.LightMode
                            Off -> Icons.Default.ModeNight
                            Indeterminate -> Icons.Default.Brightness6
                        },
                        contentDescription = "UI mode"
                    )
                }
            )
            Spacer(modifier = Modifier.width(Default))
            Text(
                text = when (isUiModeLightState) {
                    On -> "Light Mode"
                    Off -> "Night Mode"
                    Indeterminate -> "System default"
                }
            )
            Spacer(modifier = Modifier.weight(1f))
            IconButton(onClick = { isUiModeLightState = Indeterminate }) {
                Icon(
                    imageVector = Icons.Default.BrightnessAuto,
                    contentDescription = "Auto Brightness"
                )
            }
        }
    }
}

@Composable
private fun Toggleable(onShowCodeSnippetClick: (Int) -> Unit) {
    ModifierExample(
        title = AnnotatedString("Toggleable"),
        body = buildAnnotatedString {
            append("If you want to make an item support on/off capabilities without being part of a set, consider using ")
            withStyle(
                MaterialTheme.typography.bodyMedium.copy(fontWeight = FontWeight.Bold)
                    .toSpanStyle()
            ) {
                append("Modifier.toggleable")
            }
        },
        codeSnippetImage = R.drawable.modifier_toggleable,
        onShowCodeSnippetClick = onShowCodeSnippetClick,
        documentationLink = "https://developer.android.com/reference/kotlin/androidx/compose/ui/Modifier#(androidx.compose.ui.Modifier).toggleable(kotlin.Boolean,kotlin.Boolean,androidx.compose.ui.semantics.Role,kotlin.Function1)"
    ) {
        var isToggled by remember {
            mutableStateOf(false)
        }
        Row(
            Modifier
                .fillMaxWidth()
                .toggleable(isToggled) {
                    isToggled = it
                }
                .padding(Default), verticalAlignment = Alignment.CenterVertically
        ) {
            Icon(
                imageVector = if (isToggled) Icons.Filled.House else Icons.Outlined.House,
                contentDescription = "Toggleable Icon",
            )
            Spacer(modifier = Modifier.width(Default))
            Text(text = "This row is toggleable")
        }
    }
}

@Suppress("t")
@Composable
private fun Selectable(onShowCodeSnippetClick: (Int) -> Unit) {
    ModifierExample(
        title = AnnotatedString("Selectable and Selectable Group"),
        body = buildAnnotatedString {
            append("The ")
            withStyle(
                MaterialTheme.typography.bodyMedium.copy(fontWeight = FontWeight.Bold)
                    .toSpanStyle()
            ) {
                append("selectableGroup ")
            }
            append("modifier is just an accessibility purpose modifier that group a list of ")
            withStyle(
                MaterialTheme.typography.bodyMedium.copy(fontWeight = FontWeight.Bold)
                    .toSpanStyle()
            ) {
                append("selectable ")
            }
            append("modifier, and Selectable modifier configures a component to be selectable.")
            append("\n" + "Usually these modifiers used where only one item can be selected at any point, if ")
        },
        codeSnippetImage = R.drawable.modifier_selectable,
        onShowCodeSnippetClick = onShowCodeSnippetClick,
        documentationLink = "https://developer.android.com/reference/kotlin/androidx/compose/ui/Modifier#(androidx.compose.ui.Modifier).selectable(kotlin.Boolean,kotlin.Boolean,androidx.compose.ui.semantics.Role,kotlin.Function0)"
    ) {
        var currentSelectedText by remember {
            mutableIntStateOf(-1)
        }
        Column(modifier = Modifier.selectableGroup()) {
            Text(
                text = "This text is selectable ${if (currentSelectedText == 1) "✅" else ""}",
                Modifier.selectable(selected = currentSelectedText == 1) {
                    currentSelectedText = if (currentSelectedText == 1) -1 else 1
                }
            )
            Text(
                text = "And this one is selectable too! ${if (currentSelectedText == 2) "✅" else ""}",
                Modifier
                    .selectable(
                        indication = rememberRipple(),
                        interactionSource = remember {
                            MutableInteractionSource()
                        },
                        selected = currentSelectedText == 2,
                        onClick = {
                            currentSelectedText = if (currentSelectedText == 2) -1 else 2
                        })
            )
        }
    }
}

@Composable
private fun Clickable(
    onShowCodeSnippetClick: (Int) -> Unit,
    coroutineScope: CoroutineScope,
    snackbarHostState: SnackbarHostState
) {
    ModifierExample(
        title = "Clickable", body = "Adds on click action",
        codeSnippetImage = R.drawable.modifier_clickable,
        onShowCodeSnippetClick = onShowCodeSnippetClick,
        documentationLink = "https://developer.android.com/reference/kotlin/androidx/compose/ui/Modifier#(androidx.compose.ui.Modifier).clickable(kotlin.Boolean,kotlin.String,androidx.compose.ui.semantics.Role,kotlin.Function0)",
    ) {
        Text("This text now is Clickable", modifier = Modifier
            .clickable(
                indication = rememberRipple(),
                interactionSource = remember { MutableInteractionSource() }) {
                coroutineScope.launch {
                    snackbarHostState.showSnackbar(
                        "Text Clicked with one tap",
                        withDismissAction = true
                    )
                }
            }
        )
    }
}

@Composable
private fun CombinedClickable(
    onShowCodeSnippetClick: (Int) -> Unit,
    coroutineScope: CoroutineScope,
    snackbarHostState: SnackbarHostState
) {
    ModifierExample(
        title = "Combined Clickable",
        body = "This modifier allows you to set listeners to a click, double click and long click actions",
        codeSnippetImage = R.drawable.modifier_combined_clickable,
        onShowCodeSnippetClick = onShowCodeSnippetClick,
        documentationLink = "https://developer.android.com/reference/kotlin/androidx/compose/ui/Modifier#(androidx.compose.ui.Modifier).combinedClickable(kotlin.Boolean,kotlin.String,androidx.compose.ui.semantics.Role,kotlin.String,kotlin.Function0,kotlin.Function0,kotlin.Function0)"
    ) {
        @OptIn(ExperimentalFoundationApi::class)
        Text(
            "This text now can handle: normal click, long click, double click",
            modifier = Modifier
                .combinedClickable(
                    onLongClick = {
                        coroutineScope.launch {
                            snackbarHostState.showSnackbar(
                                "Text clicked with long tap",
                                withDismissAction = true
                            )
                        }
                    },
                    onDoubleClick = {
                        coroutineScope.launch {
                            snackbarHostState.showSnackbar(
                                "Text clicked with double tap",
                                withDismissAction = true,
                            )
                        }
                    },
                    onClick = {
                        coroutineScope.launch {
                            snackbarHostState.showSnackbar(
                                "Text clicked with one tap",
                                withDismissAction = true,
                            )
                        }
                    })
        )
    }
}

@LightAndNightPreview
@Composable
fun ActionModifiersScreenPreview() {
    ComposePlaygroundTheme {
        ActionModifiersScreen() {}
    }
}