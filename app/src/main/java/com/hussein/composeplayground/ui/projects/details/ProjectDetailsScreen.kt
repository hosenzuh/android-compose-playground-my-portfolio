package com.hussein.composeplayground.ui.projects.details

import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.ExperimentalLayoutApi
import androidx.compose.foundation.layout.FlowRow
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.pager.HorizontalPager
import androidx.compose.foundation.pager.rememberPagerState
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.AssistChip
import androidx.compose.material3.ElevatedCard
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.DisposableEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalLifecycleOwner
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.viewinterop.AndroidView
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleEventObserver
import androidx.media3.common.MediaItem
import androidx.media3.exoplayer.ExoPlayer
import androidx.media3.ui.PlayerView
import coil.compose.AsyncImage
import com.hussein.composeplayground.domain.base.State
import com.hussein.composeplayground.model.Project
import com.hussein.composeplayground.ui.theme.Default
import com.hussein.composeplayground.ui.theme.HalfDefault


@OptIn(ExperimentalFoundationApi::class, ExperimentalLayoutApi::class)
@Composable
fun ProjectDetailsScreen(modifier: Modifier = Modifier, projectState: State<Project>) {


    when (projectState) {
        is State.Success.Data -> {
            projectState.data.let { project ->

                val context = LocalContext.current
                val player = remember {
                    ExoPlayer.Builder(context).build()
                }
                val lifeCycleOwner = LocalLifecycleOwner.current
                var lifecycle by remember {
                    mutableStateOf(Lifecycle.Event.ON_CREATE)
                }

                DisposableEffect(key1 = lifeCycleOwner) {
                    val observer = LifecycleEventObserver { _, event ->
                        lifecycle = event
                    }

                    lifeCycleOwner.lifecycle.addObserver(observer)

                    onDispose {
                        lifeCycleOwner.lifecycle.removeObserver(observer)
                        player.release()
                    }
                }


                Column(modifier.verticalScroll(rememberScrollState())) {
                    if (project.video != null) {
                        VideoItem(
                            modifier = Modifier.aspectRatio(2f),
                            player,
                            lifecycle,
                            project.video
                        )
                    }
                    Spacer(modifier = Modifier.height(Default))
                    val pagerState = rememberPagerState(
                        initialPage = 0,
                        initialPageOffsetFraction = 0f
                    ) { project.images.size }
                    HorizontalPager(state = pagerState) {
                        ElevatedCard {
                            AsyncImage(
                                modifier = Modifier
                                    .height(200.dp)
                                    .width(100.dp),
                                model = project.images[it],
                                contentDescription = null
                            )
                        }
                    }
                    Spacer(modifier = Modifier.height(Default))
                    Text(text = project.name, style = MaterialTheme.typography.titleLarge)
                    Spacer(modifier = Modifier.height(Default))
                    FlowRow {
                        project.skills.forEach {
                            AssistChip(
                                onClick = {},
                                label = { Text(text = it) },
                                modifier = Modifier.padding(end = HalfDefault)
                            )
                        }
                    }
                    Text(
                        text = project.description,
                        style = MaterialTheme.typography.bodyLarge
                    )
                }
            }
        }

        else -> {}
    }

}

@Composable
fun VideoItem(
    modifier: Modifier = Modifier,
    player: ExoPlayer,
    lifecycle: Lifecycle.Event,
    videoUrl: String,
) {
    AndroidView(
        modifier = modifier,
        factory = { context ->
            PlayerView(context).also {
                it.player = player.also {
                    it.setMediaItem(MediaItem.fromUri(videoUrl))
                    it.prepare()
                    it.playWhenReady = true
                }
            }
        },
        update = {
            when (lifecycle) {
                Lifecycle.Event.ON_PAUSE -> {
                    it.onPause()
                    it.player?.pause()
                }

                Lifecycle.Event.ON_RESUME -> {
                    it.onResume()
                    it.player?.play()
                }

                else -> Unit
            }
        })
}

@Preview
@Composable
fun VideoItemPreview() {
}

@Preview
@Composable
fun ProjectDetailsScreenPreview() {
    ProjectDetailsScreen(
        projectState = State.success(
            Project(
                id = 2319,
                name = "Nestor Murphy",
                description = "consectetuer",
                video = null,
                link = null,
                workExpertiseId = null,
                images = listOf(),
                skills = listOf(),
                logo = null
            )
        )
    )
}