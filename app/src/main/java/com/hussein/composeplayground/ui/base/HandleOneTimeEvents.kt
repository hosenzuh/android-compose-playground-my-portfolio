package com.hussein.composeplayground.ui.base

import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.receiveAsFlow

interface HandleOneTimeEvents<T> {
    val _messages: Channel<T>

    val messages: Flow<T>
        get() = _messages.receiveAsFlow()
}

class MessagesHandler : HandleOneTimeEvents<String> {
    override val _messages: Channel<String>
        get() = Channel()
    override val messages: Flow<String>
        get() = super.messages
}