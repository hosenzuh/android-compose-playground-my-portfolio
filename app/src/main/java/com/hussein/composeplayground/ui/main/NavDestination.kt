package com.hussein.composeplayground.ui.main

import androidx.navigation.NavType
import androidx.navigation.navArgument
import com.hussein.composeplayground.model.Project
import kotlinx.serialization.Serializable

@Serializable
sealed class NavDestination(
    val title: String? = null,
    val isTopLevelDestination: Boolean = false
) {

    @Serializable
    data object Home : NavDestination(
        title = "Hussein Al-Zuhile",
        isTopLevelDestination = true
    )

    @Serializable
    data object Projects : NavDestination("Projects", isTopLevelDestination = true)

    @Serializable
    data class ProjectDetails(val project: Project) : NavDestination("Project Details")

    @Serializable
    data object WorkExpertise :
        NavDestination("Work Expertise", isTopLevelDestination = true)

    @Serializable
    data object Articles :
        NavDestination(title = "Articles", isTopLevelDestination = true)

    @Serializable
    data object GraphicModifiers : NavDestination(isTopLevelDestination = true)

    @Serializable
    data object ActionModifiers : NavDestination(isTopLevelDestination = true)

    @Serializable
    data object AlignmentModifiers :
        NavDestination(isTopLevelDestination = true)

    @Serializable
    data object AnimationModifiers :
        NavDestination(isTopLevelDestination = true)

    @Serializable
    data object BorderModifiers : NavDestination(isTopLevelDestination = true)

    @Serializable
    data object FocusModifiers : NavDestination(isTopLevelDestination = true)

    @Serializable
    data object KeyboardModifiers :
        NavDestination(isTopLevelDestination = true)

    @Serializable
    data object LayoutModifiers : NavDestination(isTopLevelDestination = true)

    @Serializable
    data object PointerModifiers : NavDestination(isTopLevelDestination = true)

    @Serializable
    data object PositionModifiers :
        NavDestination(isTopLevelDestination = true)

    @Serializable
    data object SemanticsModifiers :
        NavDestination(isTopLevelDestination = true)

    @Serializable
    data object OtherModifiers: NavDestination(isTopLevelDestination = true)
}
