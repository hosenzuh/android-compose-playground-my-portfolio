package com.hussein.composeplayground.ui.main

import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Phone
import androidx.compose.material3.BottomAppBar
import androidx.compose.material3.FloatingActionButton
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import com.hussein.composeplayground.R
import com.hussein.composeplayground.ui.theme.ComposePlaygroundTheme
import com.hussein.composeplayground.ui.theme.QuarterDefault
import com.hussein.composeplayground.util.GITLAB_URL
import com.hussein.composeplayground.util.INDEED_CV_URL
import com.hussein.composeplayground.util.IntentDirection
import com.hussein.composeplayground.util.IntentManager.startIntentDirection
import com.hussein.composeplayground.util.IntentManager.viewUrl
import com.hussein.composeplayground.util.LINKEDIN_URL
import com.hussein.composeplayground.util.MY_PHONE_NUMBER

@Composable
fun BottomBar(modifier: Modifier = Modifier) {
    val context = LocalContext.current
    BottomAppBar(modifier = modifier, floatingActionButton = {
        FloatingActionButton(onClick = {
            context.startIntentDirection(
                IntentDirection.OpenDialer(
                    MY_PHONE_NUMBER
                )
            )
        }) {
            Icon(
                imageVector = Icons.Default.Phone,
                contentDescription = "Call me"
            )
        }
    }, actions = {
        IconButton(onClick = {
            context.viewUrl(GITLAB_URL)
        }) {
            Icon(
                painter = painterResource(id = R.drawable.ic_gitlab),
                contentDescription = "Gitlab",
                modifier = Modifier.padding(QuarterDefault)
            )
        }
        IconButton(onClick = {
            context.viewUrl(LINKEDIN_URL)
        }) {
            Icon(
                painter = painterResource(id = R.drawable.ic_linkedin),
                contentDescription = "Linkedin",
                modifier = Modifier.padding(QuarterDefault),
            )
        }
        IconButton(onClick = {
            context.startIntentDirection(IntentDirection.ViewUrl(INDEED_CV_URL))
        }) {
            Icon(
                painter = painterResource(id = R.drawable.ic_cv),
                contentDescription = "CV",
                modifier = Modifier
                    .fillMaxSize()
                    .padding(QuarterDefault),
            )
        }
    })
}

@Preview
@Composable
fun BottomBarPreview() {
    ComposePlaygroundTheme {
        BottomBar()
    }
}