package com.hussein.composeplayground.ui.base.components

import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.padding
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ReportProblem
import androidx.compose.material3.ElevatedCard
import androidx.compose.material3.Icon
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.ExperimentalTextApi
import androidx.compose.ui.tooling.preview.Preview
import com.hussein.composeplayground.ui.theme.Default
import com.hussein.composeplayground.ui.theme.HalfDefault

@OptIn(ExperimentalTextApi::class)
@Composable
fun NoteCard(modifier: Modifier = Modifier, text: @Composable (() -> Unit)) {
    ElevatedCard(modifier.padding(Default)) {
        Row(Modifier.padding(HalfDefault), verticalAlignment = Alignment.CenterVertically) {
            Icon(
                imageVector = Icons.Default.ReportProblem,
                contentDescription = "Caution",
                Modifier
                    .align(Alignment.Top)
                    .padding(end = HalfDefault)
            )
            Surface {
                text()
            }
        }
    }
}

@Preview
@Composable
fun NoteCardPreview() {
    NoteCard {
        Text(text = "Warning")
    }
}