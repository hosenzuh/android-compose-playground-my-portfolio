package com.hussein.composeplayground.ui.modifiers.graphicModifiers

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.ColumnScope
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.Checkbox
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Slider
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableFloatStateOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clipToBounds
import androidx.compose.ui.draw.drawWithContent
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.PointMode
import androidx.compose.ui.graphics.TransformOrigin
import androidx.compose.ui.graphics.graphicsLayer
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import com.hussein.composeplayground.R
import com.hussein.composeplayground.ui.base.components.ModifierExample
import com.hussein.composeplayground.ui.theme.Default


@Composable
fun ColumnScope.GraphicModifiers(onShowCodeSnippetClick: (Int) -> Unit) {
    Text(
        text = "Graphic Modifiers",
        style = MaterialTheme.typography.titleLarge.copy(fontWeight = FontWeight.Bold),
    )
    ModifierExample(
        title = "Graphics Layer",
        body = "This modifier gives you a scope to apply transformations to the composable (Like Scaling, Translation, Rotation etc..)",
        codeSnippetImage = R.drawable.modifier_graphic_layer,
        documentationLink = "https://developer.android.com/jetpack/compose/graphics/draw/modifiers#graphicsLayer",
        onShowCodeSnippetClick = onShowCodeSnippetClick,
    ) {
        var scaleX by rememberSaveable {
            mutableFloatStateOf(1f)
        }
        var scaleY by rememberSaveable {
            mutableFloatStateOf(1f)
        }
        var translationX by rememberSaveable {
            mutableFloatStateOf(0f)
        }
        var translationY by rememberSaveable {
            mutableFloatStateOf(0f)
        }
        var rotationX by rememberSaveable {
            mutableFloatStateOf(0f)
        }
        var rotationY by rememberSaveable {
            mutableFloatStateOf(0f)
        }
        var rotationZ by rememberSaveable {
            mutableFloatStateOf(0f)
        }
        var transformOrigin by remember {
            mutableStateOf(TransformOrigin(0.5f, 0.5f))
        }
        var alpha by rememberSaveable {
            mutableFloatStateOf(1f)
        }
        var isClippedToBound by rememberSaveable {
            mutableStateOf(true)
        }
        SliderWithTitle(
            title = "ScaleX",
            value = scaleX,
            onValueChange = { scaleX = it },
            valueRange = 0f..2f
        )
        SliderWithTitle(
            title = "ScaleY",
            value = scaleY,
            onValueChange = { scaleY = it },
            valueRange = 0f..2f
        )
        SliderWithTitle(
            title = "TranslationX",
            value = translationX,
            onValueChange = { translationX = it },
            valueRange = 0f..100f
        )
        SliderWithTitle(
            title = "Translation",
            value = translationY,
            onValueChange = { translationY = it },
            valueRange = 0f..100f
        )
        SliderWithTitle(
            title = "RotationX",
            value = rotationX,
            onValueChange = { rotationX = it },
            valueRange = 0f..360f
        )
        SliderWithTitle(
            title = "RotationY",
            value = rotationY,
            onValueChange = { rotationY = it },
            valueRange = 0f..360f
        )
        SliderWithTitle(
            title = "RotationZ",
            value = rotationZ,
            onValueChange = { rotationZ = it },
            valueRange = 0f..360f
        )
        SliderWithTitle(
            title = "TransformOriginX",
            value = transformOrigin.pivotFractionX,
            onValueChange = { transformOrigin = transformOrigin.copy(pivotFractionX = it) },
            valueRange = 0f..1f
        )
        SliderWithTitle(
            title = "TransformOriginY",
            value = transformOrigin.pivotFractionY,
            onValueChange = { transformOrigin = transformOrigin.copy(pivotFractionY = it) },
            valueRange = 0f..1f
        )
        SliderWithTitle(
            title = "Alpha",
            value = alpha,
            onValueChange = { alpha = it },
            valueRange = 0f..1f
        )
        Row(verticalAlignment = Alignment.CenterVertically) {
            Checkbox(checked = isClippedToBound, onCheckedChange = { isClippedToBound = it })
            Text(text = "Clip to bound")
        }

        Image(
            painter = painterResource(id = R.drawable.hussein_al_zuhile),
            contentDescription = "My Picture",
            modifier = Modifier
                .align(Alignment.CenterHorizontally)
                .fillMaxWidth(0.5f)
                .aspectRatio(1f)
                .run { if (isClippedToBound) this.then(clipToBounds()) else this }
                .graphicsLayer {
                    this.scaleX = scaleX
                    this.scaleY = scaleY
                    this.translationX = translationX
                    this.translationY = translationY
                    this.transformOrigin = transformOrigin
                    this.rotationX = rotationX
                    this.rotationY = rotationY
                    this.rotationZ = rotationZ
                    this.alpha = alpha
                }
                .drawWithContent {
                    drawContent()
                    drawPoints(
                        listOf(
                            Offset(
                                size.width * transformOrigin.pivotFractionX,
                                size.height * transformOrigin.pivotFractionY,
                            )
                        ),
                        strokeWidth = 10f,
                        pointMode = PointMode.Points,
                        color = Color.Red,
                    )
                }
        )
    }
}

@Composable
fun ColumnScope.SliderWithTitle(
    title: String,
    value: Float,
    onValueChange: (Float) -> Unit,
    valueRange: ClosedFloatingPointRange<Float>
) {
    Text(text = "$title: $value")
    Slider(
        value = value,
        onValueChange = onValueChange,
        valueRange = valueRange,
        modifier = Modifier.padding(Default)
    )
}

@Preview
@Composable
fun GraphicModifiersPreview() {
    Surface {
        Column(Modifier.verticalScroll(rememberScrollState())) {
            GraphicModifiers({})
        }
    }
}