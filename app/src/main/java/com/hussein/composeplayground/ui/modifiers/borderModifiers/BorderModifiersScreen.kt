package com.hussein.composeplayground.ui.modifiers.borderModifiers

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.hussein.composeplayground.R
import com.hussein.composeplayground.ui.base.components.ModifierExample
import com.hussein.composeplayground.ui.theme.ComposePlaygroundTheme
import com.hussein.composeplayground.ui.theme.primaryColors

@Composable
fun BorderModifiersScree(
    modifier: Modifier = Modifier,
    onShowCodeSnippetClick: (imageResId: Int) -> Unit
) {
    Box(modifier) {
        ModifierExample(
            title = "Adding Border to composable",
            body = "There are three versions of this modifier, using BorderStroke,using width with color and using width with brush\n" +
                    "The BorderStroke can be created using one of the two other options, so the two other options just another way to create the BorderStroke internally\n" +
                    "I used the three ways on the same box with padding between them",
            codeSnippetImage = R.drawable.modifier_border,
            onShowCodeSnippetClick = onShowCodeSnippetClick
        ) {
            Box(
                modifier = Modifier
                    .fillMaxWidth(0.3f)
                    .aspectRatio(1f)
                    .border(
                        BorderStroke(10.dp, Brush.linearGradient(primaryColors)),
                        RoundedCornerShape(10.dp)
                    )
                    .padding(10.dp)
                    .border(8.dp, Brush.radialGradient(primaryColors), CircleShape)
                    .padding(20.dp)
                    .border(6.dp, Color.Yellow)
            )
        }
    }
}

@Preview
@Composable
fun BorderModifiersScreePreview() {
    ComposePlaygroundTheme {
        BorderModifiersScree {

        }
    }
}