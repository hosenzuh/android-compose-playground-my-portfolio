package com.hussein.composeplayground.ui.modifiers.alignmentModifiers

import androidx.compose.animation.core.animate
import androidx.compose.foundation.background
import androidx.compose.foundation.gestures.detectTapGestures
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableFloatStateOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.BiasAlignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.drawWithContent
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.input.pointer.pointerInput
import androidx.compose.ui.layout.onGloballyPositioned
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.IntSize
import com.hussein.composeplayground.R
import com.hussein.composeplayground.ui.base.components.ModifierExample
import com.hussein.composeplayground.ui.base.components.NoteCard
import com.hussein.composeplayground.ui.theme.ComposePlaygroundTheme
import kotlinx.coroutines.launch

@Composable
fun AlignmentModifiersScreen(modifier: Modifier = Modifier, onShowCodeSnippetClick: (Int) -> Unit) {
    Column(modifier.verticalScroll(rememberScrollState())) {
        ModifierExample(
            title = "Align in Box Scope",
            body = "Pull the content element to a specific Alignment within the Box.",
            codeSnippetImage = R.drawable.modifier_align,
            onShowCodeSnippetClick = onShowCodeSnippetClick
        ) {
            var boxContainerSize by remember {
                mutableStateOf(IntSize.Zero)
            }
            var innerBoxAlignment by remember {
                mutableStateOf(Alignment.TopStart)
            }

            Text(text = "Click wherever you want to align the box by to that point")
            Box(
                Modifier
                    .fillMaxWidth()
                    .onGloballyPositioned {
                        boxContainerSize = it.size
                    }
                    .aspectRatio(1f)
                    .pointerInput(Unit) {
                        detectTapGestures { tapOffset ->
                            val widthPercent = 100 * tapOffset.x / boxContainerSize.width
                            val heightPercent = 100 * tapOffset.y / boxContainerSize.height
                            println("$widthPercent $heightPercent  ${boxContainerSize.width / tapOffset.x} ${boxContainerSize.height / tapOffset.y}")
                            innerBoxAlignment = when {
                                widthPercent < 33 && heightPercent < 33 -> Alignment.TopStart
                                widthPercent in 33f..66f && heightPercent < 33 -> Alignment.TopCenter
                                widthPercent > 66 && heightPercent < 33 -> Alignment.TopEnd
                                widthPercent < 33 && heightPercent in 33f..66f -> Alignment.CenterStart
                                widthPercent in 33f..66f && heightPercent in 33f..66f -> Alignment.Center
                                widthPercent > 66 && heightPercent in 33f..66f -> Alignment.CenterEnd
                                widthPercent < 33 && heightPercent > 66 -> Alignment.BottomStart
                                widthPercent in 33f..66f && heightPercent > 66 -> Alignment.BottomCenter
                                widthPercent > 66 && heightPercent > 66 -> Alignment.BottomEnd
                                else -> {
                                    Alignment.TopStart
                                }
                            }
                        }
                    }
            ) {
                Box(
                    modifier = Modifier
                        .fillMaxWidth(0.33f)
                        .aspectRatio(1f)
                        .background(MaterialTheme.colorScheme.primary)
                        .align(innerBoxAlignment)
                )
            }
        }
        ModifierExample(
            title = "Custom Alignment in Box Scope",
            body = "If you look at the definition of the Alignment.TopStart value you will notice that the Alignment constructor consist of two bias, " +
                    "horizontal and vertical one, " +
                    "the value -1 means Start, 0 means Center and 1 means End " +
                    "so we can use this float numbers to animate and create our custom Alignments (and animate the changing between the alignments if we wanted to, as in this example)",
            codeSnippetImage = R.drawable.modifier_align_custom,
            onShowCodeSnippetClick = onShowCodeSnippetClick
        ) {
            var boxContainerSize by remember {
                mutableStateOf(IntSize.Zero)
            }
            var innerBoxHorizontalBias by remember {
                mutableFloatStateOf(0f)
            }
            var innerBoxVerticalBias by remember {
                mutableFloatStateOf(0f)
            }
            val coroutineScope = rememberCoroutineScope()
            Text(text = "Click wherever you want to align the box by to that point")
            Box(
                Modifier
                    .fillMaxWidth()
                    .onGloballyPositioned {
                        boxContainerSize = it.size
                    }
                    .aspectRatio(1f)
                    .pointerInput(Unit) {
                        detectTapGestures { tapOffset ->
                            val widthPercent = 100 * tapOffset.x / boxContainerSize.width
                            val heightPercent = 100 * tapOffset.y / boxContainerSize.height
                            // We subtract 50 because we want the 0 bias be in the center,
                            // for example if its clicked in the middle and the percent is 50%
                            // (50 - 50) / 50 = 0
                            // and if its in the very end
                            // (100 - 50)/50 = 1
                            coroutineScope.launch {
                                animate(
                                    initialValue = innerBoxHorizontalBias,
                                    targetValue = (widthPercent - 50) / 50
                                ) { value, _ ->
                                    innerBoxHorizontalBias = value
                                }
                            }
                            coroutineScope.launch {
                                animate(
                                    initialValue = innerBoxVerticalBias,
                                    targetValue = (heightPercent - 50) / 50
                                ) { value, _ ->
                                    innerBoxVerticalBias = value
                                }
                            }
                        }
                    }
            ) {
                Box(
                    modifier = Modifier
                        .fillMaxWidth(0.33f)
                        .aspectRatio(1f)
                        .background(MaterialTheme.colorScheme.primary)
                        .align(BiasAlignment(innerBoxHorizontalBias, innerBoxVerticalBias))
                )
            }
        }
        NoteCard(Modifier.fillMaxWidth()) {
            Text(text = "The same Modifier.align API exist for Column and Row scopes, but either only Horizontal alignment (in Column) or only Vertical (in Row)")
        }
        ModifierExample(
            title = "Align By in Row Scope",
            body = "Each two composable that next to each other are positioned using the passed HorizontalAlignmentLine",
            codeSnippetImage = R.drawable.modifier_align_by,
            onShowCodeSnippetClick = onShowCodeSnippetClick
        ) {
            Row(Modifier) {
                Box(
                    modifier = Modifier
                        .weight(1f)
                        .aspectRatio(1f)
                        .alignBy { it.measuredHeight }
                        .background(Color.Magenta)
                        .drawWithContent {
                            drawContent()
                            drawLine(
                                Color.Black,
                                start = Offset(size.width / 2, size.height),
                                end = Offset(size.width, size.height),
                                strokeWidth = 8f
                            )
                        }
                )
                Box(
                    modifier = Modifier
                        .weight(1f)
                        .aspectRatio(1f)
                        .alignBy { it.measuredHeight / 2 }
                        .background(Color.Blue)
                        .drawWithContent {
                            drawContent()
                            drawLine(
                                Color.Black,
                                start = Offset(0f, size.height / 2),
                                end = Offset(size.width, size.height / 2),
                                strokeWidth = 8f
                            )
                        }
                )
                Box(
                    modifier = Modifier
                        .weight(1f)
                        .aspectRatio(1f)
                        .alignBy { it.measuredHeight / 4 }
                        .background(Color.Green)
                        .drawWithContent {
                            drawContent()
                            drawLine(
                                Color.Black,
                                start = Offset(0f, size.height / 4),
                                end = Offset(size.width, size.height / 4),
                                strokeWidth = 8f
                            )
                        }
                )
                Box(
                    modifier = Modifier
                        .weight(1f)
                        .aspectRatio(1f)
                        .alignByBaseline()
                        .background(Color.Red)
                        .drawWithContent {
                            drawContent()
                            drawLine(
                                Color.Black,
                                start = Offset(0f, size.height),
                                end = Offset(size.width, size.height),
                                strokeWidth = 8f
                            )
                        }
                )
            }
            // TODO: Add Align by Horizontal Alignment Line When understand it

            NoteCard(Modifier.fillMaxWidth()) {
                Text(text = "The same Modifier.alignBy API exist for Column with the orientation difference")
            }
        }
    }
}

@Preview
@Composable
fun AlignmentModifiersScreenPreview() {
    ComposePlaygroundTheme {
        AlignmentModifiersScreen {}
    }
}