package com.hussein.composeplayground.ui.modifiers.pointerModifiers

import android.util.Half
import android.widget.Toast
import androidx.compose.animation.core.LinearEasing
import androidx.compose.animation.core.RepeatMode
import androidx.compose.animation.core.animateFloatAsState
import androidx.compose.animation.core.infiniteRepeatable
import androidx.compose.animation.core.tween
import androidx.compose.foundation.clickable
import androidx.compose.foundation.gestures.detectTapGestures
import androidx.compose.foundation.hoverable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.interaction.collectIsHoveredAsState
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Call
import androidx.compose.material.icons.filled.Favorite
import androidx.compose.material.icons.outlined.Favorite
import androidx.compose.material.icons.outlined.FavoriteBorder
import androidx.compose.material.icons.twotone.Favorite
import androidx.compose.material3.Button
import androidx.compose.material3.HorizontalDivider
import androidx.compose.material3.Icon
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableFloatStateOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.rotate
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.input.pointer.PointerIcon
import androidx.compose.ui.input.pointer.pointerHoverIcon
import androidx.compose.ui.input.pointer.pointerInput
import androidx.compose.ui.input.pointer.pointerInteropFilter
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.tooling.preview.Preview
import com.hussein.composeplayground.ui.base.components.ModifierExample
import com.hussein.composeplayground.ui.base.components.NoteCard
import com.hussein.composeplayground.ui.theme.ComposePlaygroundTheme
import com.hussein.composeplayground.ui.theme.Default
import com.hussein.composeplayground.ui.theme.HalfDefault


@OptIn(ExperimentalComposeUiApi::class)
@Composable
fun PointerModifiersScreen(
    modifier: Modifier = Modifier,
    onShowCodeSnippetClick: (imageResId: Int) -> Unit
) {
    Column(
        modifier
            .fillMaxSize()
            .verticalScroll(rememberScrollState())
    ) {
        NoteCard {
            Text(text = "Please connect a physical mouse to your phone in this screen to understand the modifiers work better.")
        }

        ModifierExample(
            title = "Pointer Hover Icon",
            body = "This modifier allows you to set a special icon for the pointer when it hovers this composable\n" +
                    "If you are using your finger you can't see the effect of this modifier, connect a mouse to see the effect.\n" +
                    "You can also pass overrideDecedent parameter true to make the effect for the children also",
            codeSnippetImage = null,
            onShowCodeSnippetClick = onShowCodeSnippetClick
        ) {
            Button(
                onClick = { },
                Modifier
                    .pointerHoverIcon(
                        PointerIcon.Hand, true
                    )
            ) {
                Text(text = "This button has a special hover icon")
            }
        }

        ModifierExample(
            title = "Pointer Interop Filter",
            body = "This modifier allows you to intercept the pointer events (like actions,location and many more thing), " +
                    "it's actually provide a MotionEvent object that contains a lot of information you can use.",
            codeSnippetImage = null,
            onShowCodeSnippetClick = onShowCodeSnippetClick
        ) {
            Text(text = "In this example we just tracking the location of the pointer on the button")
            var x by rememberSaveable {
                mutableFloatStateOf(0f)
            }
            var y by rememberSaveable {
                mutableFloatStateOf(0f)
            }
            Button(
                onClick = { },
                Modifier
                    .pointerInteropFilter {
                        x = it.x
                        y = it.y
                        true
                    }
            ) {
                Text(text = "This button has pointer interop filter")
            }
            Text(text = "Pointer location now = x: $x, y: $y")
        }

        ModifierExample(
            title = "Pinter input",
            body = "This modifier is very useful when you have some lower level of pointer gestures detection you want to define, " +
                    "a custom one you build, or a combination of the predefined detectXXX extension functions.\n" +
                    "Notice that there are some APIs that are preferable to be used if the use-case you are handling is simple, like clicking using Modifier.clickable, dragging using draggable and so on, " +
                    "because these built in APIs are handling the semantics and accessibility issues perfectly",
            codeSnippetImage = null,
            onShowCodeSnippetClick = onShowCodeSnippetClick
        ) {

            Text(
                text = "In this example we defined a mutable text state, and two Text components, " +
                        "The first one takes a Unit as a key so it will never re-call the pointer input scope when the recomposition (or any state updates).\n" +
                        "However, in the second one we are using the text state to define the double tap detector, so we passed it as a key.\n" +
                        "Notice that both of texts use the latest state, so it depends on if you want the modifier to recreated on each update to the state or not."
            )

            val context = LocalContext.current
            var text by remember {
                mutableStateOf("")
            }
            TextField(value = text, onValueChange = { text = it })
            Text(
                text = "This text contains a modifier that detecting the double tap gestures",
                Modifier.pointerInput(Unit) {
                    detectTapGestures(onDoubleTap = {
                        Toast.makeText(
                            context,
                            "This button double tapped, the text is: $text",
                            Toast.LENGTH_SHORT
                        ).show()
                    })
                }
            )
            HorizontalDivider()
            Text(
                text = "This text contains a modifier that detecting the double tap gestures only if the field contains Hussein",
                Modifier.pointerInput(text) {
                    if (text == "Hussein") {
                        detectTapGestures(onDoubleTap = {
                            Toast.makeText(
                                context,
                                "This button double tapped, the text is: $text",
                                Toast.LENGTH_SHORT
                            ).show()
                        })
                    }
                }
            )
            NoteCard {
                Text(text = "As most of the APIs that take a key in jetpack compose, this modifier can take as much keys as you want also.")
            }
            Spacer(modifier = Modifier.weight(1f))
            ModifierExample(
                title = "Hoverable",
                body = "This modifier allows you to make a composable accept the hovering state",
                codeSnippetImage = null,
                onShowCodeSnippetClick = onShowCodeSnippetClick,
                modifier = Modifier.padding(HalfDefault)
            ) {
                var isFavorite by remember {
                    mutableStateOf(false)
                }
                val heartInteractionSource = remember { MutableInteractionSource() }
                val isHeartHovered by heartInteractionSource.collectIsHoveredAsState()
                val phoneInteractionSource = remember { MutableInteractionSource() }
                val isPhoneHovered by phoneInteractionSource.collectIsHoveredAsState()
                Row {
                    Icon(
                        imageVector = when {
                            isFavorite -> Icons.Filled.Favorite
                            isHeartHovered -> Icons.TwoTone.Favorite
                            else -> Icons.Outlined.FavoriteBorder
                        },
                        contentDescription = null,
                        Modifier
                            .hoverable(heartInteractionSource)
                            .clickable(indication = null, interactionSource = heartInteractionSource) {
                                isFavorite = !isFavorite
                            },
                        tint = Color.Red
                    )
                    Spacer(modifier = Modifier.width(Default))

                    val rotation by animateFloatAsState(
                        targetValue = if (isPhoneHovered) 60f else 0f, animationSpec = if (isPhoneHovered) infiniteRepeatable(
                            animation = tween(durationMillis = 500, easing = LinearEasing),
                            repeatMode = RepeatMode.Reverse
                        ) else tween(durationMillis = 500)
                    )
                    Icon(
                        imageVector = Icons.Default.Call,
                        contentDescription = null,
                        Modifier
                            .hoverable(phoneInteractionSource)
                            .rotate(rotation),
                        tint = Color.Green
                    )
                }
            }
        }
    }
}

@Preview
@Composable
fun PointerModifiersScreenPreview() {
    ComposePlaygroundTheme {
        PointerModifiersScreen {

        }
    }
}