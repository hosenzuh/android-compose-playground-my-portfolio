package com.hussein.composeplayground.ui.base.components

import android.content.Intent
import androidx.annotation.DrawableRes
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.defaultMinSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Code
import androidx.compose.material.icons.filled.Link
import androidx.compose.material3.Divider
import androidx.compose.material3.HorizontalDivider
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.drawWithContent
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.AnnotatedString
import androidx.compose.ui.tooling.preview.Preview
import androidx.core.net.toUri
import com.hussein.composeplayground.R
import com.hussein.composeplayground.ui.theme.Default
import com.hussein.composeplayground.ui.theme.HalfDefault
import com.hussein.composeplayground.ui.theme.QuarterDefault

@Composable
fun ModifierExample(
    title: String,
    body: String,
    @DrawableRes codeSnippetImage: Int?,
    onShowCodeSnippetClick: (imageResId: Int) -> Unit,
    modifier: Modifier = Modifier,
    documentationLink: String? = null,
    exampleContent: @Composable (() -> Unit),
) {
    val context = LocalContext.current
    Column(modifier) {
        Spacer(modifier = Modifier.height(Default))
        Row(Modifier.fillMaxWidth(), verticalAlignment = Alignment.CenterVertically) {
            Text(text = title, style = MaterialTheme.typography.titleMedium)
            Spacer(modifier = Modifier.weight(1f))
            codeSnippetImage?.let {
                IconButton(onClick = { onShowCodeSnippetClick(codeSnippetImage) }) {
                    Icon(imageVector = Icons.Default.Code, contentDescription = "Show the code")
                }
            }
            Spacer(
                modifier = Modifier
                    .width(HalfDefault)
                    .defaultMinSize(HalfDefault)
            )
            documentationLink?.let {
                IconButton(onClick = {
                    Intent(Intent.ACTION_VIEW, documentationLink?.toUri()).also {
                        context.startActivity(it)
                    }
                }) {
                    Icon(
                        imageVector = Icons.Default.Link,
                        contentDescription = "Open documentation link"
                    )
                }
            }
        }
        Spacer(modifier = Modifier.height(HalfDefault))
        Text(
            text = body,
            style = MaterialTheme.typography.bodyMedium
        )
        Spacer(modifier = Modifier.height(HalfDefault))
        exampleContent()
        Spacer(modifier = Modifier.height(QuarterDefault))
        HorizontalDivider()
    }
}

@Composable
fun ModifierExample(
    title: AnnotatedString,
    body: AnnotatedString,
    @DrawableRes codeSnippetImage: Int,
    onShowCodeSnippetClick: (imageResId: Int) -> Unit,
    documentationLink: String? = null,
    modifier: Modifier = Modifier,
    exampleContent: @Composable (() -> Unit),
) {
    val context = LocalContext.current
    Column(modifier) {
        Spacer(modifier = Modifier.height(Default))
        Row(Modifier.fillMaxWidth(), verticalAlignment = Alignment.CenterVertically) {
            Text(text = title, style = MaterialTheme.typography.titleMedium)
            Spacer(modifier = Modifier.weight(1f))
            IconButton(onClick = { onShowCodeSnippetClick(codeSnippetImage) }) {
                Icon(imageVector = Icons.Default.Code, contentDescription = "Show the code")
            }
            Spacer(
                modifier = Modifier
                    .width(HalfDefault)
                    .defaultMinSize(HalfDefault)
            )
            IconButton(onClick = {
                Intent(Intent.ACTION_VIEW, documentationLink?.toUri()).also {
                    context.startActivity(it)
                }
            }) {
                Icon(
                    imageVector = Icons.Default.Link,
                    contentDescription = "Open documentation link"
                )
            }
        }
        Spacer(modifier = Modifier.height(HalfDefault))
        Text(
            text = body,
            style = MaterialTheme.typography.bodyMedium
        )
        Spacer(modifier = Modifier.height(HalfDefault))
        exampleContent()
        Spacer(modifier = Modifier.height(QuarterDefault))
        HorizontalDivider()
    }
}

@Preview
@Composable
fun ModifierExamplePreview() {
    ModifierExample(
        title = "Modifier Name",
        body = "Modifier description",
        codeSnippetImage = R.drawable.modifier_draw_with_content,
        onShowCodeSnippetClick = {},
    ) {
        Text(text = "Look at this text that contains two rectangles one is drawn before (Behind) and another after (In front of) the content",
            modifier = Modifier
                .background(Color.White)
                .drawWithContent {
                    drawRect(Color.Yellow, size = size.copy(width = size.width / 2))
                    drawContent()
                    drawRect(
                        Color.Yellow,
                        alpha = 0.8f,
                        topLeft = Offset(size.width / 2, 0f),
                        size = size.copy(width = size.width / 2)
                    )
                }
                .padding(HalfDefault)
        )
    }
}
