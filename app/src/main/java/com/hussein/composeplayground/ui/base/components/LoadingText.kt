package com.hussein.composeplayground.ui.base.components

import androidx.compose.animation.animateColor
import androidx.compose.animation.core.LinearEasing
import androidx.compose.animation.core.RepeatMode
import androidx.compose.animation.core.animateFloat
import androidx.compose.animation.core.infiniteRepeatable
import androidx.compose.animation.core.rememberInfiniteTransition
import androidx.compose.animation.core.tween
import androidx.compose.foundation.layout.BoxScope
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.scale
import androidx.compose.ui.tooling.preview.Preview
import com.hussein.composeplayground.ui.theme.ComposePlaygroundTheme
import com.hussein.composeplayground.ui.theme.primaryColors

@Composable
fun LoadingText(modifier: Modifier = Modifier) {
    val infiniteTransition = rememberInfiniteTransition(label = "")
    val colorAnimation by infiniteTransition.animateColor(
        initialValue = primaryColors.first(),
        targetValue = primaryColors.last(),
        animationSpec = infiniteRepeatable(
            animation = tween(
                2000,
                easing = LinearEasing
            ), RepeatMode.Reverse
        ),
        label = ""
    )
    val scaleAnimation by infiniteTransition.animateFloat(
        initialValue = 1f,
        targetValue = 1.5f,
        animationSpec = infiniteRepeatable(
            animation = tween(
                2000,
                easing = LinearEasing
            ), RepeatMode.Reverse
        ),
        label = ""
    )
    Text(
        text = "Loading",
        modifier = modifier
            .scale(scaleAnimation),
        color = colorAnimation
    )
}

@Preview
@Composable
fun LoadingTextPreview() {
    ComposePlaygroundTheme {
        LoadingText()
    }
}