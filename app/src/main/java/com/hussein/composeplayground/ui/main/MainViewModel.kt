package com.hussein.composeplayground.ui.main


import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(
) : ViewModel() {

    val currentNavDestination = mutableStateOf<NavDestination?>(NavDestination.Home)
}