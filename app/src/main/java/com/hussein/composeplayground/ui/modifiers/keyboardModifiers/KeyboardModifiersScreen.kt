package com.hussein.composeplayground.ui.modifiers.keyboardModifiers

import androidx.compose.foundation.border
import androidx.compose.foundation.focusable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.ElevatedButton
import androidx.compose.material3.Snackbar
import androidx.compose.material3.SnackbarHost
import androidx.compose.material3.SnackbarHostState
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.focus.focusRequester
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.input.key.onKeyEvent
import androidx.compose.ui.input.key.onPreviewKeyEvent
import androidx.compose.ui.platform.LocalSoftwareKeyboardController
import androidx.compose.ui.tooling.preview.Preview
import com.hussein.composeplayground.R
import com.hussein.composeplayground.ui.base.components.ModifierExample
import com.hussein.composeplayground.ui.theme.ComposePlaygroundTheme
import com.hussein.composeplayground.ui.theme.Default
import com.hussein.composeplayground.ui.theme.HalfDefault
import kotlinx.coroutines.launch

@Composable
fun KeyboardModifiersScreen(
    modifier: Modifier = Modifier,
    onShowCodeSnippetClick: (imageResId: Int) -> Unit
) {
    Column(modifier) {
        ModifierExample(
            title = "On Key Event Listener and On Preview Key Event Listener",
            body = "This modifier allows you to intercept the keyboard event if the component or one of its children is focused\n" +
                    "OnPreviewKeyEvent will intercept from parent to children, and OnKeyEvent will intercept from child to parents.",
            codeSnippetImage = R.drawable.modifier_on_key_event_and_on_preview_key_event,
            onShowCodeSnippetClick = onShowCodeSnippetClick
        ) {
            val focusRequester = remember {
                FocusRequester()
            }
            val snackbarHostState = remember {
                SnackbarHostState()
            }
            val coroutineScope = rememberCoroutineScope()
            val keyboard = LocalSoftwareKeyboardController.current
            ElevatedButton(onClick = {
                focusRequester.requestFocus()
                keyboard?.show()
            }) {
                Text(text = "Focus on the inner box and show keyboard")
            }
            Box(
                Modifier
                    .border(HalfDefault, Color.Yellow)
                    .onPreviewKeyEvent { keyEvent1 ->
                        coroutineScope.launch {
                            snackbarHostState.showSnackbar("onPreviewKeyEvent outer box")
                        }
                        false
                    }
                    .onKeyEvent { keyEvent4 ->
                        coroutineScope.launch {
                            snackbarHostState.showSnackbar("onKeyEvent outer box")
                        }
                        false
                    }
            ) {
                Box(
                    Modifier
                        .padding(Default)
                        .fillMaxWidth(0.5f)
                        .aspectRatio(1f)
                        .border(HalfDefault, Color.Green)
                        .onPreviewKeyEvent { keyEvent2 ->
                            coroutineScope.launch {
                                snackbarHostState.showSnackbar("onPreviewKeyEvent inner box")
                            }
                            false
                        }
                        .onKeyEvent { keyEvent3 ->
                            coroutineScope.launch {
                                snackbarHostState.showSnackbar("onKeyEvent inner box")
                            }
                            false
                        }
                        .focusRequester(focusRequester)
                        .focusable()
                )
            }

            SnackbarHost(snackbarHostState) {
                Snackbar(snackbarData = it)
            }
        }
    }
}

@Preview
@Composable
fun KeyboardModifiersScreenPreview() {
    ComposePlaygroundTheme {
        KeyboardModifiersScreen {}
    }
}