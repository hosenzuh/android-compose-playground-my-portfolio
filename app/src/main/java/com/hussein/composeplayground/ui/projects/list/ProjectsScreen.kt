package com.hussein.composeplayground.ui.projects.list

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.core.LinearEasing
import androidx.compose.animation.core.RepeatMode
import androidx.compose.animation.core.animateFloat
import androidx.compose.animation.core.infiniteRepeatable
import androidx.compose.animation.core.rememberInfiniteTransition
import androidx.compose.animation.core.tween
import androidx.compose.animation.expandVertically
import androidx.compose.animation.shrinkVertically
import androidx.compose.animation.slideInVertically
import androidx.compose.animation.slideOutVertically
import androidx.compose.foundation.clickable
import androidx.compose.foundation.horizontalScroll
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.foundation.rememberScrollState
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Link
import androidx.compose.material3.AssistChip
import androidx.compose.material3.Divider
import androidx.compose.material3.ElevatedButton
import androidx.compose.material3.ElevatedCard
import androidx.compose.material3.HorizontalDivider
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Snackbar
import androidx.compose.material3.SnackbarHost
import androidx.compose.material3.SnackbarHostState
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.derivedStateOf
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.TileMode
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.style.LineBreak
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import coil.compose.AsyncImage
import com.hussein.composeplayground.domain.base.State
import com.hussein.composeplayground.model.Project
import com.hussein.composeplayground.ui.base.components.LoadingText
import com.hussein.composeplayground.ui.theme.Default
import com.hussein.composeplayground.ui.theme.DoubleDefault
import com.hussein.composeplayground.ui.theme.HalfDefault
import com.hussein.composeplayground.ui.theme.QuarterDefault
import com.hussein.composeplayground.util.IntentManager.viewUrl
import com.hussein.composeplayground.util.collectWithLifeCycle
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

@Composable
fun ProjectsScreen(
    modifier: Modifier = Modifier,
    projectsState: State<List<Project>>,
    messages: Flow<String>,
    onProjectClick: (Project) -> Unit,
    onRetryClicked: () -> Unit
) {
    val projectsScrollState = rememberLazyListState()
    val isTitleVisible by remember {
        derivedStateOf {
            projectsScrollState.firstVisibleItemIndex == 0 && projectsScrollState.firstVisibleItemScrollOffset == 0
        }
    }
    val infiniteTransition = rememberInfiniteTransition(label = "")
    val cardAlpha by infiniteTransition.animateFloat(
        initialValue = 1f,
        targetValue = 0.7f,
        animationSpec = infiniteRepeatable(tween(1000), repeatMode = RepeatMode.Reverse), label = ""
    )
    Column(
        modifier
            .fillMaxSize()
    ) {

        AnimatedVisibility(
            visible = isTitleVisible,
            enter = slideInVertically { -it } + expandVertically(),
            exit = slideOutVertically { -it } + shrinkVertically()
        ) {
            val textStyle = MaterialTheme.typography.titleMedium
            val fontSizeInPixels =
                with(LocalDensity.current) { textStyle.fontSize.toPx() }
            val fontSizeInPixelsDoubled = fontSizeInPixels * 2
            ElevatedCard(modifier = Modifier.alpha(cardAlpha)) {
                val offset by infiniteTransition.animateFloat(
                    initialValue = 0f,
                    targetValue = fontSizeInPixelsDoubled,
                    animationSpec = infiniteRepeatable(
                        tween(1000, easing = LinearEasing),
                    ), label = ""
                )
                val brush = Brush.linearGradient(
                    colors = listOf(
                        MaterialTheme.colorScheme.primary,
                        MaterialTheme.colorScheme.inversePrimary,
                    ),
                    start = Offset(offset, offset),
                    end = Offset(
                        offset + fontSizeInPixels,
                        offset + fontSizeInPixels
                    ),
                    tileMode = TileMode.Mirror,
                )
                Text(
                    text = "I have worked in many companies and as freelancer for more than 5 years!\n and I have implemented many applications in different domains and different requirements,\n Here Are some of these projects:",
                    textAlign = TextAlign.Center,
                    style = textStyle.copy(
                        brush,
                        lineBreak = LineBreak.Heading
                    ),
                    fontFamily = FontFamily.Monospace,
                    modifier = Modifier.padding(
                        horizontal = QuarterDefault,
                        vertical = HalfDefault
                    ),
                    color = MaterialTheme.colorScheme.secondary
                )
            }
        }

        val snackbarState = remember {
            SnackbarHostState()
        }
        messages.collectWithLifeCycle {
            snackbarState.showSnackbar(it)
        }

        Box(Modifier.fillMaxSize()) {
            when (projectsState) {
                is State.Loading.Generic -> {
                    LoadingText(Modifier.align(Alignment.Center))
                }

                is State.Success.Data -> {
                    LazyColumn(
                        Modifier.padding(top = HalfDefault),
                        state = projectsScrollState,
                        verticalArrangement = Arrangement.spacedBy(HalfDefault)
                    ) {
                        items(projectsState.data, key = { it.id }) {
                            ProjectItem(project = it, modifier = Modifier
                                .clickable { onProjectClick(it) })
                            HorizontalDivider(
                                Modifier.padding(
                                    horizontal = DoubleDefault,
                                    vertical = Default
                                )
                            )
                        }
                    }
                }

                is State.Failure -> {

                    ElevatedButton(
                        onClick = onRetryClicked,
                        modifier = Modifier.align(Alignment.Center)
                    ) {
                        Text(text = "Retry")
                    }

                    SnackbarHost(
                        snackbarState, modifier =
                        Modifier
                            .padding(Default)
                            .align(Alignment.BottomCenter)
                    ) {
                        Snackbar(it)
                    }
                }

                else -> {}
            }
        }

    }
}

@Preview(showSystemUi = true)
@Composable
fun ProjectsScreenPreview() {
    ProjectsScreen(
        projectsState = State.initial(),
        messages = flow { },
        onRetryClicked = {},
        onProjectClick = {})
}

@Composable
fun ProjectItem(modifier: Modifier = Modifier, project: Project) {
    ElevatedCard {
        Column(
            modifier
                .fillMaxWidth()
                .padding(HalfDefault)
        ) {
            Row(verticalAlignment = Alignment.CenterVertically) {
                project.logo?.let {
                    AsyncImage(
                        model = it, contentDescription = "Logo",
                        modifier = Modifier.size(30.dp),
                    )
                }
                Spacer(modifier = Modifier.width(QuarterDefault))
                Text(text = project.name, style = MaterialTheme.typography.titleLarge)
                project.link?.let {
                    val context = LocalContext.current
                    Spacer(modifier = Modifier.weight(1f))
                    IconButton(onClick = { context.viewUrl(it) }) {
                        Icon(
                            imageVector = Icons.Default.Link,
                            contentDescription = "Link to project"
                        )
                    }
                }
            }
            Spacer(modifier = Modifier.height(Default))
            Text(
                text = project.description,
                style = MaterialTheme.typography.bodyMedium,
                overflow = TextOverflow.Ellipsis,
                maxLines = 3
            )
            Spacer(modifier = Modifier.height(HalfDefault))
            if (project.images.isNotEmpty()) {
                Row(modifier = Modifier.horizontalScroll(rememberScrollState())) {
                    project.images.forEach {
                        ElevatedCard(
                            modifier = Modifier
                                .padding(horizontal = QuarterDefault)
                        ) {
                            AsyncImage(
                                modifier = Modifier
                                    .height(200.dp)
                                    .width(100.dp),
                                model = it.path,
                                contentDescription = null
                            )
                        }
                    }
                }
            }
            LazyRow {
                items(project.skills) {
                    AssistChip(
                        onClick = {},
                        label = { Text(text = it) },
                        modifier = Modifier.padding(end = HalfDefault)
                    )
                }
            }
        }
    }
}

@Preview
@Composable
fun ProjectPreview() {
    Column {
        ProjectItem(
            project = Project(
                id = 7124,
                name = "Vera O'Connor",
                description = "torquent",
                video = null,
                link = "",
                workExpertiseId = null,
                images = listOf(),
                skills = listOf("Skill 1", "Skill 2"),
                logo = "null"
            )
        )
    }
}