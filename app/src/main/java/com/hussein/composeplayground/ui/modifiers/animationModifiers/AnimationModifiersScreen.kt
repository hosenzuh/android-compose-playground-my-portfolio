@file:Suppress("t")

package com.hussein.composeplayground.ui.modifiers.animationModifiers

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.animation.animateContentSize
import androidx.compose.animation.core.Spring
import androidx.compose.animation.core.VisibilityThreshold
import androidx.compose.animation.core.spring
import androidx.compose.animation.core.tween
import androidx.compose.animation.fadeIn
import androidx.compose.animation.fadeOut
import androidx.compose.animation.slideInHorizontally
import androidx.compose.animation.slideOutHorizontally
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.ColumnScope
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.ElevatedButton
import androidx.compose.material3.Text
import androidx.compose.material3.TriStateCheckbox
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableIntStateOf
import androidx.compose.runtime.mutableStateListOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.state.ToggleableState.*
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.IntOffset
import androidx.compose.ui.unit.IntSize
import androidx.compose.ui.unit.dp
import com.hussein.composeplayground.R
import com.hussein.composeplayground.ui.base.components.ModifierExample
import com.hussein.composeplayground.ui.base.components.NoteCard
import com.hussein.composeplayground.ui.theme.ComposePlaygroundTheme
import com.hussein.composeplayground.ui.theme.Default
import com.hussein.composeplayground.ui.theme.HalfDefault

@OptIn(ExperimentalFoundationApi::class, ExperimentalAnimationApi::class)
@Composable
fun AnimationModifiersScreen(modifier: Modifier = Modifier, onShowCodeSnippetClick: (Int) -> Unit) {
    Column {
        AnimateItemPlacementInList(onShowCodeSnippetClick)
        AnimateContentSize(onShowCodeSnippetClick)
        AnimateEnterExitInAnimatedVisibilityScope(onShowCodeSnippetClick)
    }
}

@Composable
private fun AnimateContentSize(onShowCodeSnippetClick: (Int) -> Unit) {
    ModifierExample(
        title = "Animate content size",
        body = "Simply add animation to the size changing of the composable",
        codeSnippetImage = null,
        onShowCodeSnippetClick = onShowCodeSnippetClick
    ) {
        Text(text = "Click on the box to change its size")
        var boxSize by remember {
            mutableStateOf(50.dp)
        }
        Box(modifier = Modifier
            .padding(HalfDefault)
            .background(Color.Red)
            .animateContentSize(tween(2000))
            .size(boxSize)
            .clickable {
                if (boxSize == 50.dp) {
                    boxSize = 100.dp
                } else {
                    boxSize = 50.dp
                }
            }
        )
    }
}

@Composable
private fun ColumnScope.AnimateEnterExitInAnimatedVisibilityScope(onShowCodeSnippetClick: (Int) -> Unit) {
    ModifierExample(
        title = "Animate Enter Exit in AnimatedVisibility Scope",
        body = "This Modifier will override the enter and exit animation defined in AnimatedVisibility Composable that su00rrounds it",
        codeSnippetImage = R.drawable.modifier_animate_enter_exit,
        onShowCodeSnippetClick = onShowCodeSnippetClick
    ) {
        var isVisible by remember {
            mutableStateOf(true)
        }
        ElevatedButton(onClick = { isVisible = !isVisible }) {
            Text(text = "Inverse visibility")
        }
        Spacer(modifier = Modifier.height(Default))
        AnimatedVisibility(
            visible = isVisible,
            enter = fadeIn(),
            exit = fadeOut()
        ) {
            Column {
                Text(text = "Text that doesn't override the AnimatedVisibility animation")
                Spacer(modifier = Modifier.height(HalfDefault))
                Text(
                    text = "Text that uses animateEnterExit modifier to override the animation",
                    Modifier.animateEnterExit(
                        enter = slideInHorizontally(),
                        exit = slideOutHorizontally()
                    )
                )
            }
        }
    }
}

@Composable
@OptIn(ExperimentalFoundationApi::class)
private fun AnimateItemPlacementInList(onShowCodeSnippetClick: (Int) -> Unit) {
    ModifierExample(
        title = "Animate Item Placement In LazyList Scope",
        body = "Adding this modifier to the items of LazyList will animate the items when the order of them change",
        codeSnippetImage = R.drawable.modifier_animate_item_placement,
        onShowCodeSnippetClick = onShowCodeSnippetClick
    ) {
        val items = remember { mutableStateListOf(*('A'..'F').map { "$it" }.toTypedArray()) }
        ElevatedButton(onClick = { items.shuffle() }) {
            Text(text = "Shuffle")
        }
        var animationState by remember {
            mutableStateOf(Off)
        }
        Row(verticalAlignment = Alignment.CenterVertically) {
            TriStateCheckbox(state = animationState, onClick = {
                animationState = when (animationState) {
                    On -> Off
                    Off -> Indeterminate
                    Indeterminate -> On
                }
            })
            Text(
                text = when (animationState) {
                    On -> "High Bouncy Spring (Custom Animation)"
                    Off -> "No animation (Without the modifier)"
                    Indeterminate -> "Default animation"
                }
            )
        }
        LazyColumn {
            items(items, key = { it }) {
                Text(
                    text = "Item $it", Modifier.run {
                        if (animationState == Off) this
                        else {
                            animateItemPlacement(
                                animationSpec =
                                if (animationState == Indeterminate) { // Default animation
                                    spring(
                                        stiffness = Spring.StiffnessMediumLow,
                                        visibilityThreshold = IntOffset.VisibilityThreshold
                                    )
                                } else { // High Bouncy
                                    spring(
                                        stiffness = Spring.StiffnessHigh,
                                        dampingRatio = Spring.DampingRatioHighBouncy
                                    )
                                }
                            )
                        }
                    }
                )
            }
        }

        NoteCard {
            Text(text = "Note that you have to define keys to the items, to let the LazyList knows where the old item became after reordering")
        }
    }
}

@Preview
@Composable
fun AnimationModifiersScreenPreview() {
    ComposePlaygroundTheme {
        AnimationModifiersScreen {}
    }
}