// Top-level build file where you can add configuration options common to all sub-projects/modules.
@Suppress("DSL_SCOPE_VIOLATION") // TODO: Remove once KTIJ-19369 is fixed
plugins {
    alias(libs.plugins.com.android.application) apply false
    alias(libs.plugins.org.jetbrains.kotlin.android) apply false
    alias(libs.plugins.google.hilt) apply false
//    kotlin("kapt").version("1.9.0")
    id("com.google.devtools.ksp") version "2.0.0-1.0.21" apply false
    alias(libs.plugins.compose.compiler) apply false
//    id("androidx.room") version "2.6.1" apply false
}
true // Needed to make the Suppress annotation work for the plugins block